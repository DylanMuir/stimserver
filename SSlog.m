function SSlog(varargin)

% SSlog - Print a message to the screen, and log it
%
% Usage: SSlog(...)
%
% Usage semantics are as for fprintf, sprintf, ...

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 1st September, 2010

% -- Global state

global STIMSERV_sState;


% -- Echo text to screen

fprintf(1, ['SSL: ' varargin{1}], varargin{2:end});

% -- Echo text to logging interface, if possible

try
   % - Filter text for newlines, replace with terminator values
   varargin{1} = strrep(varargin{1}, '\n', STIMSERV_sState.vnLogTerminator);

   if (numel(STIMSERV_sState.oLogging) == 2)
      % - Try a network log

      if (STIMSERV_sState.bUsePNET)
         % - Is there actually an open (TCP) connection?
         if (STIMSERV_sState.bUseTCP && pnet(STIMSERV_sState.hLogSocket, 'status') ~= 11)
            % - No, so try to close the port
            if (pnet(STIMSERV_sState.hLogSocket, 'status') == 0)
               pnet(STIMSERV_sState.hLogSocket, 'close');
            end
            
            % - And try to open a new connection
            % - Make a TCP connection
            STIMSERV_sState.hLogSocket = pnet('tcpconnect', STIMSERV_sState.oLogging{1}, STIMSERV_sState.oLogging{2});
         end
         
         % - Send string using PNET
         pnet(STIMSERV_sState.hLogSocket, 'printf', [datestr(clock) ': ' varargin{1}], varargin{2:end});
         
         if (~STIMSERV_sState.bUseTCP)
            % - Transmit the write buffer
            pnet(STIMSERV_sState.hLogSocket, 'writepacket');
         end
         
      else
         % - Send string using instrument control toolbox
         fprintf(STIMSERV_sState.hLogSocket, [datestr(clock) ': ' varargin{1}], varargin{2:end});
      end
      
   else
      % - Send logging string to the file interface
      fprintf(STIMSERV_sState.hLogSocket, [datestr(clock) ': ' varargin{1}], varargin{2:end});
   end
   
catch errMsg
   % - Just fail quietly
end

% --- END of SSlog.m ---
