function [vfPixelsPerDegree, vfDegreesPerMetre, vfPixelsPerMetre] = CalibrateMacBook

% CalibrateMacBook - FUNCTION Returns the screen calibration information for a macbook laptop screen,
%
% Usage: [vfPixelsPerDegree, vfDegreesPerMetre, vfPixelsPerMetre] = CalibrateMacBook
%
% Assumed 13.3 in screen, viewed at 40cm.

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 30th August, 2010

% -- Do it!

[vfPixelsPerDegree, vfDegreesPerMetre, vfPixelsPerMetre] = CalibrateDisplay([0.2865 0.1791], [1280 800], 0.4);

% --- END of CalibrateMacBook.m ---
