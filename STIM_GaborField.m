function [sStimulus] = STIM_GaborField(varargin)

% STIM_GaborField - Stimulus object
%
% Usage: [sStimulus] = STIM_GaborField( tFrameDuration, tStimDuration, ...
%                          mfGaborLocsDeg, fPixelsPerDeg, ...
%                          vfGaborWidthDeg, vfGaborSFCPD, vfGaborContrast, ...
%                          vfGaborOrientationRad, vfGaborPhaseRad, ...
%                          vfGaborDriftTFHz, vfGaborRotationHz)

% Author: Dylan Muir <muir@hifo.uzh.ch>
% Created: 9th November, 2012

%- Deal out parameters from argument list
[tFrameDuration, tStimDuration, ...
   mfGaborLocsDeg, fPixelsPerDeg, ...
   vfGaborWidthDeg, vfGaborSFCPD, vfGaborContrast, ...
   vfGaborOrientationRad, vfGaborPhaseRad, ...
   vfGaborDriftTFHz, vfGaborRotationHz] = ...
      DealParamsOrNaN(varargin{:});

% - Group parameters
cPresentationParams = {tFrameDuration, tStimDuration};
cStimParams = {mfGaborLocsDeg, fPixelsPerDeg, ...
   vfGaborWidthDeg, vfGaborSFCPD, vfGaborContrast, ...
   vfGaborOrientationRad, vfGaborPhaseRad, ...
   vfGaborDriftTFHz, vfGaborRotationHz};

% - Trim off NANs
cPresentationParams = cPresentationParams(~cellfun(@isnan_empty, cPresentationParams));
cStimParams = cStimParams(~cellfun(@isnan_empty, cStimParams));

% - Construct "get duration" function
fhDurationFunction = @(s, b)(s.cPresentationParameters{2}+b);

% - Build stimulus structure                 
sStimulus = CreateStimulusStructure(@PresentGaborFieldStimulus, ...
               cPresentationParams, cStimParams, fhDurationFunction, ...
               @DescribeGaborFieldStimulus);

            
function vbIsNAN = isnan_empty(voThing)

vbIsEmpty = isempty(voThing);

vbIsNAN = false(size(vbIsEmpty));
vbIsNAN(~vbIsEmpty) = isnan(voThing(~vbIsEmpty));



% --- END of STIM_GaborField.m ---
