function StopStimulusServer(strSeverity)

% StopStimulusServer - FUNCTION Shutdown the stimulus server
%
% Usage: StopStimulusServer
%        StopStimulusServer('shutdown')
%        StopStimulusServer('abort')
%        StopStimulusServer('cleanup')

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 1st September, 2010

% -- Global state

global STIMSERV_sStimulusList STIMSERV_sState; %#ok<NUSED>


% -- Defaults

DEF_strSeverity = 'shutdown';


% -- Check arguments

if (~exist('strSeverity', 'var') || isempty(strSeverity))
   strSeverity = DEF_strSeverity;
end

if (isempty(STIMSERV_sState))
   if (~isequal(strSeverity, 'cleanup'))
      SSlog('--- StopStimulusServer: The server was never initialised.\n');
   end
   
   return;
end


% -- Close either using pnet or Instrument Control toolbox

if (isfield(STIMSERV_sState, 'hWindow'))
   hWindow = STIMSERV_sState.hWindow;
else
   hWindow = [];
end

StimServerVisualFeedback(hWindow, 'Shutting down...');

try
   if (STIMSERV_sState.bUsePNET)
      StopStimulusServerPNET;
   else
      StopStimulusServerICT;
   end

catch err
   % - There was an error shutting down the server
   try SSlog('*** StopStimulusServer: Server is aborting; couldn''t shut down cleanly.\n'); end %#ok<TRYNC>
   try SStalkback('SST ABORT'); end %#ok<TRYNC>

   % - Kill stimulus display
   try Screen('CloseAll'); end %#ok<TRYNC>
   
   % - Close all pnet sockets
   if (exist('pnet') == 3) %#ok<EXIST>
      try pnet('closeall'); end %#ok<TRYNC>
   end
   
   % - Clear server state
   STIMSERV_sState = [];
   STIMSERV_sStimulusList = [];
   
   % - Display error state
   SSlog('       Error: [%s]\n', err.message);
   disp('       Stack:');
   
   for (nStack = 1:numel(err.stack))
      disp(err.stack(nStack));
      disp('---');
   end
end

StimServerVisualFeedback(hWindow, 'Server not running', true);

return;

% --- END of StopStimulusServer FUNCTION ---


function StopStimulusServerPNET
   
   % -- Perform requested action
   
   switch (lower(strSeverity))
      case 'shutdown'
         SSlog('--- StopStimulusServer: Shutting down server...\n');
         SStalkback('SST SHUTDOWN');
         
         % - Turn off logging and talkback
         StimServerLogging([]);
         StimServerTalkback([]);
         
         % - Close all pnet sockets
         pnet(STIMSERV_sState.hServSocket{2}, 'close');
         
         if (STIMSERV_sState.bUseTCP)
            pnet(STIMSERV_sState.hServSocket{1}, 'close');
         end
         
         % - Clear server state
         STIMSERV_sState = [];
         STIMSERV_sStimulusList = [];
         
         SSlog('--- StopStimulusServer: Server has shutdown.\n');
         
      case 'abort'
         SSlog('--- StopStimulusServer: Aborting server...\n');
         SStalkback('SST ABORT');
         
         % - Turn off logging and talkback
         StimServerLogging([]);
         StimServerTalkback([]);
         
         % - Kill stimulus display
         Screen('CloseAll');
         
         % - Close all pnet sockets
         pnet('closeall');

         % - Clear server state
         STIMSERV_sState = [];
         STIMSERV_sStimulusList = [];
         
         SSlog('--- StopStimulusServer: Server has aborted (as cleanly as possible).\n');
         
      case 'cleanup'
         try SSlog('--- StopStimulusServer: Shutting down server and cleaning up...\n'); end %#ok<TRYNC>
         try SStalkback('SST SHUTDOWN'); end %#ok<TRYNC>
         
         % - Turn off logging and talkback
         try StimServerLogging([]); end %#ok<TRYNC>
         try StimServerTalkback([]); end %#ok<TRYNC>
         
         % - Close all pnet sockets
         try pnet('closeall'); end %#ok<TRYNC>

         % - Clear server state
         STIMSERV_sState = [];
         STIMSERV_sStimulusList = [];

         SSlog('--- StopStimulusServer: Server has been shut down and cleaned up.\n');

      otherwise
         SSlog('--- StopStimulusServer: Incorrect usage.\n');
         help StopStimulusServer;
   end

end

% --- END of StopStimulusServerPNET FUNCTION ---


function StopStimulusServerICT
   
   % -- Perform requested action
   
   switch (lower(strSeverity))
      case {'shutdown', 'cleanup'}
         SSlog('--- StopStimulusServer: Shutting down server...\n');
         SStalkback('SST SHUTDOWN');
         
         % - Turn off logging and talkback
         StimServerLogging([]);
         StimServerTalkback([]);
         
         % - Disable server callback
         set(STIMSERV_sState.hServSocket, 'DatagramReceivedFcn', '');
         
         % - Close UDP channel
         fclose(STIMSERV_sState.hServSocket);
         
         % - Clear server state
         STIMSERV_sState = [];
         STIMSERV_sStimulusList = [];
         
         SSlog('--- StopStimulusServer: Server has shutdown.\n');
         
      case 'abort'
         SSlog('--- StopStimulusServer: Aborting server...\n');
         SStalkback('SST ABORT');
         
         % - Turn off logging and talkback
         StimServerLogging([]);
         StimServerTalkback([]);
         
         % - Kill stimulus display
         Screen('CloseAll');
         
         % - Disable server callback
         set(STIMSERV_sState.hServSocket, 'DatagramReceivedFcn', '');
         
         % - Close UDP channel
         fclose(STIMSERV_sState.hServSocket);
         
         % - Clear server state
         STIMSERV_sState = [];
         STIMSERV_sStimulusList = [];
         
         SSlog('--- StopStimulusServer: Server has aborted (as cleanly as possible).\n');
         
      otherwise
         SSlog('--- StopStimulusServer: Incorrect usage.\n');
         help StopStimulusServer;
   end

end

% --- END of StopStimulusServerICT FUNCTION


end

% --- END of StopStimulusServer.m ---
