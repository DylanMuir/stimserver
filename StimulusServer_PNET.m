function StimulusServer_PNET

% - StimulusServer_PNET - FUNCTION Core server using pnet
%
% Used internally by StimServer. Do not call this function.

% -- Globals

global STIMSERV_sState;


% -- Has the toolbox been initialised?

if (isempty(STIMSERV_sState))
   SSlog('*** StimulusServer_PNET: Server was not started using StartStimulusServer');
   return;
end


% -- Check to see if we should really be running the pnet verion

if (~STIMSERV_sState.bUsePNET)
   SSlog('*** StimulusServer_PNET: We should be using the instrument control toolbox!');
   return;
end


% -- Main server loop

% - Register an onCleanup instance to shut down the server if this function
% terminates
SSonCleanup(@() StopStimulusServer('shutdown'));

% - Wrap server operation in try/fail block to catch errors and shut down the
% server nicely
try

   while (~isempty(STIMSERV_sState))
      
      % - Wait for a command to be received
      while (~IsDataAvailable)
         % - Blank screen
         StimServerVisualFeedback(STIMSERV_sState.hWindow, '');
         
         % - Check socket connection status
         % - We only need to handle connections if using TCP
         if (STIMSERV_sState.bUseTCP)
            % - Check to see if this was a disconnected socket
            if (pnet(STIMSERV_sState.hServSocket{2}, 'status') == 0)
               % - The socket was disconnected, so we should close it nicely
               pnet(STIMSERV_sState.hServSocket{2},'close');
               STIMSERV_sState.hServSocket{2} = -1;
            end
            
            if (isempty(STIMSERV_sState.hServSocket{2}) || (STIMSERV_sState.hServSocket{2} == -1))
               % - Try a new connection
               StimServerVisualFeedback(STIMSERV_sState.hWindow, 'Waiting for connection...', true);
               STIMSERV_sState.hServSocket{2} = pnet(STIMSERV_sState.hServSocket{1}, 'tcplisten', 'noblock');
               pnet(STIMSERV_sState.hServSocket{2}, 'setreadtimeout', 1);
               
               % - Display some information about the connected host
               if (pnet(STIMSERV_sState.hServSocket{2}, 'status') == 12)
                  [vnIP, nPort] = pnet(STIMSERV_sState.hServSocket{2}, 'gethost');
                  strAddress = sprintf('%d.%d.%d.%d', vnIP);
                  % strHostname = resolvehost(strAddress);
                  
                  SSlog('--- StimulusServer: Connected to a remote client at [%s:%d]\n', strAddress, nPort);
               end
            end
         end
      end
      
      % - Read commands until they are all processed
      while (~isempty(STIMSERV_sState) && IsDataAvailable)
         % - Read a command string
         strCommand = pnet(STIMSERV_sState.hServSocket{2}, 'readline', 65535);
         
         % - Check for an empty command string
         if (isempty(strCommand))
            % - Let's just go around and try again
            continue;
         end
         
         % - Process the command;
         StimulusServer_COMMAND(strCommand);
      end
      
      % - Send IDLE status
      SStalkback('SST IDLE');
   end

   
catch errMsg
   % - There was an error somewhere, so try to shut down the server nicely
   StopStimulusServer('shutdown');
   
   % - Re-throw the error message
   rethrow(errMsg);
end
   

function bAvailable = IsDataAvailable
   if (~STIMSERV_sState.bUseTCP)
      % = Check for UDP data and read it
      if (isempty(pnet(STIMSERV_sState.hServSocket{2}, 'read', 1, [], [], 'view')))
         pnet(STIMSERV_sState.hServSocket{2}, 'readpacket');
      end
   end
   
   bAvailable = ~isempty(pnet(STIMSERV_sState.hServSocket{2}, 'read', 1, [], [], 'view'));
end


% --- END of StimulusServer_PNET FUNCTION ---

end

% --- END of StimulusServer_PNET
