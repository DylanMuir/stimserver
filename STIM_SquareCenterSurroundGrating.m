function [sStimulus] = STIM_SquareCenterSurroundGrating(varargin)

% STIM_SquareGrating - Stimulus object
%
% Usage: [sStimulus] = ...
%        STIM_SquareCenterSurroundGrating( tFrameDuration, vtStimulusDuration, nNumRepeats, ...
%                                          fPixelsPerDegree, vfCSCyclesPerDegree, ...
%                                          cvfCSBarWidthDegrees, vfCSAngleDegrees, ...
%                                          vfCSShiftCyclesPerSecond, vfCSRotateCyclesPerSecond, ...
%                                          vfCenterPosOffsetDegrees, cvfCASRadiusDegrees)

% Author: Dylan Muir and Patricia Molina-Luna, based on Dylan Muir's STIM_SquareGrating.m
% Created: 20th January, 2011

%- Deal out parameters from argument list
[tFrameDuration, vtStimulusDuration, nNumRepeats, ...
   fPixelsPerDegree, vfCSCyclesPerDegree, ...
   cvfCSBarWidthDegrees, vfCSAngleDegrees, ...
   vfCSShiftCyclesPerSecond, vfCSRotateCyclesPerSecond, ...
   vfCenterPosOffsetDegrees, cvfCASRadiusDegrees] = DealParamsOrNaN(varargin{:});

% - Group parameters
cPresentationParams = {tFrameDuration, vtStimulusDuration, nNumRepeats};
cStimParams = {fPixelsPerDegree, vfCSCyclesPerDegree, ...
   cvfCSBarWidthDegrees, vfCSAngleDegrees, ...
   vfCSShiftCyclesPerSecond, vfCSRotateCyclesPerSecond, ...
   vfCenterPosOffsetDegrees, cvfCASRadiusDegrees};

% - Trim off NANs
cPresentationParams = cPresentationParams(~cellfun(@isnan_empty, cPresentationParams));
cStimParams = cStimParams(~cellfun(@isnan_empty, cStimParams));

% - Construct "get duration" function
fhDurationFunction = @(s, b)(sum(s.cPresentationParameters{2}(:))+b);

% - Build stimulus structure                 
sStimulus = CreateStimulusStructure(@PresentSquareCenterSurroundGratingStimulus, ...
               cPresentationParams, cStimParams, fhDurationFunction, ...
               @DescribeSquareCenterSurroundGratingStimulus);

            
function vbIsNAN = isnan_empty(voThing)

if (iscell(voThing))
   vbIsNAN = false;
   
else
   vbIsEmpty = isempty(voThing);
   vbIsNAN = false(size(vbIsEmpty));
   vbIsNAN(~vbIsEmpty) = isnan(voThing(~vbIsEmpty));
end




% --- END of STIM_SquareCenterSurroundGrating.m ---
