function [sStimulus, hWindow, vtBlankTime, nStimID] = ...
   StimulusServer_DecodeStimulusCommand(strCommand)

% StimulusServer_DecodeStimulusCommand - FUNCTION Decode a command string
%
% Do not call this function.

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 31st August, 2010

% -- Global state

global STIMSERV_sStimulusList STIMSERV_sState;


% -- Check arguments

if (nargin < 1)
   SSlog('*** StimulusServer_DecodeStimulusCommand: Do not call this function from the command line.\n');
   return;
end

% - Read command tokens
cellTokens = textscan(strCommand, '%s %s %f %f %f %f %f %*[^\n]');

if (isempty(cellTokens{3}) || isempty(cellTokens{4}))
   % - Incorrect argument format, so return empty values
   sStimulus = [];
   hWindow = [];
   vtBlankTime = [];
   tFrameDuration = [];
   nStimID = [];
   return;
end   

% - Expand command parameters
[nStimID, vtBlankTime(1)] = cellTokens{3:4};

% - Check stimulus ID
if (nStimID > numel(STIMSERV_sStimulusList))
   % - This stimulus didn't exist, so return empty values
   sStimulus = [];
   hWindow = [];
   vtBlankTime = [];
   tFrameDuration = [];
   nStimID = [];
   return;
end

% - Extract the stimulus structure
sStimulus = STIMSERV_sStimulusList(nStimID);

% - Extract other parameters
if (STIMSERV_sState.tLastStimTime ~= -inf)
   vtBlankTime(2) = STIMSERV_sState.tLastStimTime;
end

hWindow = STIMSERV_sState.hWindow;


% -- Extract optional parameters from command line

if (~isempty(cellTokens{5}) && ~isnan(cellTokens{5}))
   nNumExtraPresentationArgs = cellTokens{5};
else
   nNumExtraPresentationArgs = 0;
end

if (~isempty(cellTokens{6}) && ~isnan(cellTokens{6}))
   nNumExtraStimArgs = cellTokens{6};
else
   nNumExtraStimArgs = 0;
end

% - How many extra arguments must we expect?
nNumExtraArgs = nNumExtraPresentationArgs + nNumExtraStimArgs;

% - Read any extra arguments
if (nNumExtraArgs > 0)
   % - Construct de-tokenise string
   strFormat = '%*s %*s %*f %*f %*f %*f';
   strFormat = [strFormat repmat(' %s', 1, nNumExtraArgs) ' %*[^\n]'];
   
   % - Read extra arguments only
   cArgTokens = textscan(strCommand, strFormat);

   % - Convert argument formats
   cExtraArgs = cell(1, nNumExtraArgs);
   
   for (nExtraArg = 1:nNumExtraArgs) %#ok<FORPF>
      strArg = cArgTokens{nExtraArg}{1};
      fArg = str2num(strArg); %#ok<ST2NM>
      
      if (~isempty(fArg))
         % - Treat this argument as a number
         cExtraArgs{nExtraArg} = fArg;
      else
         cExtraArgs{nExtraArg} = strArg;
      end
   end
   
   % - Parcel out arguments
   cExtraPresentationArgs = cExtraArgs(1:nNumExtraPresentationArgs);
   cExtraStimArgs = cExtraArgs((nNumExtraPresentationArgs+1):end);
   
   sStimulus.cPresentationParameters = [sStimulus.cPresentationParameters cExtraPresentationArgs];
   sStimulus.cStimulusArguments = [sStimulus.cStimulusArguments cExtraStimArgs];
end


% --- END of StimulusServer_DecodeStimulusCommand.m ---
