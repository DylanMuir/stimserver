function varargout = ScreenDimensions(oScreenSize, fAspectRatio)

% ScreenDimensions - FUNCTION Calculate dimensions of a screen
% 
% Usage: [fDiagSize] = ScreenDimensions([fWidth fHeight])
%        [fWidth fHeight] = ScreenDimensions(fDiagSize, fAspectRatio)
%
%

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 2nd September, 2009

% -- Check arguemtns

if (numel(oScreenSize) == 1)
   % - Compute width and height from diagonal distance and aspect ratio   
   fDiagSize = oScreenSize;
   fHeight = sqrt(fDiagSize^2 / (fAspectRatio^2 + 1));
   fWidth = fHeight * fAspectRatio;
   varargout = {fWidth fHeight};

elseif (numel(oScreenSize) == 2)
   %  - Compute diagonal size from width and height
   fWidth = oScreenSize(1);
   fHeight = oScreenSize(2);
   fDiagSize = sqrt(fWidth^2 + fHeight^2);
   
   varargout = {fDiagSize};
   
else
   disp('*** ScreenDimensions: Incorrect usage');
   help ScreenDimensions;
end



% --- END of ScreenDimensions.m ---
