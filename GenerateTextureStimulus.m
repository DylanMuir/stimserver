function [mfTexture, fPixelsShiftMod] = GenerateTextureStimulus(vfSizeDegrees, fGridsPerDegree, ...
                                            fPixelsPerDegree, nPixelsPerGrid, ...
                                            fRelativeAngle, ...
                                            fJitter, fWidthDegrees, fLengthDegrees, ...
                                            bGabor, fBandwidth, fPhaseoffset, nSeed, vfGreyLevels)
                                        
                                       
% GenerateTextureStimulus - Compute oriented semi-pseudorandomly distributed bar segments.
%
% % % % % Usage: mfTexture = GenerateSineGratingStimulus(vnSize, fCyclesPerPixel)
% % % % %        mfTexture = GenerateSineGratingStimulus(vfSizeDegrees, fCyclesPerDegree <, fPixelsPerDegree>)
% % % % %
% % % % % 'vnSize' is a two-elelment vector [N M], defining the size of the grating
% % % % % texture to be NxM (in degrees, if 'fPixelsPerDegree' is supplied.
% % % % % 'fCyclesPerPixel' or 'fCyclesPerDegree' and 'fPixelsPerDegree' define the
% % % % % spatial frequency of the grating.  'fContrast' is a real value between 0 and
% % % % % 1, where 1 corresponds to 100% contrast (black to white) and 0 corresponds to
% % % % % 0% contrast (grey only).
% % % % %
% % % % % 'mfTexture' will be a matlab matrix containing a grating stimulus.  The black
% % % % % and white values will be 0 (black) and 1 (white).
% % % % %
% % % % % Make it big enough that you can shift it around on the screen without seeing
% % % % % the edges of the texture.

% Author: Andreas Keller <andi@ini.phys.ethz.ch>
% Created: 19th August, 2011


% -- Check arguments

if (nargin < 10)
   disp('*** GenerateTextureStimulus: Incorrect usage');
   help GenerateTextureStimulus;
   return;
end


fDark = -1;
fBright = 1;
fGrey = 0;


% - Create minimal rectangle around texture element

fWidth = round(fWidthDegrees * fPixelsPerDegree);
fLength = round(fLengthDegrees * fPixelsPerDegree);


if bGabor
    % - Derived values
    fAngle = mod(90 - fRelativeAngle, 180);
    fWavelength = fWidth*fBandwidth;
    fAspectratio = fWidth/fLength; %aspect ratio
    nElementlength = 3*round(max(fWidth, fLength));
    nElementHalf = round(nElementlength/2);
    
    
    % - Create texture element
    
    fSigma = fWavelength/pi * sqrt(log(2)/2) * (2^fBandwidth + 1)/(2^fBandwidth - 1);
    
    mfStimElement = [];
    mfStimElement = fDark * ones(2*nElementHalf);
    
%     fAngle = fAngle - 90;
    for x = -nElementHalf+1:nElementHalf
        for y = -nElementHalf+1:nElementHalf
            x2 = x*cos(fAngle/180*pi) + y*sin(fAngle/180*pi);
            y2 = -x*sin(fAngle/180*pi) + y*cos(fAngle/180*pi);
            mfStimElement(x + round(nElementlength/2), y + round(nElementlength/2)) = ...
            exp(-(x2^2 + fAspectratio^2*y2^2) / (2*fSigma^2)) * cos(2*pi*x2/fWavelength + fPhaseoffset);
        end
    end
    
    fBackground = fGrey;
    
else
    fAngle = mod(fRelativeAngle, 180);
    
    fX1 = abs(cos((fAngle)/180*pi)) * fLength;
    fX2 = sin((fAngle)/180*pi) * fWidth;
    fY1 = sin((fAngle)/180*pi) * fLength;
    fY2 = abs(cos((fAngle)/180*pi)) * fWidth;
    fTan = abs(tan((fAngle)/180*pi));
    
    if fAngle > 90
        fMemory = fX1;
        fX1 = fX2;
        fX2 = fMemory;
        fMemory = fY1;
        fY1 = fY2;
        fY2 = fMemory;
        fTan = 1/fTan;
    end

    mfStimElement = fDark * ones(round(fX1 + fX2), round(fY1 + fY2));

    for nIndX = 1 : round(fX1 + fX2)
        mfStimElement(nIndX, 1:max(round(fY1 - fTan * nIndX),0)) = fBright;
        mfStimElement(nIndX, min(round(fY1 + nIndX / fTan), round(fY1 + fY2)) : round(fY1 + fY2)) = fBright;
        mfStimElement(nIndX, 1:max(round((nIndX-fX1) / fTan), 0)) = fBright;
        mfStimElement(nIndX, min(round((fY1 + fY2) - (nIndX - fX2) * fTan), round(fY1 + fY2)) : round(fY1 + fY2)) = fBright;
    end
    
    fBackground = fBright;
    
    %tweaking the parameters to get a desired overlay of mfStimElements
    %with mfTexture
    fBackground = fBackground - 1;
    mfStimElement = mfStimElement - 1;
end

% - Create the texture

vnSize = round(vfSizeDegrees .* fPixelsPerDegree);
mfTexture = fBackground * ones(vnSize);

vnGrids = [round(vfSizeDegrees(1) * fGridsPerDegree), round((vfSizeDegrees(2) - vfSizeDegrees(1)) * fGridsPerDegree)];


rand('twister', nSeed);

vfRand = rand(prod(vnGrids) * 2,1) * fJitter;
nXJitterMax = nPixelsPerGrid;
nYJitterMax = nPixelsPerGrid;
vnHalfSpace = round(([1 1] * nPixelsPerGrid - size(mfStimElement))/2);

for nIndGrid = 1 : prod(vnGrids)
    [nXGrid, nYGrid] = ind2sub(vnGrids, nIndGrid);
    mfTexture(mod(round(((nXGrid - 1) * nPixelsPerGrid) + vnHalfSpace(1) + round(vfRand(2 * nIndGrid - 1) * nXJitterMax)) : ...
        round(((nXGrid - 1) * nPixelsPerGrid) + vnHalfSpace(1) + size(mfStimElement, 1) + round(vfRand(2 * nIndGrid - 1) * nXJitterMax) - 1), vnSize(1)) + 1, ...
        mod(round(((nYGrid - 1) * nPixelsPerGrid) + vnHalfSpace(2) + round(vfRand(2 * nIndGrid) * nYJitterMax)) : ...
        round(((nYGrid - 1) * nPixelsPerGrid) + vnHalfSpace(2) + size(mfStimElement, 2) + round(vfRand(2 * nIndGrid) * nYJitterMax) - 1), vnSize(2) - vnSize(1)) + 1) = ...
        (mfTexture(mod(round(((nXGrid - 1) * nPixelsPerGrid) + vnHalfSpace(1) + round(vfRand(2 * nIndGrid - 1) * nXJitterMax)) : ...
        round(((nXGrid - 1) * nPixelsPerGrid) + vnHalfSpace(1) + size(mfStimElement, 1) + round(vfRand(2 * nIndGrid - 1) * nXJitterMax) - 1), vnSize(1)) + 1, ...
        mod(round(((nYGrid - 1) * nPixelsPerGrid) + vnHalfSpace(2) + round(vfRand(2 * nIndGrid) * nYJitterMax)) : ...
        round(((nYGrid - 1) * nPixelsPerGrid) + vnHalfSpace(2) + size(mfStimElement, 2) + round(vfRand(2 * nIndGrid) * nYJitterMax) - 1), vnSize(2) - vnSize(1)) + 1) + ...
        mfStimElement);
end

% - Copy at least one screan diagonal of the beginning to the end for
% smooth transitions in screen function. Starting from the second pixel
% avoids a psychtoolbox edge artefact.

mfTexture(:, vnGrids(2) * nPixelsPerGrid + 1 : end) = ...
    mfTexture(:, 1 : size(mfTexture(:, vnGrids(2) * nPixelsPerGrid + 1 : end), 2));

fPixelsShiftMod = vnGrids(2) * nPixelsPerGrid;

% - Scale mfTexture from [-1 1] to [0 1]
if (bGabor)
    mfTexture = mfTexture / 2 + 0.5;
else
    vfBars = [];
    vfBackground = [];
    mfTexture = mfTexture / 2 + 1;
    vfBars = find(mfTexture ~= 1);
    vfBackground = find(mfTexture == 1);
    if vfGreyLevels == [0 0.5];
        mfTexture(vfBackground) = 0.5 * (1 + fWidthDegrees * fLengthDegrees * fGridsPerDegree^2);
    elseif vfGreyLevels == [1 0.5];
        mfTexture(vfBars) = 1;
        mfTexture(vfBackground) = 0.5 * (1 - fWidthDegrees * fLengthDegrees * fGridsPerDegree^2);
    elseif vfGreyLevels == [1 0];
        mfTexture(vfBars) = 1;
        mfTexture(vfBackground) = 0;
    elseif vfGreyLevels == [0 1];
        mfTexture(vfBars) = 0;
        mfTexture(vfBackground) = 1;
    end
end



% --- END of GenerateSineGrating.m ---
