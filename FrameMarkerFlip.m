function varargout = FrameMarkerFlip(hWindow, varargin)

% FrameMarkerFlip - FUNCTION Flip frames, but mark each frame 
% with an oscillating black / white marker region at the bottom right corner of
% the screen, as well as a stimulus marker at the bottom left corner of the
% screen.
%
% Usage: varargout = FrameMarkerFlip(hWindow, vnMarkerSize, bShowStimulusMarker,
%                                    bShowFrameMarker, bSynchToFrames, 
%                                    bInvertStimMarker, bMarkerStartsBlack)
%        varargout = FrameMarkerFlip(hWindow, bFlipStimMarker <, Screen arguments>)
%
% 'hWindow' is a window opened with the PsychToolbox command 'Screen'.
% 'vnMarkerSize' defines the size of the marker region in pixels.  It must be a
% vector [M N] for an MxN sized region.  Subsequent arguments are passed to the
% Screen 'Flip' command.  If 'vnMarkerSize' is provided then the textures will
% be reset.  Only provide this parameter to initialise the display.
%
% 'bShowStimulusMarker' is a boolean indicating if the stimulus marker
% should be displayed
% 'bShowFrameMarker' is a boolean indicating if the frame marker should be
% displayed
% 'bSynchToFrames' is a boolean. If true, subtract half the native frame 
% duration from the requested flip time
% 'bInvertStimMarker' is a boolean. If true, the color of the stim marker
% changes from black to white to black at every stim iteration
% 'bMarkerStartsBlack' is a boolean. If true the markers are first black
% (then change color depending on arguments), if false, markers a re first
% white
%
% Return arguments are passed from Screen('Flip', ...).

% Author: Dylan Muir <dyan@ini.phys.ethz.ch>
% Created: 25th August, 2010

% -- Persistent variables

persistent  FMF_texMarkerBlack FMF_texMarkerWhite FMF_vnMarkerSize FMF_bBlack ...
            FMF_vnFMDestRect FMF_bSynchToFrames FMF_vtSafeFrameDurations FMF_tHalfIFI ...
            FMF_bShowStimMarker FMF_bShowFrameMarker FMF_vnSMDestRect ...
            FMF_bStimMarkerBlack FMF_bInvertStimMarker ...
            FMF_bFunctionAtFrameFlip FMF_fFunctionAtFrameFlip; %#ok<PUSE>


% -- Check arguments

if (nargin < 1)
   disp('*** FrameMarkerFlip: Incorrect usage');
   help FrameMarkerFlip;
   return;
end

if ((numel(varargin) > 0) && (numel(varargin{1}) == 2))
   % - Extract marker size
   FMF_vnMarkerSize = varargin{1}(:)';

   % - Reset the textures
   % - Get black and white indices
   nBlack = BlackIndex(hWindow);
   nWhite = WhiteIndex(hWindow);
   
   % - Make textures
   FMF_texMarkerBlack = Screen('MakeTexture', hWindow, nBlack * ones(FMF_vnMarkerSize([2 1])));
   FMF_texMarkerWhite = Screen('MakeTexture', hWindow, nWhite * ones(FMF_vnMarkerSize([2 1])));
   
   if (nargin > 2 && islogical(varargin{2}))
      FMF_bShowStimMarker = varargin{2};
   else
      FMF_bShowStimMarker = false;
   end
   
   if (nargin > 2 && islogical(varargin{3}))
      FMF_bShowFrameMarker = varargin{3};
   else
      FMF_bShowFrameMarker = true;
   end
   
   if (numel(varargin) > 2 && islogical(varargin{4}))
      FMF_bSynchToFrames = varargin{4};
   else
      FMF_bSynchToFrames = false;
   end
   
   if (nargin > 5 && islogical(varargin{5}))
      FMF_bInvertStimMarker = varargin{5};
   else
      FMF_bInvertStimMarker = true;
   end
   
   % - Define flip variables
   if (nargin > 6 && islogical(varargin{6}))
       FMF_bBlack = varargin{6};
       FMF_bStimMarkerBlack = varargin{6};

   elseif (nargin > 6)
       warning('--- FrameMarkerFlip: bMarkerStartsBlack should be a logical value. Using default.')
       FMF_bBlack = true;
       FMF_bStimMarkerBlack = false;
   else
       FMF_bBlack = true;
       FMF_bStimMarkerBlack = false;
   end
   
   if nargin > 7
       FMF_bFunctionAtFrameFlip = true;
       FMF_fFunctionAtFrameFlip = varargin{7};
   else
       FMF_bFunctionAtFrameFlip = false;
       FMF_fFunctionAtFrameFlip = [];
   end
   
   % - Find where to draw the marker
   vnScreenRect = Screen('Rect', hWindow);
   FMF_vnSMDestRect = [vnScreenRect(3:4) - FMF_vnMarkerSize vnScreenRect(3:4)];
   FMF_vnFMDestRect = [vnScreenRect(1) vnScreenRect(4)-FMF_vnMarkerSize(2) FMF_vnMarkerSize(1) vnScreenRect(4)];
   
   % - Get safe frame durations
   [FMF_vtSafeFrameDurations, FMF_tHalfIFI] = GetSafeFrameDurations(hWindow, 1:100);
   FMF_tHalfIFI = FMF_tHalfIFI/2;
   
   

   
   % - Blank window and return timing measures
   [varargout{1:5}] = Screen('Flip', hWindow);
   
else
   
   % -- Draw texture and flip

   bFlipStimMarker = varargin{1};
   varargin = varargin(2:end);
   
   % - Set alpha = 1 for frame markers
   Screen('Blendfunction', hWindow, GL_ONE, GL_ZERO);
   
   if (FMF_bShowStimMarker)
      if (bFlipStimMarker && FMF_bInvertStimMarker)
         FMF_bStimMarkerBlack = ~FMF_bStimMarkerBlack;
      end
      
      if (FMF_bStimMarkerBlack)
         Screen('DrawTexture', hWindow, FMF_texMarkerBlack, [], FMF_vnSMDestRect);
      else
         Screen('DrawTexture', hWindow, FMF_texMarkerWhite, [], FMF_vnSMDestRect);
      end
   end
   
   if (FMF_bShowFrameMarker)
       FMF_bBlack = ~FMF_bBlack;

       if (FMF_bBlack)
          Screen('DrawTexture', hWindow, FMF_texMarkerBlack, [], FMF_vnFMDestRect);
       else
          Screen('DrawTexture', hWindow, FMF_texMarkerWhite, [], FMF_vnFMDestRect);
       end
   end

if FMF_bFunctionAtFrameFlip
    FMF_fFunctionAtFrameFlip();
end

if (FMF_bSynchToFrames && numel(varargin) > 1)
  % - Subtract half the native frame duration from the requested flip time
  [varargout{1:5}] = Screen('Flip', hWindow, varargin{1}-FMF_tHalfIFI, varargin{2:end});
else

  % - Flip window and return arguments
  [varargout{1:5}] = Screen('Flip', hWindow, varargin{:});
end
   
end


% --- END of FrameMarkerFlip.m ---
