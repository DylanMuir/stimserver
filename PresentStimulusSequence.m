function [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
            PresentStimulusSequence(hWindow, vtBlankTime, ...
                                    vnOrdering, sSequence)

% PresentStimulusSequence - FUNCTION Present a sequence of stimuli
%
% Usage: [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
%             PresentStimulusSequence(hWindow, vtBlankTime, ...
%                                     vnOrdering, sSequence)
%
% 'hWindow' and 'vtBlankTime'' have the semantics for
% PresentArbitraryStimulus.
%
% 'sSequence' is a structure array, where each element in the array is a
% stimulus structure as defined in PresentArbitraryStimulus.
%
% 'vnOrdering' is a vector of sequence IDs, where each ID indexes a stimulus in
% 'sSequence'.  It can be of any length.  If an empty matrix is passed, the
% sequence will be presented once in order.

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 3rd Septmeber, 2010

% -- Check arguments

if (nargin < 3)
   SSlog('*** PresentStimulusSequence: Incorrect usage.\n');
   return;
end

if (nargin == 3)
    sSequence = vnOrdering;
    vnOrdering = [];
end

% - Check ordering
nNumStimuli = numel(sSequence);

if (~exist('vnOrdering', 'var') || isempty(vnOrdering))
   vnOrdering = 1:nNumStimuli;

elseif ((max(vnOrdering) > nNumStimuli) || (min(vnOrdering) < 1))
   SSlog('*** PresentStimulusSequence: Invalid sequence ordering.\n');
   return;
end


% -- If the blank time was empty, call each presentation function to
% initialise

if (isempty(vtBlankTime))
   for (nSeqID = 1:nNumStimuli) %#ok<FORPF>
      SSlog('--- PresentStimulusSequence: Pre-initialising sequence ID [%d]...\n', nSeqID);
      PresentArbitraryStimulus(sSequence(nSeqID), hWindow, []);
   end
   
   tLastPresentation = [];
   bBlewBlankTime = [];
   bBlewFrameRate = [];
   return;
end


% -- Construct an "expected start time" trace

% - Get durations for each stimululs

vtStimDurations = zeros(1, nNumStimuli);
for (nSeqID = 1:nNumStimuli) %#ok<FORPF>
   vtStimDurations(nSeqID) = sSequence(nSeqID).fhDurationFunction(sSequence(nSeqID), vtBlankTime(1));
end

% - Re-order and work out start times
vtStimDurations = vtStimDurations(vnOrdering(:));
vtStartTimes = [0 cumsum(vtStimDurations(1:end-1))];

% - Get a timestamp
tStartTime = Screen('Flip', hWindow);
vtStartTimes = vtStartTimes + tStartTime;


% -- Present each stimulus in turn

bBlewBlankTime = false;
bBlewFrameRate = false;

for (nSeqStimID = 1:numel(vnOrdering))
   % - Announce that we are presenting a sequence ID
   SSlog('--- PresentStimulusSequence: Presenting sequence ID [%d]\n', vnOrdering(nSeqStimID));
   SStalkback('SST SEQ %d', vnOrdering(nSeqStimID));
   
   % - Present the stimulus
   try
       [tLastPresentation, bBlewThisBlankTime, bBlewThisFrameRate] = ...
         PresentArbitraryStimulus(sSequence(vnOrdering(nSeqStimID)), hWindow, ...
         [vtBlankTime(1) vtStartTimes(nSeqStimID)]);
      
      % -- Record whether we blew any blank time or frame rate
      if (bBlewThisBlankTime)
         SSlog('--- PresentStimulusSequence: Warning: The last stimulus blew its blank time.\n');
      end
      
      if (bBlewThisFrameRate)
         SSlog('--- PresentStimulusSequence: Warning: The last stimulus blew its frame rate.\n');
      end
      
      bBlewBlankTime = bBlewBlankTime | bBlewThisBlankTime;
      bBlewFrameRate = bBlewFrameRate | bBlewThisFrameRate;
      
   catch errMsg
      rethrow(errMsg);
   end
end

% --- END of PresentStimulusSequence.m ---
