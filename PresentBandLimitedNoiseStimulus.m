function [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
            PresentBandLimitedNoiseStimulus( hWindow, vtBlankTime, ...
                                             tFrameDuration, fPPD, nNumRepeats, ...
                                             vfSpatFreqCutoffCPD, vfTempFreqCutoffHz, ...
                                             vfStimSizeDeg, tStimDuration, fSpatSamplingSPD, ...
                                             tWindowTime, tModulationPeriod, fModulationAmplitude, nSeed)

% PresentBandLimitedNoiseStimulus - FUNCTION Present a band-limited noise stimulus
%
% Usage: [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
%             PresentBandLimitedNoiseStimulus(  hWindow, vtBlankTime, ...
%                                               tFrameDuration, fPPD, nNumRepeats, ...
%                                               vfSpatFreqCutoffCPD, vfTempFreqCutoffHz, ...
%                                               vfStimSizeDeg, tStimDuration, fSpatSamplingSPD, ...
%                                               < tWindowTime, tModulationPeriod, fModulationAmplitude, nSeed>)
%
% 'hWindow' is a handle to a currently open window. 'vtBlankTime' is the time
% preceeding stimulus presentation.  'tFrameDuration' is the duration of each
% presented frame. 
%
% 'vfSpatFreqCutoffCPD' and 'vfTempFreqCutoffHz' can be supplied as scalar
% values, in which case they are assumed to be the upper cut-offs of low-pass
% filters.

% Author: Dylan Muir <dylan@ini.pys.ethz.ch>
% Created: 6th September, 2010

% -- Persistent arguments

persistent PBLNS_CACHE PBLNS_cNoiseCache;

if (~exist('vtBlankTime', 'var') || isempty(vtBlankTime))
   tStartTime = 0;
   vtBlankTime = 0;
   bPresentStimulus = false;
else
   % -- Blank screen
   tStartTime = Screen('Flip', hWindow);
   
   bPresentStimulus = true;
end

if (numel(vtBlankTime) > 1)
   tFlipTime = vtBlankTime(2) + vtBlankTime(1);
else
   tFlipTime = tStartTime + vtBlankTime(1);
end


% -- Check arguments

if (~exist('nSeed', 'var'))
   nSeed = [];
end

if (~exist('tWindowTime', 'var'))
   tWindowTime = [];
end

if (~exist('tModulationPeriod', 'var'))
   tModulationPeriod = [];
end

if (~exist('fModulationAmplitude', 'var'))
   fModulationAmplitude = [];
end

% -- Do we need to generate the stimulus for this parameter set?

% - Make parameters structure
sParams.nSeed = nSeed;
sParams.hWindow = hWindow;
sParams.vfSpatFreqCutoffCPD = vfSpatFreqCutoffCPD;
sParams.vfTempFreqCutoffHz = vfTempFreqCutoffHz;
sParams.vfStimSizeDeg = vfStimSizeDeg;
sParams.tStimDuration = tStimDuration;
sParams.fSpatSamplingSPD = fSpatSamplingSPD;
sParams.tFrameDuration = tFrameDuration;
sParams.tWindowTime = tWindowTime;
sParams.tModulationPeriod = tModulationPeriod;
sParams.fModulationAmplitude = fModulationAmplitude;

% - Record screen rectangle
sParams.vnScreenRect = [0 0 0 0];
[sParams.vnScreenRect(3), sParams.vnScreenRect(4)] = Screen('WindowSize', hWindow);

% - How big should the stimulus be?
if (isempty(vfStimSizeDeg))
   vfStimSizeDeg = sParams.vnScreenRect(3:4) ./ fPPD;
end

% - Compute presentation rectangle
sParams.vnDestRec = CenterRect([0 0 round(vfStimSizeDeg * fPPD)], sParams.vnScreenRect);

% - Check cache
[nCacheNum, bCacheHit, bCacheCollision, sCachedParams, PBLNS_CACHE] = ...
   ParamCache(sParams, PBLNS_CACHE); %#ok<ASGLU>

% - Do we need to generate the stimulus?
bGenerateStim = ~bCacheHit | bCacheCollision;


% -- Generate stimulus as single pixels

if (bGenerateStim)
   SSlog('--- PresentBandLimitedNoiseStimulus: Generating stimulus\n');
   
   % - Generate stimulus
   PBLNS_cNoiseCache{nCacheNum}.tfBLNoise = ...
      GenerateBandLimitedNoiseStimulus(vfSpatFreqCutoffCPD, vfTempFreqCutoffHz, ...
                                       vfStimSizeDeg, tStimDuration, fSpatSamplingSPD, tFrameDuration, ...
                                       tWindowTime, tModulationPeriod, fModulationAmplitude, nSeed);
   
   % - Scale between black and white
   nBlack = BlackIndex(hWindow);
   nWhite = WhiteIndex(hWindow);
   nRange = nWhite - nBlack;
   
   PBLNS_cNoiseCache{nCacheNum}.tfBLNoise = PBLNS_cNoiseCache{nCacheNum}.tfBLNoise + 0.5;
   PBLNS_cNoiseCache{nCacheNum}.tfBLNoise = PBLNS_cNoiseCache{nCacheNum}.tfBLNoise .* nRange;
   PBLNS_cNoiseCache{nCacheNum}.tfBLNoise = PBLNS_cNoiseCache{nCacheNum}.tfBLNoise + nBlack;
   SliceFunction(PBLNS_cNoiseCache{nCacheNum}.tfBLNoise, @round, 3);

   % - Wipe any existing textures
   PBLNS_cNoiseCache{nCacheNum}.vnTexIDs = [];
end


% - Create textures, if necessary
if (isempty(PBLNS_cNoiseCache{nCacheNum}.vnTexIDs))
   bGenerateTexture = true;

else
   % - Test to see if our texture exists
   vnTextures = Screen('Windows');
   
   if (~all(ismember(PBLNS_cNoiseCache{nCacheNum}.vnTexIDs, vnTextures)))
      bGenerateTexture = true;
   else
      bGenerateTexture = false;
   end
end

% - Generate textures, if required
if (bGenerateTexture)   
   % - Check that we can actually use a shader
   AssertGLSL;
   
   % - Generate the textures
   PBLNS_cNoiseCache{nCacheNum}.vnTexIDs = GenerateTextures(permute(PBLNS_cNoiseCache{nCacheNum}.tfBLNoise, [2 1 3]), hWindow);
end


% -- Try to pre-load textures

Screen('PreloadTextures', hWindow, PBLNS_cNoiseCache{nCacheNum}.vnTexIDs);


% -- Present stimulus

if (~bPresentStimulus)
   tLastPresentation = [];
   bBlewFrameRate = [];
   bBlewBlankTime = [];
   return;
end   

nNumFrames = numel(PBLNS_cNoiseCache{nCacheNum}.vnTexIDs);
bFirst = true;
bBlewFrameRate = false;
bBlewBlankTime = false;
tEndTime = tFlipTime + nNumRepeats * nNumFrames * tFrameDuration;

for (nRepeat = 1:nNumRepeats)
   for (nFrame = 1:nNumFrames)
      % - Draw texture and flip window
      Screen('DrawTexture', hWindow, PBLNS_cNoiseCache{nCacheNum}.vnTexIDs(nFrame), [], sParams.vnDestRec);
      [tCurrTime, nul, nul, Missed] = FrameMarkerFlip(hWindow, bFirst, tFlipTime); %#ok<ASGLU>

      % - Did we miss any deadlines?
      if (bFirst)
         bBlewBlankTime = Missed > 0;
      else
         bBlewFrameRate = bBlewFrameRate | (Missed > 0);
      end

      bFirst = false;
      
      % - Compute next flip time
      tFlipTime = tCurrTime + tFrameDuration;
      
      % - Have we blown our stimulation duration?
      if (tFlipTime > tEndTime)
         break;
      end
   end
end

% - Display a warning if necessary
if (bBlewBlankTime || bBlewFrameRate)
   SSlog('--- PresentBandLimitedNoiseStimulus: The desired frame rate or blanking period was not met.\n');
end

% - Return the last stimulation time
tLastPresentation = Screen('Flip', hWindow, tEndTime);

% --- END of PresentBandLimitedNoiseStimulus.m ---

