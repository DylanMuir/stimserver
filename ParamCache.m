function [nCacheNum, bCacheHit, bCacheCollision, sCachedParams, CACHE] = ...
   ParamCache(sParams, CACHE)

% ParamCache - FUNCTION Maintain a parameter cache
%
% Usage: [nCacheNum, bCacheHit, bCacheCollision, sCachedParams, CACHE] = ...
%           ParamCache(sParams, CACHE)
%
% 'sParams' is a structure array with the desired parameters to cache.  For
% better caching, only include parameters for which modifications will require
% re-generating a stimulus.  'CACHE' is an opaque object that must be passed in
% to this function on every call.
%
% 'nCacheNum' will be a low-valued, unique integer that corresponds to the
% location of the provided parameter set in the cache.  This can be used to
% cache the result of generating a stimulus in an external cache structure.  The
% cache indices in the external cache can then be managed using ParamCache.
% 'bCacheHit' is a boolean variable indicating whether or not the provided
% parameters in 'sParams' were already present in the cache.  'bCacheCollision'
% indicates that another set of parameters, that hashed to the same value, was
% already present in the cache.  If 'bCollision' is true, then the cached
% parameters were replaced with the new set in 'sParams', and any externally
% cached stimuli MUST be regenerated.  'sCachedParams' returns the parameters
% that were in the cache, if present -- in the case of a collision, the OLD
% parameters are returned for convenience.
%
% 'CACHE' MUST be passed IN and OUT of ParamCache.

% -- Defaults

DEF_nCacheSize = 100;


% -- Check arguments

if ((nargin < 2) || (nargout < 5))
   disp('*** ParamCache: Incorrect usage.');
   help ParamCache;
   return;
end

% -- Hash parameters

nHashVal = round(mod(mod(HashStructure(sParams), DEF_nCacheSize-1), DEF_nCacheSize-1)+1);


% -- Initialise cache, insert first parameters

if (isempty(CACHE))
   CACHE.vsParamCache = {};
   CACHE.vnCacheIndex = sparse(DEF_nCacheSize, 1);
   
   bCacheHit = false;
   bCacheCollision = false;
   nCacheNum = 1;
   CACHE.vnCacheIndex(nHashVal) = nCacheNum;
   sCachedParams = sParams;
   CACHE.vsParamCache{nCacheNum} = sCachedParams;
   return;
end


% -- Check cache for parameters

if (CACHE.vnCacheIndex(nHashVal) == 0)
   % - Cache miss, so insert
   bCacheHit = false;
   bCacheCollision = false;
   nCacheNum = numel(CACHE.vsParamCache)+1;
   CACHE.vnCacheIndex(nHashVal) = nCacheNum;
   sCachedParams = sParams;
   CACHE.vsParamCache{nCacheNum} = sCachedParams;
   return;

else
   % - Extract cached parameters, check for collision
   bCacheHit = true;
   nCacheNum = CACHE.vnCacheIndex(nHashVal);
   sCachedParams = CACHE.vsParamCache{nCacheNum};
   bCacheCollision = ~isequal(sCachedParams, sParams);
end


% --- END of ParamCache.m ---
