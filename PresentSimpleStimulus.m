function [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
   PresentSimpleStimulus(  hWindow, vtBlankTime, ...
   tFrameDuration, tStimDuration, ...
   nNumRepeats, vhTextureHandles, ...
   vfPixelsPerFrameShift, fBaseAngle, fRotationPerFrame, ...
   fPixelsShiftMod, fPixelsPerDegree, bDrawMask, ...
   bInvertMask, fMaskDiameterDeg, vfMaskPosPix, ...
   fContrast, bAdapt, tAdaptationDuration, fAdaptShiftPixPerFr, ...
   fAdaptContrast, bCyclicTF, ...
   tTFRampDuration, tTFTransitionDuration, ...
   vfTextureShiftPix)


% PresentSimpleStimuls - FUNCTION Present a simple visual stimulus, with
% shift and rotation. The stimulus can be presented with a circular mask.
%
% Usage: [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
%          PresentSimpleStimulus(  hWindow, vtBlankTime, ...
%                                  tFrameDuration, tStimDuration, ...
%                                  nNumRepeats, vhTextureHandles, ...
%                                  <vfPixelsPerFrameShift, fBaseAngle, fRotationPerFrame, ...
%                                  fPixelsShiftMod, fPixelsPerDegree, bDrawMask, ...
%                                  bInvertMask, fMaskDiameterDeg, vfMaskPosPix, ...
%                                  tAdaptationDuration, fAdaptShiftPixPerFr, ...
%                                  fAdaptContrast, bCyclicTF, ...
%                                  tTFRampDuration, tTFTransitionDuration, ..
%                                  vfTextureShiftPix>)
%

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>, modified by Patricia Molina-Luna
% to add masking (24th January, 2011), modified by Andreas Keller (7th
% Oktober 2011)
% Created: 27th August, 2010

persistent PSS_nMaskTexID;

if (~exist('vtBlankTime', 'var') || isempty(vtBlankTime))
   tStartTime = 0;
   vtBlankTime = 0;
   bPresentStimulus = false;
else
   % -- Blank screen
   tStartTime = Screen('Flip', hWindow);
   
   bPresentStimulus = true;
end

if (numel(vtBlankTime) > 1)
   tFlipTime = vtBlankTime(2) + vtBlankTime(1);
else
   tFlipTime = tStartTime + vtBlankTime(1);
end


% -- Default parameters

DEF_bDrawMask = false;
DEF_bInvertMask = false;
DEF_fMaskDiameterDeg = [];
DEF_vfMaskPosPix = [0 0];
DEF_vfTextureShiftPix = [0 0];


% -- Check arguments

if (nargin < 6)
   disp('*** PresentSimpleStimulus: Incorrect usage');
   help PresentSimpleStimulus;
   sca;
   return;
end

if (~exist('vfPixelsPerFrameShift', 'var') || isempty(vfPixelsPerFrameShift))
   vfPixelsPerFrameShift = [0 0];
end

if isscalar(vfPixelsPerFrameShift)
    vfPixelsPerFrameShift = [vfPixelsPerFrameShift vfPixelsPerFrameShift];
end

if (~exist('fBaseAngle', 'var') || isempty(fBaseAngle))
   fBaseAngle = 0;
end

if (~exist('fRotationPerFrame', 'var') || isempty(fRotationPerFrame))
   fRotationPerFrame = 0;
end

if (~exist('fPixelsShiftMod', 'var') || isempty(fPixelsShiftMod))
   fPixelsShiftMod = 0;
end

if (~exist('fPixelsPerDegree', 'var') || isempty(fPixelsPerDegree))
   fPixelsPerDegree = 1;
end

if (~exist('bDrawMask', 'var') || isempty(bDrawMask))
   bDrawMask = DEF_bDrawMask;
end

if (~exist('bInvertMask', 'var') || isempty(bInvertMask))
   bInvertMask = DEF_bInvertMask;
end

if (~exist('fMaskDiameterDeg', 'var') || isempty(fMaskDiameterDeg))
   fMaskDiameterDeg = DEF_fMaskDiameterDeg;
end

if (~exist('vfMaskPosPix', 'var') || isempty(vfMaskPosPix))
   vfMaskPosPix = DEF_vfMaskPosPix;
end

if (~exist('fContrast', 'var') || isempty(fContrast))
   fContrast = 1;
end

if (~exist('bAdapt', 'var') || isempty(bAdapt))
   bAdapt = false;
end

if (~exist('tAdaptationDuration', 'var') || isempty(tAdaptationDuration))
   tAdaptationDuration = 0;
end

if (~exist('fAdaptShiftPixPerFr', 'var') || isempty(fAdaptShiftPixPerFr))
   fAdaptShiftPixPerFr = 0;
end

if (~exist('fAdaptContrast', 'var') || isempty(fAdaptContrast))
   fAdaptContrast = 1;
end

if (~exist('bCyclicTF', 'var') || isempty(bCyclicTF))
   bCyclicTF = false;
end

if (~exist('tTFRampDuration', 'var') || isempty(tTFRampDuration))
   tTFRampDuration = 0;
end

if (~exist('tTFTransitionDuration', 'var') || isempty(tTFTransitionDuration))
   tTFTransitionDuration = 0;
end

if (~exist('vfTextureShiftPix', 'var') || isempty(vfTextureShiftPix))
    vfTextureShiftPix = DEF_vfTextureShiftPix;
end

% - make sure CyclicTF and Adapt are not enabled simultaneously

if (bCyclicTF && bAdapt)
   disp('*** PresentSimpleStimulus: Incorrect usage');
   disp('CyclicTF and Adapt can not be enabled simultaneously');
   help PresentSimpleStimulus;
   sca;
   return;
end

% -- Try to preload textures

Screen('PreloadTextures', hWindow, vhTextureHandles);

% -- Determine nGrey

nWhite = WhiteIndex(hWindow);
nBlack = BlackIndex(hWindow);
nGrey = round((nWhite + nBlack)/2);

if (nGrey == nWhite)
   nGrey = (nWhite + nBlack)/2;
end

% -- Determine screen size

vnTextureRect = Screen('Rect', vhTextureHandles(1));
vnScreenRect = Screen('Rect', hWindow');
nTextureRestLeft = floor((vnTextureRect(3) - vnTextureRect(1) ...
    - ((vnScreenRect(3) - vnScreenRect(1)) ...
    * abs(cos((mod(fBaseAngle,180))/180*pi)) ...
    + (vnScreenRect(4) - vnScreenRect(2)) ...
    * sin((mod(fBaseAngle,180))/180*pi)))/2);


% -- Generate the mask texture

if (bDrawMask && bPresentStimulus)
   nMaskWidth = fMaskDiameterDeg * fPixelsPerDegree;
   nMaskHeight =  fMaskDiameterDeg * fPixelsPerDegree;
   mbMask = Ellipse(nMaskWidth/2, nMaskHeight/2);
   
   if (bInvertMask)
      nBackground = nBlack;
      nMask = nWhite;
   else
      nBackground = nWhite;
      nMask = nBlack;
   end
   
   mnPartialMask = double(mbMask);
   mnPartialMask(mbMask) = nMask;
   mnPartialMask(~mbMask) = nBackground;
   
   vnMaskRect = CenterRect([0 0 size(mbMask)], vnScreenRect);
   vnMaskRect = vnMaskRect + round([vfMaskPosPix vfMaskPosPix]);
   
   tnMask = nGrey * ones(vnScreenRect(4), vnScreenRect(3), 2);
   tnMask(:, :, 2) = nBackground;
   tnMask(vnMaskRect(2)+1:vnMaskRect(4), vnMaskRect(1)+1:vnMaskRect(3), 2) = mnPartialMask;
   
   % - Remove an existing mask texture
   if (exist('PSS_nMaskTexID', 'var') && ~isempty(PSS_nMaskTexID) && ismember(PSS_nMaskTexID, Screen('Windows')))
      try %#ok<TRYNC>
         Screen('Close', PSS_nMaskTexID);
      end
   end
   
   % - Generate the mask texture   
   PSS_nMaskTexID = Screen('MakeTexture', hWindow, tnMask);
end


% -- Present stimulus

if (~bPresentStimulus)
   tLastPresentation = [];
   bBlewBlankTime = [];
   bBlewFrameRate = [];
   return;
end

% - Start from first stimulus frame
bFirst = true;
bBlewFrameRate = false;
nNumStims = numel(vhTextureHandles);
tNextFlipTime = tFlipTime;
tCurrTime = -inf;
nStimID = 0;
   
for (nRepeat = 1:nNumRepeats) %#ok<FORPF>
   
   % - Reset stimulus params
   tEndTime = inf;
   bFirstThisRepeat = true;
   fCurrShift = 0;
   fCurrAngle = fBaseAngle;
   fFade = 1;
   tTFCycleTime = 0;
   fCurrPixPerFrSh = vfPixelsPerFrameShift(1);
   
   if (bAdapt)
       bCurrAdapt = true;
   else
       tAdaptationDuration = 0;
       bCurrAdapt = false;
   end
   
   while (tCurrTime <= tEndTime)
       
      if (bCurrAdapt)
         % - Yes, so we need to pre-set the adaptation stimulus contrast by
         % using an alpha value
         Screen('FillRect', hWindow, [nGrey nGrey nGrey 255 * (1-fAdaptContrast)]);
         Screen('Blendfunction', hWindow, GL_ONE_MINUS_DST_ALPHA, GL_DST_ALPHA);
         
         % - Are we adjusting the temporal frequency (and also the
         % contrast) of the stimulus?
      elseif (bCyclicTF)
         % - Yes, so we need to pre-set the stimulus contrast by using an alpha
         % value
         Screen('FillRect', hWindow, [nGrey nGrey nGrey 255 * fFade]);
         Screen('Blendfunction', hWindow, GL_ONE_MINUS_DST_ALPHA, GL_DST_ALPHA);
         
      elseif (fContrast < 1)
         Screen('FillRect', hWindow, [nGrey nGrey nGrey 255 * (1-fContrast)]);
         Screen('Blendfunction', hWindow, GL_ONE_MINUS_DST_ALPHA, GL_DST_ALPHA);
      end
      
      % - Draw a single texture onto the screen, centred, shifted and
      % rotated
      vnDestRect = Screen('Rect', vhTextureHandles(nStimID+1));
      vnDestRect = CenterRect(vnDestRect, Screen('Rect', hWindow));
      vnDestRect = vnDestRect + round([vfTextureShiftPix vfTextureShiftPix]);
      Screen('DrawTexture', hWindow, vhTextureHandles(nStimID+1), [], vnDestRect, fCurrAngle, [], [], [], [], [], [0 mod(fCurrShift, fPixelsShiftMod)-nTextureRestLeft 0 0]);
      
      if (bCurrAdapt || bCyclicTF || (fContrast < 1))
          Screen('Blendfunction', hWindow, GL_ONE, GL_ZERO);
      end
      
      % - Draw the mask, taking the alpha value of the mask
      if (bDrawMask)
         Screen('Blendfunction', hWindow, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
         Screen('DrawTexture', hWindow, PSS_nMaskTexID);
      end
      
      % - Flip window
      [tCurrTime, nul, nul, Missed] = FrameMarkerFlip(hWindow, bFirstThisRepeat, tNextFlipTime); %#ok<ASGLU>
      
      
      % - Initialise stimulation and check deadlines
      
      if (bFirst)
         bBlewBlankTime = Missed > 0;
         tStartTime = tCurrTime;
         tNextFlipTime = tCurrTime;
         bFirst = false;
      else
         bBlewFrameRate = bBlewFrameRate | (Missed > 0);
      end
      
      % - Reset end time for this stimulus repetition
      
      if (bFirstThisRepeat)
          tEndTime = tStartTime + nRepeat * tAdaptationDuration + nRepeat * tStimDuration;
          bFirstThisRepeat = false;
      end
      
      % - Determine next flip time
      tNextFlipTime = tNextFlipTime + tFrameDuration;
      tTFCycleTime = tTFCycleTime + tFrameDuration;
      
      % - Update shift and rotation parameters
      if (tNextFlipTime <= tStartTime + nRepeat * tAdaptationDuration + (nRepeat-1) * tStimDuration)
          fCurrShift = fCurrShift + fAdaptShiftPixPerFr;
      else
          if (bCurrAdapt)
              Screen('Blendfunction', hWindow, GL_ONE, GL_ZERO);
              bFirstThisRepeat = true;
              bCurrAdapt = false;
          end
          
          fCurrAngle = fCurrAngle + fRotationPerFrame;
          fCurrShift = fCurrShift + fCurrPixPerFrSh;
          
          if bCyclicTF
             if tTFCycleTime < tTFTransitionDuration/2
                fFade = fFade - 2 / tTFTransitionDuration * tFrameDuration * fContrast;
             elseif tTFCycleTime < tTFTransitionDuration/2 + tTFRampDuration
                fCurrPixPerFrSh = fCurrPixPerFrSh + ...
                   vfPixelsPerFrameShift(2) / tTFRampDuration * tFrameDuration;
             elseif tTFCycleTime < tTFTransitionDuration + tTFRampDuration
                fFade = fFade + 2 / tTFTransitionDuration * tFrameDuration * fContrast;
             else
                tTFCycleTime = 0;
                fCurrPixPerFrSh = vfPixelsPerFrameShift(1);
             end
          end
      end
      
      % - Present the next stimulus frame next time
      nStimID = mod(nStimID+1, nNumStims);
   end
end

% - Display a warning
if (bBlewBlankTime || bBlewFrameRate)
   SSlog('--- PresentSimpleStimulus: The desired frame rate or blanking period was not met.\n');
end

% - Return the last stimulation time
tLastPresentation = Screen('Flip', hWindow, tEndTime);


% --- END of PresentSimpleStimulus.m ---

