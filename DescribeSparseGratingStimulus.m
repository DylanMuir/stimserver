function [strDescription] = DescribeSparseGratingStimulus(sStimulus, strIndent)

% SparseGratingStimulus - STIMULUS A stimulus composed of active regions randomly placed on a field, each on e a drifting square-wave grating, to characterise a simple cell receptive field
%
% This stimulus is a uniform field, on which single active regions are placed
% randomly in turn.  An active regin consists of an (optionally) drifting and
% rotating square-wave grating.  A sequence consisting of all possible active region
% locations, with a random order, is generated.  The number of different
% contrasts (or colours) used for the active regions is fully configurable.
% 
% Presentation parameters:
%
%     tFrameDuration - The desired duration of a single frame
%     tPixelDuration - The desired duration for whice a single active region is
%        presented
%     nNumRepeats - The number of times to repeat the stimulus presentation
%        (there for convenience) 
%
% Stimulus parameters:
%     fUnitWidthDegrees - The width (diameter) of the circular active regions.
%     vnNumUnits - A two-element vector [nXSize nYSize], specifying the number
%        of active units used in the stimulus, as a grid of 'nXSize' x 'nYSize'.
%     fUnitOverlapProportion - The degree of overlap between adjacent active
%        regions.  0.5 means that two adjacent regions will overlap for half of
%        their width.  0 means that no overlap will be present.
%     fPixelsPerDegree - The calibration of the presentation screen, in pixels
%        per degree.
%     fCyclesPerDegree - The spatial frequency of the grating, in cycles per
%        degree.
%     fBarWidthDegrees - The width of a single grating bar, in visual degrees.
%        If not provided, then the gratings have a 50% duty cycle.
%     fAngleDegrees - The orientation of the grating in degrees, where 0 degrees
%        is a vertical grating.
%     fShiftCyclesPerSec - The distance to shift the grating during the stimulus
%        presentation, in grating cycles per second.
%     fRotateCyclesPerSec - The amount to rotate the grating during the stimulus
%        presentation, in rotational cycles per second.
%     vnSeed - A list (optionally a single number) of integers to use as seeds
%        for the pseudo-random sequence, so that the sequence is repeatable.  If
%        a list is provided, then the sequence will consist of a number of
%        sub-sequences equal to the length of this list, where each sub-sequence
%        in turn is the full sparse noise sequence corresponding to each of the
%        seeds in 'vnSeed'.
%     nNumFrames - An optional parameter specifying the total number of frames
%        to generate.  This number must be smaller or equal to the number of
%        frames in the full random sequence.  If provided, a sequence of length
%        'nNumFrames' will be returned, beginning from the first frame in the
%        random sequence.

% DescribeSparseNoiseStimulus - FUNCTION Describe the parameters for an square-wave grating stimulus
%
% Usage: [strDescription] = DescribeSparseNoiseStimulus(sStimulus <, strIndent>)
%
% Usually called automatically by StimulusServerDescribeStimuli

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 1st October, 2010

% -- Check arguments

if (nargin == 0)
   SSlog('*** DescribeSparseNoiseStimulus: Incorrect usage\n');
   help DescribeSparseNoiseStimulus;
   return;
end

if (~exist('strIndent', 'var') || isempty(strIndent))
   strIndent = '';
end


% -- Convert parameters to parameter strings

% - Extract parameters from stimulus
[tFrameDuration, tPixelDuration, nNumRepeats] = DealParamsOrNaN(sStimulus.cPresentationParameters{:});
[fUnitWidthDegrees, vnNumUnits, fUnitOverlapProportion, fPixelsPerDegree, ...
   fCyclesPerDegree, fBarWidthDegrees, ...
   fShiftCyclesPerSec, fBaseAngle, fRotateCyclesPerSec, ...
   vnSeed, nNumPixelsToPresent] = ...
      DealParamsOrNaN(sStimulus.cStimulusArguments{:});

strFrameDuration = CheckNanSprintf('%.2f ms', [], tFrameDuration, tFrameDuration*1e3);
strPixelDuration = CheckNanSprintf('%.2f ms', [], tPixelDuration, tPixelDuration*1e3);
strNumRepeats = CheckNanSprintf('%d', [], nNumRepeats, nNumRepeats);
strUnitWidthDegrees = CheckNanSprintf('%.2f deg', [], fUnitWidthDegrees, fUnitWidthDegrees);
strNumUnits = CheckNanSprintf('[%d x %d]', [], vnNumUnits, vnNumUnits);
strUnitOverlap = CheckNanSprintf('%.2f%%', [], fUnitOverlapProportion, fUnitOverlapProportion*100);
strPixelsPerDegree = CheckNanSprintf('%.2f ppd', [], fPixelsPerDegree, fPixelsPerDegree);
strCyclesPerDegree = CheckNanSprintf('%.2f cpd; %.2f dpc', [], fCyclesPerDegree, fCyclesPerDegree, 1/fCyclesPerDegree);
strBarWidthDegrees = CheckEmptyNanSprintf('%.2f deg', '(0.5 duty cycle)', [], fBarWidthDegrees, fBarWidthDegrees);
strAngleDegrees = CheckNanSprintf('%.2f deg', [], fBaseAngle, fBaseAngle);
strShiftCyclesPerSec = CheckEmptyNanSprintf('%.2f Hz', '(stationary)', '(dynamic or stationary)', fShiftCyclesPerSec, fShiftCyclesPerSec);
strRotateCyclesPerSec = CheckEmptyNanSprintf('%.2f Hz', '(stationary)', '(dynamic or stationary)', fRotateCyclesPerSec, fRotateCyclesPerSec);
strSeed = ['[' CheckEmptyNanSprintf(' %d', '1', [], vnSeed, vnSeed) ']'];
strNumPixels = CheckEmptyNanSprintf('%d', '(full sequence)', [], nNumPixelsToPresent, nNumPixelsToPresent);
   
% -- Produce description
   
strDescription = [strIndent sprintf('Oscillating plaid stimulus\n') ...
                  strIndent sprintf('Parameter list:\n')];
strIndent = [strIndent '   '];

strDescription = [strDescription ...
   strIndent sprintf('Frame duration: %s\n', strFrameDuration) ...
   strIndent sprintf('Single unit presentation duration: %s\n', strPixelDuration) ...
   strIndent sprintf('Number of repeats: %s\n', strNumRepeats) ...
   strIndent sprintf('Active unit width: %s\n', strUnitWidthDegrees) ...
   strIndent sprintf('Number of units: %s\n', strNumUnits) ...
   strIndent sprintf('Degree of overlap between units: %s\n', strUnitOverlap) ...
   strIndent sprintf('Screen calibration: %s\n', strPixelsPerDegree) ...
   strIndent sprintf('Grating spatial frequency: %s\n', strCyclesPerDegree) ...
   strIndent sprintf('Grating bar width: %s\n', strBarWidthDegrees) ...
   strIndent sprintf('Grating orientation: %s\n', strAngleDegrees) ...
   strIndent sprintf('Grating drift: %s\n', strShiftCyclesPerSec) ...
   strIndent sprintf('Grating rotation: %s\n', strRotateCyclesPerSec) ...
   strIndent sprintf('Random seed(s): %s\n', strSeed) ...
   strIndent sprintf('Number of active units in sequence: %s\n', strNumPixels)];
   
% --- END of DescribeSquareGratingStimulus.m ---


function strString = CheckNanSprintf(strFormat, strEmptyFormat, oCheckNan, varargin)

if (~isnan(oCheckNan))
   strString = sprintf(strFormat, varargin{:});

elseif isempty(strEmptyFormat)
   strString = '(dynamic)';

else
   strString = sprintf(strEmptyFormat, varargin{:});
end


function strString = CheckEmptyNanSprintf(strFormat, strEmptyFormat, strNanFormat, oCheckEmptyNan, varargin)

if (isnan(oCheckEmptyNan))
   if (isempty(strNanFormat))
      strString = '(dynamic)';
   else
      strString = sprintf(strNanFormat, varargin{:});
   end
   
elseif (isempty(oCheckEmptyNan))
   if isempty(strEmptyFormat)
      strString = '(dynamic)';
   else
      strString = sprintf(strEmptyFormat, varargin{:});
   end
   
else
   strString = sprintf(strFormat, varargin{:});
end

% --- END of DescribeSparseNoiseStimulus.m ---
