function [varargout] = DealParamsOrEmpty(varargin)

% DealParamsOrEmpty - FUNCTION Deal out a list of parameters, or return []
%
% Usage: [varargout] = DealParamsOrEmpty(varargin)
%
% Use like [a, b, c, d] = DealParamsOrEmpty(cellParamsList{:});
%
% a, b, c and d will be assigned values in order from 'cellParamsList'.  If
% 'cellParamsList' is too short, excess return parameters will be returned as
% empty matrices.

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 1st October, 2010

if (nargin < nargout)
   varargin{nargout} = [];
end

varargout = varargin;
