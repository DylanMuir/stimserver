function [sStimulus] = STIM_SparseGrating(varargin)

% STIM_SparseGrating - Stimulus object
%
% Usage: [sStimulus] = STIM_SparseGrating( tFrameDuration, tPixelDuration, nNumRepeats, ...
%                                          fUnitWidthDegrees, vnNumUnits, fUnitOverlapProportion, fPixelsPerDegree, ...
%                                          fCyclesPerDegree, fBarWidthDegrees, ...
%                                          fShiftCyclesPerSec, fBaseAngle, fRotateCyclesPerSec, ...
%                                          vnSeed, nNumPixelsToPresent)

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 26th October, 2010

%- Deal out parameters from argument list
[tFrameDuration, tPixelDuration, nNumRepeats, ...
   fUnitWidthDegrees, vnNumUnits, fUnitOverlapProportion, fPixelsPerDegree, ...
   fCyclesPerDegree, fBarWidthDegrees, ...
   fShiftCyclesPerSec, fBaseAngle, fRotateCyclesPerSec, ...
   vnSeed, nNumPixelsToPresent] = DealParamsOrNaN(varargin{:});

% - Group parameters
cPresentationParams = {tFrameDuration, tPixelDuration, nNumRepeats};
cStimParams = {fUnitWidthDegrees, vnNumUnits, fUnitOverlapProportion, fPixelsPerDegree, ...
   fCyclesPerDegree, fBarWidthDegrees, ...
   fShiftCyclesPerSec, fBaseAngle, fRotateCyclesPerSec, ...
   vnSeed, nNumPixelsToPresent};

% - Trim off NANs
cPresentationParams = cPresentationParams(~cellfun(@isnan_empty, cPresentationParams));
cStimParams = cStimParams(~cellfun(@isnan_empty, cStimParams));

% - Build stimulus structure                 
sStimulus = CreateStimulusStructure(@PresentSparseGratingStimulus, ...
               cPresentationParams, cStimParams, @calc_duration, ...
               @DescribeSparseGratingStimulus);

            
function vbIsNAN = isnan_empty(voThing)

vbIsEmpty = isempty(voThing);

vbIsNAN = false(size(vbIsEmpty));
vbIsNAN(~vbIsEmpty) = isnan(voThing(~vbIsEmpty));


function tDuration = calc_duration(sStim, tBlank)

% - How many seeds were provided for the stimulus?
if ((numel(sStim.cStimulusArguments) < 10) || isempty(sStim.cStimulusArguments{10}))
   % - By default, a single seed
   nNumSeeds = 1;
else
   % - A specific list of seeds was provided
   nNumSeeds = numel(sStim.cStimulusArguments{10});
end

% - Determine total number of frames in a single stimulus presentation
if ((numel(sStim.cStimulusArguments) < 11) || isempty(sStim.cStimulusArguments{11}))
   % - The total number of stimulus locations * the number of seeds
   nNumFrames = prod(sStim.cStimulusArguments{2}) * nNumSeeds;
else
   % - The number of frames was constrained
   nNumFrames = sStim.cStimulusArguments{11};
end

% - Total duration is the number of frames * pixel display time * number of repeats + blank time
tDuration = nNumFrames * sStim.cPresentationParameters{2} * sStim.cPresentationParameters{3} + tBlank;


% --- END of STIM_SparseGrating.m ---
