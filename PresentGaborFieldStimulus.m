function [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
            PresentGaborFieldStimulus(hWindow, vtBlankTime, ...
            	tFrameDuration, tStimDuration, mfGaborLocsDeg, fPixelsPerDeg, ...
               vfGaborWidthDeg, vfGaborSFCPD, vfGaborContrast, ...
               vfGaborOrientationRad, vfGaborPhaseRad, ...
               vfGaborDriftTFHz, vfGaborRotationHz)
                           
% PresentGaborFieldStimulus - FUNCTION Present an arbitrary field of drifting Gabors
%
% Usage: [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
%           PresentGaborFieldStimulus(hWindow, vtBlankTime, ...
%             	tFrameDuration, tStimDuration, mfGaborLocsDeg, fPixelsPerDeg, ...
%                vfGaborWidthDeg, vfGaborSFCPD, vfGaborContrast, ...
%                vfGaborOrientationRad, vfGaborPhaseRad, ...
%                vfGaborDriftTFHz, vfGaborRotationHz)

% Author: Dylan Muir <muir@hifo.uzh.ch>
% Created: 9th November, 2012

% -- Persistent arguments

persistent PGFS_CACHE PGFS_cmfGratings PGFS_cmfMasks PGFS_vnTexIDs; %#ok<PUSE>


% -- Default arguments

DEF_fGaborOrientationRad = 0;
DEF_fGaborPhaseRad = 0;
DEF_fGaborDriftTFHz = 0;
DEF_fGaborRotationHz = 0;
DEF_fGaborContrast = 1;


% - Deal with blank times

if (~exist('vtBlankTime', 'var') || isempty(vtBlankTime))
   tStartTime = 0;
   vtBlankTime = 0;
   bPresentStimulus = false;
else
   % -- Blank screen
   tStartTime = Screen('Flip', hWindow);
   
   bPresentStimulus = true;
end

if (numel(vtBlankTime) > 1)
   tFlipTime = vtBlankTime(2) + vtBlankTime(1);
else
   tFlipTime = tStartTime + vtBlankTime(1);
end



%% -- Check arguments

if (nargin < 9)
   disp('*** PresentGaborFieldStimulus: Incorrect usage');
   help PresentGaborFieldStimulus;
   return;
end

nNumGabors = size(mfGaborLocsDeg, 1);

if (isscalar(vfGaborWidthDeg))
   vfGaborWidthDeg = repmat(vfGaborWidthDeg, [nNumGabors 1]);

elseif (numel(vfGaborWidthDeg) ~= nNumGabors)
   SSlog('*** PresentGaborFieldStimulus: ''vfGaborWidthDeg'' must be scalar, or the same number of Gabors as ''mfGaborLocsDeg''');
   return;
end

if (~exist('vfGaborOrientationRad', 'var') || isempty(vfGaborOrientationRad))
   vfGaborOrientationRad = DEF_fGaborOrientationRad;
end

if (isscalar(vfGaborOrientationRad))
   vfGaborOrientationRad = repmat(vfGaborOrientationRad, [nNumGabors 1]);

elseif (numel(vfGaborOrientationRad) ~= nNumGabors)
   SSlog('*** PresentGaborFieldStimulus: ''vfGaborOrientationRad'' must be scalar, or the same number of Gabors as ''mfGaborLocsDeg''');
   return;
end

if (~exist('vfGaborPhaseRad', 'var') || isempty(vfGaborPhaseRad))
   vfGaborPhaseRad = DEF_fGaborPhaseRad;
end

if (isscalar(vfGaborPhaseRad))
   vfGaborPhaseRad = repmat(vfGaborPhaseRad, [nNumGabors 1]);

elseif (numel(vfGaborPhaseRad) ~= nNumGabors)
   SSlog('*** PresentGaborFieldStimulus: ''vfGaborPhaseRad'' must be scalar, or the same number of Gabors as ''mfGaborLocsDeg''');
   return;
end

if (isscalar(vfGaborSFCPD))
   vfGaborSFCPD = repmat(vfGaborSFCPD, [nNumGabors 1]);

elseif (numel(vfGaborSFCPD) ~= nNumGabors)
   SSlog('*** PresentGaborFieldStimulus: ''vfGaborSFCPD'' must be scalar, or the same number of Gabors as ''mfGaborLocsDeg''');
   return;
end

if (~exist('vfGaborDriftTFHz', 'var') || isempty(vfGaborDriftTFHz))
   vfGaborDriftTFHz = DEF_fGaborDriftTFHz;
end

if (isscalar(vfGaborDriftTFHz))
   vfGaborDriftTFHz = repmat(vfGaborDriftTFHz, [nNumGabors 1]);

elseif (numel(vfGaborDriftTFHz) ~= nNumGabors)
   SSlog('*** PresentGaborFieldStimulus: ''vfGaborDriftTFHz'' must be scalar, or the same number of Gabors as ''mfGaborLocsDeg''');
   return;
end

if (~exist('vfGaborRotationHz', 'var') || isempty(vfGaborRotationHz))
   vfGaborRotationHz = DEF_fGaborRotationHz;
end

if (isscalar(vfGaborRotationHz))
   vfGaborRotationHz = repmat(vfGaborRotationHz, [nNumGabors 1]);

elseif (numel(vfGaborRotationHz) ~= nNumGabors)
   SSlog('*** PresentGaborFieldStimulus: ''vfGaborRotationHz'' must be scalar, or the same number of Gabors as ''mfGaborLocsDeg''');
   return;
end

if (~exist('vfGaborContrast', 'var') || isempty(vfGaborContrast))
   vfGaborContrast = DEF_fGaborContrast;
end

if (isscalar(vfGaborContrast))
   vfGaborContrast = repmat(vfGaborContrast, [nNumGabors 1]);

elseif (numel(vfGaborContrast) ~= nNumGabors)
   SSlog('*** PresentGaborFieldStimulus: ''vfGaborContrast'' must be scalar, or the same number of Gabors as ''mfGaborLocsDeg''');
   return;
end

if (~exist('vfGaborPhaseRad', 'var') || isempty(vfGaborPhaseRad))
   vfGaborPhaseRad = DEF_fGaborPhaseRad;
end

if (isscalar(vfGaborPhaseRad))
   vfGaborPhaseRad = repmat(vfGaborPhaseRad, [nNumGabors 1]);

elseif (numel(vfGaborPhaseRad) ~= nNumGabors)
   SSlog('*** PresentGaborFieldStimulus: ''vfGaborPhaseRad'' must be scalar, or the same number of Gabors as ''mfGaborLocsDeg''');
   return;
end


% -- Determine where the Gabors will be presented

mfGaborLocsPix = mfGaborLocsDeg .* fPixelsPerDeg;
vnScreenRect = Screen('Rect', hWindow');


% -- Find cached params for each Gabor

% - Make a shader
AssertGLSL;
hGlsl = MakeTextureDrawShader(hWindow, 'SeparateAlphaChannel');

% - Get 'white' and 'black' colours
nWhite = WhiteIndex(hWindow);
nBlack = BlackIndex(hWindow);
nGrey = (nBlack+nWhite)/2;

vnGaborCacheLoc = nan(nNumGabors, 1);
vnGaborTexHandle = nan(nNumGabors, 1);
cvnGaborDestRects = cell(nNumGabors, 1);

for (nGabor = 1:nNumGabors)
   % - Set up relevant params for this Gabor
   sStimParams.fSpatialFreqCPD = vfGaborSFCPD(nGabor);
   sStimParams.fContrast = vfGaborContrast(nGabor);
   sStimParams.fGaborWidthDeg = vfGaborWidthDeg(nGabor);
   
   % - Check cache
   [vnGaborCacheLoc(nGabor), bCacheHit, bCacheCollision, sCachedParams, PGFS_CACHE] = ...
      ParamCache(sStimParams, PGFS_CACHE);
   
   % - Do we need to generate these params?
   if (~bCacheHit)
      % - Yes, so generate a sine-wave texture
      fStimSizeDeg = max(2.*sStimParams.fGaborWidthDeg, 4./sStimParams.fSpatialFreqCPD);
      [mfGrating, cmfLocDeg] = GenerateSineGratingStimulus( fStimSizeDeg * [1 1], ...
                                                            sStimParams.fSpatialFreqCPD, fPixelsPerDeg);
      vnStimSizePix = size(mfGrating);
      
      % - Modulate contrast
      mfGrating = (mfGrating - 0.5) * sStimParams.fContrast;
      
      % - Multiply by Gaussian mask to make single Gabor
      mfD2 = cmfLocDeg{1}.^2 + cmfLocDeg{2}.^2;
      mfMask = exp(-mfD2 / 2 / (sStimParams.fGaborWidthDeg/4).^2);

      % - Store textures and generate texture handles
      PGFS_cmfGratings{vnGaborCacheLoc(nGabor)} = mfGrating;
      PGFS_cmfMasks{vnGaborCacheLoc(nGabor)} = mfMask;

      % - We need to generate the texture
      PGFS_vnTexIDs(vnGaborCacheLoc(nGabor)) = nan;
   else
      vnStimSizePix = size(PGFS_cmfGratings{vnGaborCacheLoc(nGabor)});
   end
   
   % - Do we need to re-generate the texture?
   vnTextures = Screen('Windows');
   if (~ismember(PGFS_vnTexIDs(vnGaborCacheLoc(nGabor)), vnTextures))
      tfTexture = PGFS_cmfGratings{vnGaborCacheLoc(nGabor)};
      tfTexture(:, :, 2) = PGFS_cmfMasks{vnGaborCacheLoc(nGabor)};

      PGFS_vnTexIDs(vnGaborCacheLoc(nGabor)) = Screen('MakeTexture', hWindow, tfTexture, [], [], 2, [], hGlsl); %#ok<AGROW>
   end
   
   % - Remember the texture handle
   vnGaborTexHandle(nGabor) = PGFS_vnTexIDs(vnGaborCacheLoc(nGabor));
   
   % - Generate Gabor destination rectangles
   vnGaborRect = CenterRect([0 0 vnStimSizePix], vnScreenRect);
   cvnGaborDestRects{nGabor} = OffsetRect(vnGaborRect, mfGaborLocsPix(nGabor, 1), mfGaborLocsPix(nGabor, 2));
end


% - Preload textures
Screen('PreloadTextures', hWindow, PGFS_vnTexIDs(vnGaborCacheLoc));

% -- Present the stimulus

% - Don't present stimulus if blank time was empty
if (~bPresentStimulus)
    tLastPresentation = [];
    bBlewBlankTime = [];
    bBlewFrameRate = [];
    return;
end

bFirst = true;
bBlewFrameRate = false;
tCurrTime = -inf;
tLastFlipTime = nan;
tCurrFrameDuration = tFrameDuration;
tEndTime = inf;
nCurrFrame = 1;
bContinue = true;

vfCurrGaborPhase = vfGaborPhaseRad;

while (tCurrTime <= tEndTime)
   % - Set blend mode to allow superimposition
   Screen('FillRect', hWindow, nGrey);
   Screen('BlendFunction', hWindow, GL_SRC_ALPHA, GL_ONE);%_MINUS_SRC_ALPHA);
   
   % - Loop over all Gabors and draw them into the window buffer
   for (nGabor = 1:nNumGabors) %#ok<FORPF>
      % - Draw a single Gabor onto the screen, centred, shifted and rotated
      nThisGaborTex = vnGaborTexHandle(nGabor);
      vnThisGaborRect = cvnGaborDestRects{nGabor};
      fThisGaborShiftPix = mod(vfCurrGaborPhase(nGabor), 2*pi);
      fThisGaborShiftPix = fThisGaborShiftPix ./ 2/pi ./ vfGaborSFCPD(nGabor) .* fPixelsPerDeg;
      Screen('DrawTexture', hWindow, nThisGaborTex, [], vnThisGaborRect, ...
         vfGaborOrientationRad(nGabor)*180/pi, [], [], [], [], [], ...
         [0 fThisGaborShiftPix 0 0]);
      
      % - Shift and rotate this Gabor
      vfCurrGaborPhase(nGabor) = vfCurrGaborPhase(nGabor) + vfGaborDriftTFHz(nGabor) .* tCurrFrameDuration .* 2*pi;
      vfGaborOrientationRad(nGabor) = vfGaborOrientationRad(nGabor) + vfGaborRotationHz(nGabor) .* tCurrFrameDuration .* 2.*pi;
   end
      
   % - Flip the screen
   [tCurrTime, nul, nul, Missed] = FrameMarkerFlip(hWindow, bFirst, tFlipTime); %#ok<ASGLU>
   
   % - Did we miss any deadlines?
   if (bFirst)
      bBlewBlankTime = Missed > 0;
      tEndTime = tStartTime + tStimDuration + vtBlankTime(1);
      tLastFlipTime = tLastFlipTime - tFrameDuration;
   else
      bBlewFrameRate = bBlewFrameRate | (Missed > 0);
      tCurrFrameDuration = tCurrTime - tLastFlipTime;
   end
   
   bFirst = false;
   
   % - Compute next flip time
   tFlipTime = tFlipTime + tFrameDuration;
   tLastFlipTime = tCurrTime;
end

% - Display a warning if necessary
if (bBlewBlankTime || bBlewFrameRate)
   SSlog('--- PresentGaborFieldStimulus: The desired frame rate or blanking period was not met.\n');
end

% - Return the last stimulation time
tLastPresentation = tCurrTime + tFrameDuration;


% --- END of PresentGaborFieldStimulus.m ---
                              
