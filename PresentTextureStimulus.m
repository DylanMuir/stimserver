
function [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
            PresentTextureStimulus( hWindow, vtBlankTime, tFrameDuration, ...
                                            tStimulusDuration, nNumRepeats, ...
                                            vfSizeDegrees, fGridsPerDegree, fPixelsPerDegree, ...
                                            fDirectionDegrees, fOrientationDegrees, ...
                                            fShiftGridsPerSecond, fJitter, ...
                                            fWidthDegrees, fLengthDegrees, bGabor, ...
                                            fBandwidth, fPhaseoffset, nGridRepetition, ...
                                            fContrast, bAdapt, tAdaptationDuration, ...
                                            fAdaptGridsPerSec, fAdaptContrast, nSeed, vfGreyLevels)
                             
% PresentTexture - FUNCTION Present moving oriented semi-pseudorandomly distributed bar segments
%
% Usage: [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
%             PresentTexture(     hWindow, vtBlankTime, tFrameDuration, ...
%                                             tStimulusDuration, nNumRepeats, ...
%                                             vfSizeDegrees, fGridsPerDegree, fPixelsPerDegree, ...
%                                             fDirectionDegrees, fOrientationDegrees, ...
%                                             fShiftGridsPerSecond, fJitter, ...
%                                             fWidthDegrees, fLengthDegrees, <bGabor>, ...
%                                             <fBandwidth, fPhaseoffset, nGridRepetition>, ...
%                                             <fContrast, tAdaptationDuration>)
% 
% % % % % 'vfSizeDegrees' is a vector [X Y] that defines the size of the entire
% % % % % stimulus, in degrees.  Leave as an empty vector to fill the presentation
% % % % % screen.
% % % % % 'vfMaskPosPix' is a vector [X Y] that defines the mask position relative
% % % % % to the center of the screen.
% % % % % 'tAdaptationDuration' is the duration of the presentation of a standing
% % % % % grating before the grating starts moving.
% % % % % 'bCyclicTF' - 'true' if a cyclic temporal frequency ramp should be
% % % % % presented, i.e. an accelerating sine wave grating.
% % % % % 'fTFBandWidth' defines how the range of cycles per second covered by the
% % % % % temporal frequency cycling.
% % % % % 'tTFRampDuration' determines the time it takes to transform the lowest
% % % % % temporal frequency to the highest.
% % % % % 'tTFTransitionDuration' determines the time of fading out from the high
% % % % % temporal frequency and fading in to the lower temporal frequency.
% 
% Author: Andreas Keller <andi@ini.phys.ethz.ch>,
% Created: 18st August, 2011 (from SineGratingStimulus.m)


% -- Persistent arguments

persistent PSGS_sParams PSGS_nTexID;


% -- Check arguments

if (nargin < 14)
   disp('*** PresentTexture: Incorrect usage');
   help PresentTexture;
   return;
end

if (~exist('vtBlankTime', 'var') || isempty(vtBlankTime))
   tStartTime = 0;
   vtBlankTime = 0;
   bPresentStimulus = false;
else
   % -- Blank screen
   tStartTime = Screen('Flip', hWindow);
   bPresentStimulus = true;
end

if (numel(vtBlankTime) == 1)
   vtBlankTime(2) = vtBlankTime(1);
   vtBlankTime(1) = tStartTime;
end

% -- Check arguments

if (~exist('bGabor', 'var') || isempty(bGabor))
   bGabor = false;
end

if (~exist('fBandwidth', 'var') || isempty(fBandwidth))
   fBandwidth = 1;
end

if (~exist('fPhaseoffset', 'var') || isempty(fPhaseoffset))
   fPhaseoffset = 0;
end

if (~exist('nGridRepetition', 'var') || isempty(nGridRepetition))
   nGridRepetition = 1;
end

if (~exist('fContrast', 'var') || isempty(fContrast))
   fContrast = 1;
end

if (~exist('bAdapt', 'var') || isempty(bAdapt))
   bAdapt = false;
end

if (~exist('tAdaptationDuration','var') || isempty(tAdaptationDuration))
    tAdaptationDuration = 0;
end

if (~exist('fAdaptGridsPerSec','var') || isempty(fAdaptGridsPerSec))
    fAdaptGridsPerSec = 0;
end

if (~exist('fAdaptContrast','var') || isempty(fAdaptContrast))
    fAdaptContrast = 0;
end

mfTexture = [];

if (~exist('nSeed','var') || isempty(nSeed))
    nSeed = 0;
end

if (~exist('vfGreyLevels','var') || isempty(vfGreyLevels))
    vfGreyLevels = [0 0.5];
end

% - Ensure nPixelsPerGrid to be a natural number by adapting fGridsPerDegree

fGridsPerDegree = 1 / round(fPixelsPerDegree / fGridsPerDegree) * fPixelsPerDegree;


% - Do we need to re-generate the stimulus?

if (isempty(PSGS_sParams))
   bGenerateStim = true;

else
   % - Test each relevant parameter
   bGenerateStim = ~isequal(PSGS_sParams.hWindow, hWindow);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.vfSizeDegrees, vfSizeDegrees);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.fGridsPerDegree, fGridsPerDegree);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.fPixelsPerDegree, fPixelsPerDegree);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.fRelativeAngle, fOrientationDegrees - fDirectionDegrees);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.fJitter, fJitter);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.fWidthDegrees, fWidthDegrees);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.fLengthDegrees, fLengthDegrees);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.bGabor, bGabor);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.fBandwidth, fBandwidth);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.fPhaseoffset, fPhaseoffset);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.nGridRepetition, nGridRepetition);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.nSeed, nSeed);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.vfGreyLevels, vfGreyLevels);
end

% -- Generate stimulus

if (bGenerateStim)
   disp('--- PresentTexture: Generating stimulus');
   
   % - Record parameters
   PSGS_sParams = [];
   PSGS_sParams.hWindow = hWindow;
   PSGS_sParams.vfSizeDegrees = vfSizeDegrees;
   PSGS_sParams.fGridsPerDegree = fGridsPerDegree;
   PSGS_sParams.fPixelsPerDegree = fPixelsPerDegree;
   PSGS_sParams.nPixelsPerGrid = fPixelsPerDegree / fGridsPerDegree;
   PSGS_sParams.fRelativeAngle = fOrientationDegrees - fDirectionDegrees;
   PSGS_sParams.fShiftGridsPerSecond = fShiftGridsPerSecond;
   PSGS_sParams.fJitter = fJitter;
   PSGS_sParams.fWidthDegrees = fWidthDegrees;
   PSGS_sParams.fLengthDegrees = fLengthDegrees;
   PSGS_sParams.bGabor = bGabor;
   PSGS_sParams.fBandwidth = fBandwidth;
   PSGS_sParams.fPhaseoffset = fPhaseoffset;   
   PSGS_sParams.nGridRepetition = nGridRepetition;
   PSGS_sParams.fContrast = fContrast;
   PSGS_sParams.bAdapt = bAdapt;
   PSGS_sParams.tAdaptationDuration = tAdaptationDuration;
   PSGS_sParams.fAdaptGridsPerSec = fAdaptGridsPerSec;
   PSGS_sParams.fAdaptContrast = fAdaptContrast;
   PSGS_sParams.nSeed = nSeed;
   PSGS_sParams.vfGreyLevels = vfGreyLevels;

   
   % - Determine how big the stimulus should be, so it can be rotated and
   %   shifted. nGridRepetition determins after how many grids 
      [vnScreenSize(1), vnScreenSize(2)] = Screen('WindowSize', hWindow);
      vfSizeGridsRep = (floor(ceil((sqrt(sum((vnScreenSize/fPixelsPerDegree).^2))...
          .* [1 nGridRepetition]) .* fGridsPerDegree) / 2) * 2 + [1 1]);
      vfSizeDegrees = [vfSizeGridsRep(1), sum(vfSizeGridsRep) + 1] / fGridsPerDegree;


   % - Generate stimulus
   [mfTexture, fPixelsShiftMod] = GenerateTextureStimulus(vfSizeDegrees, fGridsPerDegree, ...
                                            fPixelsPerDegree, PSGS_sParams.nPixelsPerGrid, ...
                                            PSGS_sParams.fRelativeAngle, fJitter, fWidthDegrees, ...
                                            fLengthDegrees, bGabor, fBandwidth, fPhaseoffset, ...
                                            nSeed, vfGreyLevels);

                                        
   % - Convert to screen colour indices
   nBlack = BlackIndex(hWindow);
   nWhite = WhiteIndex(hWindow);
   
   mfTexture=round(mfTexture*nWhite+(1-mfTexture)*nBlack);
  
   PSGS_sParams.mfTexture = mfTexture;
   PSGS_sParams.fPixelsShiftMod = fPixelsShiftMod;
   
   % - Clear texture cache
   PSGS_nTexID = [];
end

% - Create textures, if necessary
if (isempty(PSGS_nTexID))
   bGenerateTexture = true;
else
   % - Test to see if our texture exists
   vnTextures = Screen('Windows');
   
   if (~any(vnTextures == PSGS_nTexID))
      bGenerateTexture = true;
   else
      bGenerateTexture = false;
   end
end

% - Generate a texture, if required
if (bGenerateTexture)   
   % - Check that we can actually use a shader
   AssertGLSL;
   
   % - Generate a shader to use
   hGlsl = MakeTextureDrawShader(hWindow, 'SeparateAlphaChannel');
   
   % - Generate the texture
   PSGS_nTexID = GenerateTextures(PSGS_sParams.mfTexture, hWindow, hGlsl);
end


% -- Present the stimulus using PresentSimpleStimulus

if (~bPresentStimulus)
    tLastPresentation = [];
    bBlewBlankTime = [];
    bBlewFrameRate = [];
    return;
end

fShiftPixelsPerFrame = fShiftGridsPerSecond / fGridsPerDegree * fPixelsPerDegree * tFrameDuration;
fAdaptShiftPixPerFr = fAdaptGridsPerSec / fGridsPerDegree * fPixelsPerDegree * tFrameDuration;

[tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
    PresentSimpleStimulus(hWindow, vtBlankTime, ...
        tFrameDuration, tStimulusDuration, nNumRepeats, ...
        PSGS_nTexID, fShiftPixelsPerFrame, fDirectionDegrees, [], ...
        PSGS_sParams.fPixelsShiftMod, fPixelsPerDegree, [], [], ...
        [], [], fContrast, bAdapt, tAdaptationDuration, ...
        fAdaptShiftPixPerFr, fAdaptContrast);

% --- END of PresentTextureStimulus.m ---
