function StimulusServer_COMMAND(strCommand)

% StimulusServer_COMMAND - FUNCTION Core command execution for the stimulus server
%
% Do not call this function directly.

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 8th September, 2010


% - Decode the stimulus command section
[cellTokens, nCmdPosition] = textscan(strCommand, '%s %s %*[^\n]');

% - Check for a valid stimulus server preamble
if (~isequal(cellTokens{1}{1}, 'SS'))
   disp('--- StimulusServer: Warning: Invalid command read:');
   fprintf(1, '       [%s]\n', strCommand);
   SStalkback('SST CMDERROR');
   
   % - Just ignore the command
   return;
end

% - Check what command we got
switch upper(cellTokens{2}{1})
   case {'PRESENT'}
      % - Present a stimulus
      [sStimulus, hWindow, vtBlankTime, nStimID] = ...
         StimulusServer_DecodeStimulusCommand(strCommand);
      
      % - Error check
      if (isempty(sStimulus))
         SSlog('--- StimulusServer: Warning: Presentation requested for undefined stimulus.\n');
         SSlog('       Command: [%s]\n', strCommand);
         SStalkback('SST CMDERROR');
         
         % - Ignore command
         return;
      end
      
      % - Actually present the stimulus
      SStalkback('SST PRESENT %d', nStimID);
      SSlog(sprintf('Presenting stimulus ID [%d]\n', nStimID));
      
      try
         [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
            PresentArbitraryStimulus(sStimulus, hWindow, vtBlankTime);
         
         % - Record the stimulus results
         STIMSERV_sState.tLastPresentation = tLastPresentation;
         STIMSERV_sState.bBlewBlankTime = bBlewBlankTime;
         STIMSERV_sState.bBlewFrameRate = bBlewFrameRate;
         
      catch errMsg
         % - Couldn't present the stimulus
         SSlog('*** StimulusServer: Error presenting stimulus.\n');
         SSlog('       Offending command: [%s]\n', strCommand);
         SSlog('       Error message: [%s]\n', errMsg.message);
         SStalkback('SST FAILED');
      end
      
   case {'LISTEN_PORT'}
      cellTokens = textscan(strCommand, '%s %s %f %*[^\n]');
      
      if (isempty(cellTokens{3}))
         SSlog('*** StimulusServer: Incorrect LISTEN_PORT command semantics.\n');
         SStalkback('SST CMDERROR');
         
      else
         nOldPort = get(STIMSERV_hUDP, 'LocalPort');
         nNewPort = cellTokens{3};
         
         % - Close UDP channel and re-open with new port
         SSlog('--- StimulusServer: Shifting listen port to [%d].\n', nNewPort);
         fclose(STIMSERV_hUDP);
         
         try
            % - Change port and re-open
            set(STIMSERV_hUDP, 'LocalPort', nNewPort);
            fopen(STIMSERV_hUDP);
            SStalkback('SST LISTEN %d', nNewPort);
            
         catch errMsg
            % - Couldn't switch ports, so try to switch back
            SSlog('*** StimulusServer: Couldn''t switch listen port to [%d].\n', nNewPort);
            SSlog('       Restoring port to [%d].\n', nOldPort);
            
            fclose(STIMSERV_hUDP);
            set(STIMSERV_hUDP, 'LocalPort', nOldPort);
            fopen(STIMSERV_hUDP);
            SStalkback('SST FAILED');
            SStalkback('SST LISTEN %d', nOldPort);
         end
      end
      
   case {'TALKBACK_ADDRESS'}
      cellTokens = textscan(strCommand, '%s %s %s %d %*[^\n]');
      
      % - Try to set new talkback details
      if (isempty(cellTokens{3}) || isempty(cellTokens{4}))
         SSlog('*** StimulusServer: Incorrect TALKBACK_ADDRESS command semantics.\n');
         SStalkback('SST CMDERROR');
         
      else
         StimServerTalkback({cellTokens{3}{1} cellTokens{4}});
      end
      
   case {'LOGGING'}
      cellTokens = textscan(strCommand, '%s %s %s %d %*[^\n]');
      
      % - Try to set new logging details
      if (isempty(cellTokens{3}))
         SSlog('*** StimulusServer: Incorrect LOGGING command semantics.\n');
         SStalkback('SST CMDERROR');
         
      else
         if (isempty(cellTokens{4}))
            oNewLogging = cellTokens{3}{1};
         else
            oNewLogging = {cellTokens{3}{1} cellTokens{4}};
         end
         
         StimServerLogging(oNewLogging);
      end
      
   case {'DESCRIBE_CONFIG'}
      StimulusServerDescribeConfig;
      
   case {'DESCRIBE_STIMULI'}
      StimulusServerDescribeStimuli;

   case {'KILL'}
      % - Kill the server, close everything
      StopStimulusServer('abort');
      return;
      
   case {'SHUTDOWN'}
      % - Shutdown the stimulus server nicely
      StopStimulusServer();
      return;
      
   otherwise
      % - Invalid command
      SSlog('--- StimulusSever: Warning: I don''t understand the command [%s].\n', cellTokens{2}{1});
      SSlog('       Command string: [%s]\n', strCommand);
      SStalkback('SST CMDERROR');
      
      % - Ignore the command
      return;
end

% --- END of StimulusServer_COMMAND.m ---
