function [vnTexIDs] = GenerateTextures(tfImages, hWindow, hShader)

% GenerateTextures - FUNCTION Make textures from all slices of a tensor (good for movies)
%
% Usage: [vnTexIDs] = GenerateTextures(tfImages, hWindow <, hShader>)
%
% 'tfImages' is a tensor, each slice of which will be transformed into a
% B&W texture.  These textures should already be scaled between black and white.
%

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 30th August, 2010

% -- Defaults

DEF_hShader = [];


% -- Check arguments

if (nargin < 2)
   disp('*** GenerateTextures: Incorrect usage');
   help GenerateTextures;
   sca;
   return;
end

if (~exist('hShader', 'var'))
   hShader = DEF_hShader;
end


% -- Generate textures

nNumTextures = size(tfImages, 3);

vnTexIDs = nan(nNumTextures, 1);

for (nTextureID = 1:nNumTextures) %#ok<FORPF>
   vnTexIDs(nTextureID) = Screen('MakeTexture', hWindow, uint8(tfImages(:, :, nTextureID)), [], [], [], [], hShader);
end


% --- END of GenerateTextures.m ---
