function [tfBLN] = GenerateBandLimitedNoiseStimulus(  vfSpatFreqCutoffCPD, vfTempFreqCutoffHz, ...
                                                      vfStimSizeDeg, tStimDuration, fSpatSamplingSPD, tFrameDuration, ...
                                                      tWindowTime, tModulationPeriod, fModulationAmplitude, nSeed, ...
                                                      strAVIFile)
                                                   
% GenerateBandLimitedNoiseStimulus - FUNCTION Generate a band-limited noise stimulus
%
% Usage: [tfBLN] = GenerateBandLimitedNoiseStimulus(  fSpatFreqCutoffCPD, fTempFreqCutoffHz, ...
%                                                     vfStimSizeDeg, tStimDuration, fSpatSamplingSPD, tFrameDuration, ...
%                                                     < tWindowTime, tModulationPeriod, fModulationAmplitude, nSeed, ...
%                                                     strAVIFile>)
%
%                  GenerateBandLimitedNoiseStimulus(  vfSpatFreqCutoffCPD, vfTempFreqCutoffHz, ...
%
% Note: This function uses a MappedTensor to return the (possibly very large)
% stimulus tensor.

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 6th September, 2010

% -- Defaults

DEF_nSeed = 1;
DEF_tWindowTime = 0;
DEF_tModulationPeriod = [];
DEF_fModulationAmplitude = 1;


% -- Check arguments

if (nargin < 6)
   SSlog('*** GenerateBandLimitedNoiseStimulus: Incorrect usage');
   help GenerateBandLimitedNoiseStimulus;
   return;
end

if (~exist('nSeed', 'var') || isempty(nSeed))
   nSeed = DEF_nSeed;
end

if (~exist('tWindowTime', 'var') || isempty(tWindowTime))
   tWindowTime = DEF_tWindowTime;
end

if (~exist('tModulationPeriod', 'var') || isempty(tModulationPeriod))
   tModulationPeriod = DEF_tModulationPeriod;
end

if (~exist('fModulationAmplitude', 'var') || isempty(fModulationAmplitude))
   fModulationAmplitude = DEF_fModulationAmplitude;
end

if (numel(vfSpatFreqCutoffCPD) == 1)
   vfSpatFreqCutoffCPD = [0 vfSpatFreqCutoffCPD];
end

if (numel(vfTempFreqCutoffHz) == 1)
   vfTempFreqCutoffHz = [0 vfTempFreqCutoffHz];
end


% -- Seed RNG

rand('twister', nSeed); %#ok<RAND>


%%

% fSpatFreqCutoffCPD = 1/20;
% fTempFreqCutoffHz = 1;
% vfStimSizeDeg = [20 20];
% tStimDuration = 10;
% fSpatSamplingSPD = 4;
% tFrameDuration = 40e-3;
% tWindowTime = 1;
% nSeed = 1;


%% -- Set up parameters

% - Determine stimulus size and spatial filter size
vnStimSize = round(vfStimSizeDeg .* fSpatSamplingSPD);
vnStimSize(3) = round(tStimDuration / tFrameDuration);
vnSFiltSize = 2.^ [nextpow2(vnStimSize(1)) nextpow2(vnStimSize(2)) nextpow2(vnStimSize(3))];

% - Construct space and spatial frequency sampling matrices
vfxDeg = linspace(-vfStimSizeDeg(1)/2, vfStimSizeDeg(1)/2, vnStimSize(1));
vfyDeg = linspace(-vfStimSizeDeg(2)/2, vfStimSizeDeg(2)/2, vnStimSize(2));
[mfXDeg, mfYDeg] = meshgrid(vfxDeg, vfyDeg);

vfxSFreq = fSpatSamplingSPD .* ((-vnSFiltSize(1)/2) : (vnSFiltSize(1)/2-1)) ./ vnSFiltSize(1);
vfySFreq = fSpatSamplingSPD .* ((-vnSFiltSize(2)/2) : (vnSFiltSize(2)/2-1)) ./ vnSFiltSize(2);
[mfYSFreq, mfXSFreq] = meshgrid(vfySFreq, vfxSFreq);

% - Construct time and temporal frequency sampling vectors
fFrameRateHz = 1/tFrameDuration;
vftTime = (0:vnStimSize(3)-1) * tFrameDuration;
vftFreq = fFrameRateHz .* ((-vnSFiltSize(3)/2) : (vnSFiltSize(3)/2-1)) ./ vnSFiltSize(3);


% -- Generate a spatial band-pass filter

mfSpatFilter = zeros(vnSFiltSize(1:2));
mfSpatFilter(sqrt((mfXSFreq.^2) + (mfYSFreq.^2)) >= vfSpatFreqCutoffCPD(1)) = 1;
mfSpatFilter(sqrt((mfXSFreq.^2) + (mfYSFreq.^2)) > vfSpatFreqCutoffCPD(2)) = 0;
mfSpatFilter = ifftshift(mfSpatFilter);


% -- Generate a temporal low-pass filter

vftTempFilter = zeros(1, vnSFiltSize(3));
vftTempFilter(abs(vftFreq) >= vfTempFreqCutoffHz(1)) = 1;
vftTempFilter(abs(vftFreq) > vfTempFreqCutoffHz(2)) = 0;
vftTempFilter = ifftshift(vftTempFilter);


% -- Generate sinusoidal window

vftWindowRad = vftTime * pi / tWindowTime;
vftWindowRad(vftWindowRad > pi) = pi;
vftWindowRad = min(vftWindowRad, vftWindowRad(end:-1:1)); %#ok<NODEF>
vftWindow = (-cos(vftWindowRad) + 1) / 2;


% -- Generate a sinusoidal modulation trace

if (isempty(tModulationPeriod))
   vftModulation = fModulationAmplitude * ones(size(vftTime));
else
   vftModulationRad = mod(vftTime * 2*pi / tModulationPeriod, 2*pi);
   vftModulation = (-cos(vftModulationRad) + 1) / 2 * fModulationAmplitude;
end

%% -- Generate noise and filter it

% - Generate white noise movie
SSlog('--- GenerateBandLimitedNoiseStimulus: Allocating space for stimulus...\n');
tfBLN = MappedTensor(vnStimSize, 'Class', 'single');

SSlog('--- GenerateBandLimitedNoiseStimulus: Generating white noise...\n');
SliceFunction(tfBLN, @()(randn(vnStimSize(1:2))), 3);

% - Perform spatial filtering
SSlog('--- GenerateBandLimitedNoiseStimulus: Performing spatial filtering...\n');
SliceFunction(tfBLN, @filt, 3, [], mfSpatFilter, vnSFiltSize, vnStimSize);

% - Perform temporal filtering
SSlog('--- GenerateBandLimitedNoiseStimulus: Performing temporal filtering...\n');
SliceFunction(tfBLN, @tfilt, 2, [], repmat(permute(vftTempFilter, [1 3 2]), vnStimSize(1), 1), vnSFiltSize, vnStimSize);

% - Normalise each stimulus frame to the global limits
SSlog('--- GenerateBandLimitedNoiseStimulus: Normalising stimulus...\n');

vfMin = SliceFunction(tfBLN, @(mfMat)(min(mfMat(:))), 3, [1 1]);
fMin = min(vfMin(:));
tfBLN = tfBLN - fMin;

vfMax = SliceFunction(tfBLN, @(mfMat)(max(mfMat(:))), 3, [1 1]);
fMax = max(vfMax(:));
tfBLN = tfBLN ./ fMax - 0.5;

% - Perform temporal windowing
if (tWindowTime > 0)
   SSlog('--- GenerateBandLimitedNoiseStimulus: Performing temporal windowing...\n');
   fhWindow = @(mfMat, nIndex)(mfMat .* vftWindow(nIndex));
   SliceFunction(tfBLN, fhWindow, 3);
end

% - Perform contrast modulation
if ((~isempty(tModulationPeriod) && (tModulationPeriod > 0)) || (fModulationAmplitude ~= 1))
   SSlog('--- GenerateBandLimitedNoiseStimulus: Performing contrast modulation...\n');
   fhModulate = @(mfMat, nIndex)(mfMat .* vftModulation(nIndex));
   SliceFunction(tfBLN, fhModulate, 3);
   SliceFunction(tfBLN, @clip, 3);
end

% -- Write stimulus to disk, if requested

if (exist('strAVIFile', 'var'))
   % - Create a new AVI file
   try
      % - Attempt to use the new videoWriter class
      hAVIFile = VideoWriter(strAVIFile, 'Uncompressed AVI');
      hAVIFile.FrameRate = 1/tFrameDuration;
      open(hAVIFile);
      bUseVideoWriter = true;

   catch %#ok<CTCH>
      % - Fall back to the old avifile functioj
      hAVIFile = avifile(strAVIFile, 'compression', 'None', 'fps', 1/tFrameDuration);
      bUseVideoWriter = false;
   end
      
   
   % - Write each frame in turn
   SSlog('--- GenerateBandLimitedNoiseStimulus: Writing stimulus movie... ');
   fprintf(1, '[%6.2f%%]', 0);
   for (nFrame = 1:vnStimSize(3))
      if (bUseVideoWriter)
         writeVideo(hAVIFile, clip(tfBLN(:, :, nFrame)') + 0.5);
      else
         hAVIFile = addframe(hAVIFile, round((tfBLN(:, :, nFrame)' + 0.5) * 255));
      end
      fprintf(1, '\b\b\b\b\b\b\b\b%6.2f%%]', nFrame / vnStimSize(3) * 100);
   end
   fprintf(1, '\b\b\b\b\b\b\b\b%6.2f%%]\n', 100);
   
   % - Close the AVI file
   close(hAVIFile);
end


% --- Utility functions ---

function [mfClippedMat] = clip(mfMat)
mfClippedMat = mfMat;
mfClippedMat(mfClippedMat<-0.5) = -0.5;
mfClippedMat(mfClippedMat>0.5) = 0.5;

function [mfFiltMat] = filt(mfMat, nul, mfSpatFilt, vnFiltSize, vnMatSize)
mfFiltMat = ifft2(fft2(mfMat, vnFiltSize(1), vnFiltSize(2)) .* mfSpatFilt, 'symmetric');
mfFiltMat = mfFiltMat(1:vnMatSize(1), 1:vnMatSize(2));

function [mfFiltMat] = tfilt(mfMat, nul, mfTempFilt, vnFiltSize, vnMatSize)
mfFiltMat = real(ifft(fft(mfMat, vnFiltSize(3), 3) .* mfTempFilt, [], 3, 'symmetric'));
mfFiltMat = mfFiltMat(:, :, 1:vnMatSize(3));

% --- END of GenerateBandLimitedNoiseStimulus.m ---
