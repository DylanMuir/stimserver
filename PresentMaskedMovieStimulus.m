function [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
            PresentMaskedMovieStimulus(hWindow, vtBlankTime, ...
                                       tFrameDuration, nNumRepeats, tStimulusDuration, bDrawMask, bInvertMask, fMaskDiameterDeg, vfMaskPosPix, vfMovieSizeDeg, vfPixelsPerDegree, ...
                                       strMovieName, nFitToScreenMode, bCenterMovieOnMask)

% PresentMaskedMovieStimulus - FUNCTION Present a masked movie stimulus
%
% Usage: [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
%             PresentMaskedMovieStimulus(hWindow, vtBlankTime, ...
%                                        tFrameDuration, nNumRepeats, tStimulusDuration, bDrawMask, bInvertMask, fMaskDiameterDeg, vfMaskPosPix, vfMovieSizeDeg, vfPixelsPerDegree, ...
%                                        strMovieName, nFitToScreenMode, bCenterMovieOnMask)
%

% Authors: Dylan Muir and Patricia Molina-Luna
% Created: 26th October, 2010

% -- Persistent arguments

persistent PMMS_CACHE PMMS_sMovieCache PMMS_nMaskTexID;

if (~exist('vtBlankTime', 'var') || isempty(vtBlankTime))
   tStartTime = 0;
   vtBlankTime = 0;
   bPresentStimulus = false;
else
   % -- Blank screen
   tStartTime = Screen('Flip', hWindow);
   
   bPresentStimulus = true;
end

if (numel(vtBlankTime) > 1)
   tFlipTime = vtBlankTime(2) + vtBlankTime(1);
else
   tFlipTime = tStartTime + vtBlankTime(1);
end


% -- Parameters

BUFFER_FRAMES = 10;
BUFFER_SIZE = 100;


% -- Default parameters

DEF_bDrawMask = false;
DEF_bInvertMask = false;
DEF_fMaskDiameterDeg = [];
DEF_vfMaskPosPix = [0 0];
DEF_vfMovieSizeDeg = [];
DEF_nFitToScreenMode = 1;
DEF_bCenterMovieOnMask = false;


% -- Check arguments

if (nargin < 9)
   SSlog('*** PresentMaskedMovieStimulus: Incorrect usage\n');
   help PresentMaskedMovieStimulus;
   return;
end

if (~exist('bDrawMask', 'var') || isempty(bDrawMask))
   bDrawMask = DEF_bDrawMask;
end

if (~exist('bInvertMask', 'var') || isempty(bInvertMask))
   bInvertMask = DEF_bInvertMask;
end

if (~exist('fMaskDiameterDeg', 'var') || isempty(fMaskDiameterDeg))
   fMaskDiameterDeg = DEF_fMaskDiameterDeg;
end

if (~exist('vfMaskPosPix', 'var') || isempty(vfMaskPosPix))
   vfMaskPosPix = DEF_vfMaskPosPix;
end

if (~exist('vfMovieSizeDeg', 'var'))
   vfMovieSizeDeg = DEF_vfMovieSizeDeg;
end

if (numel(vfPixelsPerDegree) == 1)
   vfPixelsPerDegree(2) = vfPixelsPerDegree(1);
end

if (~exist('nFitToScreenMode', 'var') || isempty(nFitToScreenMode))
   nFitToScreenMode = DEF_nFitToScreenMode;
end

if (~exist('bCenterMovieOnMask', 'var') || isempty(bCenterMovieOnMask))
   bCenterMovieOnMask = DEF_bCenterMovieOnMask;
end


% - Do we need to re-generate the stimulus?

sStimParams.strMovieName = strMovieName;

[nCacheNum, bCacheHit, bCacheCollision, sCachedParams, PMMS_CACHE] = ...
   ParamCache(sStimParams, PMMS_CACHE);

bGenerateStim = ~bCacheHit | bCacheCollision;


% -- Generate stimulus

% - Get screen size
vnScreenRect = Screen('Rect', hWindow');

if (bGenerateStim)
   SSlog('--- PresentMaskedMovieStimulus: Loading movie\n');

   PMMS_sMovieCache{nCacheNum}.strMovieName = strMovieName;
   PMMS_sMovieCache{nCacheNum}.vsMovie = aviread(strMovieName);
   PMMS_sMovieCache{nCacheNum}.sMovieInfo = aviinfo(strMovieName);
   PMMS_sMovieCache{nCacheNum}.vnMovieTexIDs = [];
end

% - Create textures, if necessary
if (~isfield(PMMS_sMovieCache{nCacheNum}, 'vnMovieTexIDs') || ...
    isempty(PMMS_sMovieCache{nCacheNum}.vnMovieTexIDs))
   bGenerateTexture = true;

else
   % - Test to see if our texture exists
   vnTextures = Screen('Windows');
   
   if (~all(ismember(PMMS_sMovieCache{nCacheNum}.vnMovieTexIDs, vnTextures)))
      bGenerateTexture = true;
   else
      bGenerateTexture = false;
   end
end

% - Generate a texture, if required
if (bGenerateTexture)   
   % - Generate the textures
   for (nTexID = PMMS_sMovieCache{nCacheNum}.sMovieInfo.NumFrames:-1:1)
      PMMS_sMovieCache{nCacheNum}.vnMovieTexIDs(nTexID) = Screen('MakeTexture', hWindow, PMMS_sMovieCache{nCacheNum}.vsMovie(nTexID).cdata);
   end
end


% -- Generate the mask texture

nWhite = WhiteIndex(hWindow);
nBlack = BlackIndex(hWindow);
nGrey = round((nWhite + nBlack)/2);

if (nGrey == nWhite)
   nGrey = (nWhite + nBlack)/2;
end

if (bDrawMask)
   nMaskWidth = fMaskDiameterDeg * vfPixelsPerDegree(1);
   nMaskHeight =  fMaskDiameterDeg * vfPixelsPerDegree(2);
   mbMask = Ellipse(nMaskWidth/2, nMaskHeight/2);
   
   if (bInvertMask)
      nBackground = nBlack;
      nMask = nWhite;
   else
      nBackground = nWhite;
      nMask = nBlack;
   end
   
   mnPartialMask = double(mbMask);
   mnPartialMask(mbMask) = nMask;
   mnPartialMask(~mbMask) = nBackground;
   
   vnMaskRect = CenterRect([0 0 size(mbMask)], vnScreenRect);
   vnMaskRect = vnMaskRect + round([vfMaskPosPix vfMaskPosPix]);
   
   tnMask = nGrey * ones(vnScreenRect(4), vnScreenRect(3), 2);
   tnMask(:, :, 2) = nBackground;
   tnMask(vnMaskRect(2)+1:vnMaskRect(4), vnMaskRect(1)+1:vnMaskRect(3), 2) = mnPartialMask;
   
   % - Remove an existing mask texture
   if (exist('PMMS_nMaskTexID', 'var') && ~isempty(PMMS_nMaskTexID) && ismember(PMMS_nMaskTexID, Screen('Windows')))
      try %#ok<TRYNC>
         Screen('Close', PMMS_nMaskTexID);
      end
   end
   
   % - Generate the mask texture
   PMMS_nMaskTexID = Screen('MakeTexture', hWindow, tnMask);
end


% -- Try to pre-load textures

Screen('PreloadTextures', hWindow, PMMS_sMovieCache{nCacheNum}.vnMovieTexIDs);


% -- Determine movie size

vnMovieSize = [PMMS_sMovieCache{nCacheNum}.sMovieInfo.Width PMMS_sMovieCache{nCacheNum}.sMovieInfo.Height];

% - If no movie size was provided, measure from the movie information
if (isempty(vfMovieSizeDeg))
   vfMovieSizeDeg = vnMovieSize ./ mean(vfPixelsPerDegree);
end

% - Estimate scaling parameters
vnFitToWidth = vnMovieSize ./ vnMovieSize(1) .* vnScreenRect(3);
vnFitToHeight = vnMovieSize ./ vnMovieSize(2) .* vnScreenRect(4);

switch nFitToScreenMode
   case 0
      % - No scaling
      vnMoviePos = [0 0 round(vfMovieSizeDeg .* mean(vfPixelsPerDegree))];
      vnMoviePos = CenterRect(vnMoviePos, vnScreenRect);
      
   case 1
      % - Show entire movie frame, fit to screen with no clipping
      vnMoviePos = [0 0 0 0];
      
      if (vnFitToWidth(2) > vnScreenRect(4))
         vnMoviePos(3:4) = vnFitToHeight;
      else
         vnMoviePos(3:4) = vnFitToWidth;
      end
      
      vnMoviePos = CenterRect(vnMoviePos, vnScreenRect);
      
   case 2
      % - Fill entire screen (clip movie)
      vnMoviePos = [0 0 0 0];
      
      if (vnFitToWidth(2) < vnScreenRect(4))
         vnMoviePos(3:4) = vnFitToHeight;
      else
         vnMoviePos(3:4) = vnFitToWidth;
      end
      
   otherwise
      SSlog('*** PresentMaskedMovieStimulus: Invalid fit to screen mode specified.');
end

% - Shift the movie to the center of the mask
if (bCenterMovieOnMask)
   vnMoviePos = OffsetRect(vnMoviePos, vfMaskPosPix(1), vfMaskPosPix(2));
end


% -- Present stimulus

% - Don't present stimulus if blank time was empty
if (~bPresentStimulus)
    tLastPresentation = [];
    bBlewBlankTime = [];
    bBlewFrameRate = [];
    return;
end

for (nStimNumber = 1:nNumRepeats)
   bFirst = true;
   bBlewFrameRate = false;
   tCurrTime = -inf;
   tEndTime = inf;
   nCurrFrame = 1;
   bContinue = true;
   
   while (bContinue)
      % - Draw movie frame with alpha=1
      Screen('Blendfunction', hWindow, GL_ONE, GL_ZERO);
      Screen('DrawTexture', hWindow, PMMS_sMovieCache{nCacheNum}.vnMovieTexIDs(nCurrFrame), [], vnMoviePos);
      
      %  - Draw the mask
      if (bDrawMask)
         Screen('Blendfunction', hWindow, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
         Screen('DrawTexture', hWindow, PMMS_nMaskTexID);
      end
      
      % - Flip the screen
      [tCurrTime, nul, nul, Missed] = FrameMarkerFlip(hWindow, bFirst, tFlipTime); %#ok<ASGLU>
      
      % - Did we miss any deadlines?
      if (bFirst)
         bBlewBlankTime = Missed > 0;
         if (~isempty(tStimulusDuration))
             tEndTime = tStartTime + tStimulusDuration*nStimNumber + vtBlankTime(1);
         end
      else
         bBlewFrameRate = bBlewFrameRate | (Missed > 0);
      end
      
      bFirst = false;
      
      % - Compute next flip time
      tFlipTime = tCurrTime + tFrameDuration;
      
      % - Move to next movie frame
      nCurrFrame = nCurrFrame + 1;
      
      % - Should we continue presentation?
      if (tFlipTime > tEndTime)
         bContinue = false;
      end
      
      if (nCurrFrame > PMMS_sMovieCache{nCacheNum}.sMovieInfo.NumFrames)
         bContinue = false;
      end
   end
   
   % - Display a warning if necessary
   if (bBlewBlankTime || bBlewFrameRate)
      SSlog('--- PresentMaskedMovieStimulus: The desired frame rate or blanking period was not met.\n');
   end
end

% - Return the last stimulation time
tLastPresentation = tCurrTime + tFrameDuration;


% --- END of PresentMaskedMovieStimulus.m ---
