function [mfGrating1 mfGrating2] = GenerateOscillatingGratingStimulus(vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, ...
                                    fBarWidthDegrees, bOscillateOutOfPhase, vfStimulusColours)

% MakeOscillatingGrating - Compute an upright square grating for use as a texture
%
% Usage: [mfGrating1 mfGrating2] = GenerateOscillatingGratingStimulus(vfSizeDegrees, fCyclesPerPixel)
%        [mfGrating1 mfGrating2] = GenerateOscillatingGratingStimulus(vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree ...
%                                      <, fBarWidthDegrees, bOscillateOutOfPhase, vfStimulusColours>)
%
% 'vfSizeDegrees' is a two-elelment vector [N M], defining the size of the
% grating texture to be NxM degrees.  'fCyclesPerPixel' or 'fCyclesPerDegree'
% and 'fPixelsPerDegree' define the spatial frequency of the grating.
%
% 'mfGrating1' and 'mfGrating2' will be a matlab matrix containing a grating 
% stimulus.  The grating values will be 0 (black), 0.5 (grey) and 1 (white).
%
% Make it big enough that you can shift it around on the screen without seeing
% the edges of the texture.

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 26th August, 2010

% -- Defaults

DEF_fPixelsPerDegree = 1;
DEF_bOscillateOutOfPhase = false;
DEF_vfStimulusColours = [0.5 0 1];


% -- Check arguments

if (nargin < 2)
   disp('*** GenerateOscillatingGratingStimulus: Incorrect usage');
   help GenerateOscillatingGratingStimulus;
   return;
end

if (~exist('fPixelsPerDegree', 'var') || isempty(fPixelsPerDegree))
   fPixelsPerDegree = DEF_fPixelsPerDegree;
end

if (~isscalar(fPixelsPerDegree))
   fPixelsPerDegree = nanmean(fPixelsPerDegree(:));
end

if (~exist('bOscillateOutOfPhase', 'var') || isempty(bOscillateOutOfPhase))
   bOscillateOutOfPhase = DEF_bOscillateOutOfPhase;
end

if (~exist('vfStimulusColours', 'var') || isempty(vfStimulusColours))
   vfStimulusColours = DEF_vfStimulusColours;
end


% - Compute stimulus size
vnSize = round(vfSizeDegrees .* fPixelsPerDegree);


% -- Make the grating

% - Fill with background field colour
vfGrating1 = vfStimulusColours(1) * ones(1, vnSize(2));
vfGrating2 = vfStimulusColours(1) * ones(1, vnSize(2));

% - Determine bar duty-cycle
if (~exist('fBarWidthDegrees', 'var') || isempty(fBarWidthDegrees))
   % - Default to a 0.5 duty-cycle
   fBarWidthDegrees = 1/(2*fCyclesPerDegree);
end

% - Construct bars
fPixelsPerFlip = (fPixelsPerDegree / fCyclesPerDegree);
fBarWidthPixels = fBarWidthDegrees * fPixelsPerDegree;
vnStarts = 1:fPixelsPerFlip:vnSize(2);
vnEnds = round(vnStarts + fBarWidthPixels);
vnStarts = round(vnStarts);

% - Reshape to start/end pairs
mnBars1 = [vnStarts' vnEnds'];

% - Generate secondary frame
if (bOscillateOutOfPhase)
   vnStarts = fPixelsPerFlip/2:fPixelsPerFlip:vnSize(2);
   vnEnds = round(vnStarts + fBarWidthPixels);
   vnStarts = round(vnStarts);
   mnBars2 = [vnStarts' vnEnds'];
else
   mnBars2 = mnBars1;
end

nNumBars1 = size(mnBars1, 1);
nNumBars2 = size(mnBars2, 1);

% - Fill bars with defined stimulus colours
for (nBar = 1:nNumBars1)
   vfGrating1(mnBars1(nBar, 1):mnBars1(nBar, 2)) = vfStimulusColours(2);
end

for (nBar = 1:nNumBars2)
   vfGrating2(mnBars2(nBar, 1):mnBars2(nBar, 2)) = vfStimulusColours(3);
end

% - Make sure the gratings are clipped to the desired size
vfGrating1 = vfGrating1(1:vnSize(2));
vfGrating2 = vfGrating2(1:vnSize(2));

% - Replicate base bars to make gratings
mfGrating1 = repmat(vfGrating1, vnSize(1), 1);
mfGrating2 = repmat(vfGrating2, vnSize(1), 1);

% --- END of MakeOscillatingGrating.m ---

