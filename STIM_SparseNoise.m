function [sStimulus] = STIM_SparseNoise(varargin)

% STIM_SparseNoise - Stimulus object
%
% Usage: [sStimulus] = STIM_SparseNoise( tFrameDuration, nNumRepeats, ...
%                                        vfUnitSizeDegrees, vnNumUnits, fPixelsPerDegree, ...
%                                        nSeed, nNumFrames, vfPixelValues)

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 26th October, 2010

%- Deal out parameters from argument list
[tFrameDuration, nNumRepeats, ...
   vfUnitSizeDegrees, vnNumUnits, fPixelsPerDegree, ...
   nSeed, nNumFrames, vfPixelValues] = DealParamsOrNaN(varargin{:});

% - Group parameters
cPresentationParams = {tFrameDuration, nNumRepeats};
cStimParams = {vfUnitSizeDegrees, vnNumUnits, fPixelsPerDegree, nSeed, nNumFrames, vfPixelValues};

% - Trim off NANs
cPresentationParams = cPresentationParams(~cellfun(@isnan_empty, cPresentationParams));
cStimParams = cStimParams(~cellfun(@isnan_empty, cStimParams));

% - Build stimulus structure                 
sStimulus = CreateStimulusStructure(@PresentSparseNoiseStimulus, ...
               cPresentationParams, cStimParams, @calc_duration, ...
               @DescribeSparseNoiseStimulus);

            
function vbIsNAN = isnan_empty(voThing)

vbIsEmpty = isempty(voThing);

vbIsNAN = false(size(vbIsEmpty));
vbIsNAN(~vbIsEmpty) = isnan(voThing(~vbIsEmpty));


function tDuration = calc_duration(sStim, tBlank)

% - Determine number of stimulus pixel values
if ((numel(sStim.cStimulusArguments) < 6) || isempty(sStim.cStimulusArguments{6}))
   % - By default, [0.5 0 1] meaning two pixel values on a grey background
   nNumPixVals = 2;
else
   % - Another list of stimulus pixel values was supplied
   nNumPixVals = numel(sStim.cStimulusArguments{6})-1;
end

% - How many seeds were provided for the stimulus?
if ((numel(sStim.cStimulusArguments) < 4) || isempty(sStim.cStimulusArguments{4}))
   % - By default, a single seed
   nNumSeeds = 1;
else
   % - A specific list of seeds was provided
   nNumSeeds = numel(sStim.cStimulusArguments{4});
end

% - Determine total number of frames in a single stimulus presentation
if ((numel(sStim.cStimulusArguments) < 5) || isempty(sStim.cStimulusArguments{5}))
   % - The total number of stimulus locations * the number of stimulus
   % pixel values * the number of seeds
   nNumFrames = prod(sStim.cStimulusArguments{2}) * nNumPixVals * nNumSeeds;
else
   % - The number of frames was constrained
   nNumFrames = sStim.cStimulusParameters{5};
end

% - Total duration is the number of frames * frame rate * number of repeats + blank time
tDuration = nNumFrames * sStim.cPresentationParameters{1} * sStim.cPresentationParameters{2} + tBlank;


% --- END of STIM_SparseNoise.m ---
