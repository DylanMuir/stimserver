function [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
            PresentSparseGratingStimulus( hWindow, vtBlankTime, ...
                                          tFrameDuration, tPixelDuration, nNumRepeats, ...
                                          fUnitWidthDegrees, vnNumUnits, fUnitOverlapProportion, fPixelsPerDegree, ...
                                          fCyclesPerDegree, fBarWidthDegrees, ...
                                          fShiftCyclesPerSec, fBaseAngle, fRotateCyclesPerSec, ...
                                          vnSeed, nNumPixelsToPresent)
                                    
% PresentSparseGratingStimulus - FUNCTION Present a sparse noise square-wave grating stimulus
%
% Usage: [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
%             PresentSparseGratingStimulus(hWindow, vtBlankTime, ...
%                                          tFrameDuration, tPixelDuration, nNumRepeats, ...
%                                          fUnitWidthDegrees, vnNumUnits, fUnitOverlapProportion, fPixelsPerDegree, ...
%                                          fCyclesPerDegree, fBarWidthDegrees, ...
%                                          fShiftCyclesPerSec, fBaseAngle, fRotateCyclesPerSec, ...
%                                          vnSeed, nNumPixelsToPresent)

% -- Persistent arguments

persistent PSGS_sParams PSGS_sSequence;

% -- Check arguments

if (nargin < 9)
   disp('*** PresentSparseGratingStimulus: Incorrect usage');
   help PresentSparseGratingStimulus;
   return;
end


% -- Check arguments

if (isempty(fUnitOverlapProportion))
   fUnitOverlapProportion = 0;
end

if (~exist('fBarWidthDegrees', 'var'))
   fBarWidthDegrees = [];
end

if (~exist('fShiftCyclesPerSec', 'var') || isempty(fShiftCyclesPerSec))
    fShiftCyclesPerSec = 0;
end

if (~exist('fRotateCyclesPerSec', 'var') || isempty(fRotateCyclesPerSec))
    fRotateCyclesPerSec = 0;
end

if (~exist('fBaseAngle', 'var') || isempty(fBaseAngle))
   fBaseAngle = 0;
end

if (~exist('vnSeed', 'var') || isempty(vnSeed))
   vnSeed = 1;
end

if (~exist('nNumPixelsToPresent', 'var'))
   nNumPixelsToPresent = [];
end


% - Do we need to re-generate the stimulus?

if (isempty(PSGS_sParams))
   bGenerateStim = true;

else
   % - Test each relevant parameter
   bGenerateStim = ~isequal(PSGS_sParams.fCyclesPerDegree, fCyclesPerDegree);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.hWindow, hWindow);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.tFrameDuration, tFrameDuration);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.tPixelDuration, tPixelDuration);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.nNumRepeats, nNumRepeats);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.fPixelsPerDegree, fPixelsPerDegree);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.fUnitOverlapProportion, fUnitOverlapProportion);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.fBarWidthDegrees, fBarWidthDegrees);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.fUnitWidthDegrees, fUnitWidthDegrees);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.vnNumUnits, vnNumUnits);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.vnSeed, vnSeed);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.nNumPixelsToPresent, nNumPixelsToPresent);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.fShiftCyclesPerSec, fShiftCyclesPerSec);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.fBaseAngle, fBaseAngle);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.fRotateCyclesPerSec, fRotateCyclesPerSec);
end

% -- Generate stimulus

if (bGenerateStim)
   disp('--- PresentSparseGratingStimulus: Generating stimulus');
   
   % - Record parameters
   PSGS_sParams = [];
   PSGS_sParams.fCyclesPerDegree = fCyclesPerDegree;
   PSGS_sParams.hWindow = hWindow;
   PSGS_sParams.tFrameDuration = tFrameDuration;
   PSGS_sParams.tPixelDuration = tPixelDuration;
   PSGS_sParams.nNumRepeats = nNumRepeats;
   PSGS_sParams.fUnitOverlapProportion = fUnitOverlapProportion;
   PSGS_sParams.fPixelsPerDegree = fPixelsPerDegree;
   PSGS_sParams.fBarWidthDegrees = fBarWidthDegrees;   
   PSGS_sParams.fUnitWidthDegrees = fUnitWidthDegrees;
   PSGS_sParams.fPixelsPerCycle = fPixelsPerDegree / fCyclesPerDegree;
   PSGS_sParams.vnNumUnits = vnNumUnits;
   PSGS_sParams.vnSeed = vnSeed;
   PSGS_sParams.nNumPixelsToPresent = nNumPixelsToPresent;
   PSGS_sParams.fShiftCyclesPerSec = fShiftCyclesPerSec;
   PSGS_sParams.fBaseAngle = fBaseAngle;
   PSGS_sParams.fRotateCyclesPerSec = fRotateCyclesPerSec;
   
   % - Generate pixel stimuli
   vsStimuli = GenerateSparseGratingStimuli(tFrameDuration, tPixelDuration, ...
                                            vnNumUnits, ...
                                            fUnitWidthDegrees, fUnitOverlapProportion, ...
                                            fCyclesPerDegree, fPixelsPerDegree, fBarWidthDegrees, ...
                                            fBaseAngle, fShiftCyclesPerSec, fRotateCyclesPerSec);
   
   % - Generate stimulus sequences
   vnOrder = [];
   for (nSeed = vnSeed(:)')
      rand('twister', nSeed); %#ok<RAND>
      vnOrder = [vnOrder random_order(1:prod(vnNumUnits))];
   end

   % - Take the requested number of pixels
   if ~isempty(nNumPixelsToPresent)
       vnOrder = vnOrder(1:nNumPixelsToPresent);
   end

   % - Generate a sequence stimulus
   PSGS_sSequence = STIM_Sequence(vnOrder, vsStimuli);
end


% -- Present stimulus

if (isempty(vtBlankTime))
    [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
        PresentArbitraryStimulus(PSGS_sSequence, hWindow, []);
else
    PresentArbitraryStimulus(STIM_Blank(vtBlankTime(1)), hWindow, vtBlankTime);
    [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
        PresentArbitraryStimulus(PSGS_sSequence, hWindow, .2);
end

% --- END of PresentSparseGratingStimulus FUNCTION


function vnOrder = random_order(vData)

[nul, vnIndices] = sort(rand(1, numel(vData)));
vnOrder = vData(vnIndices);


% --- END of PresentSparseGratingStimulus.m ---
