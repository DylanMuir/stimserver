%% Sparse Noise Demo with black and white pixels on grey background
% 
% uses StimServer and Psychtoolbox
%
%
% SSL: *** DescribeSparseNoiseStimulus: Incorrect usage
%   SparseNoiseStimulus - STIMULUS A stimulus composed of single active regions, randomly placed on a field, to characterise a simple cell receptive field
%  
%   This stimulus is a uniform field, on which single active regions are placed
%   randomly in turn.  A sequence consisting of all possible active region
%   locations, with a random order, is generated.  The number of different
%   contrasts (or colours) used for the active regions is fully configurable.
%   
%   Presentation parameters:
%  
%       tFrameDuration - The desired duration of a single frame
%       nNumRepeats - The number of times to repeat the stimulus presentation
%          (there for convenience) 
%  
%   Stimulus parameters:
%  
%       vfUnitSizeDegrees - A two-element vector [fXSize fYSize], where 'fXSize' and
%          'fYSize' define the size of each of the active regions (or "units")
%          used in the stimulus.
%       vnNumUnits - A two-element vector [nXSize nYSize], specifying the number
%          of active units used in the stimulus, as a grid of 'nXSize' x 'nYSize'.
%       fPixelsPerDegree - The calibration of the presentation screen, in pixels
%          per degree.
%       vnSeed - A list (optionally a single number) of integers to use as seeds
%          for the pseudo-random sequence, so that the sequence is repeatable.  If
%          a list is provided, then the sequence will consist of a number of
%          sub-sequences equal to the length of this list, where each sub-sequence
%          in turn is the full sparse noise sequence corresponding to each of the
%          seeds in 'vnSeed'.
%       nNumFrames - An optional parameter specifying the total number of frames
%          to generate.  This number must be smaller or equal to the number of
%          frames in the full random sequence.  If provided, a sequence of length
%          'nNumFrames' will be returned, beginning from the first frame in the
%          random sequence.
%       vfPixelValues - A vector specifying the colour indices used to generate
%          the stimulus.  It has the format [nFieldIndex nActive1Index
%          nActive2Index ...]  'nFieldIndex' specifies the colour index of the
%          uniform background field.  The following indices specify the number and
%          colour indices of the active regions.  The default is [0.5 0 1],
%          meaning that black (0) and white (1) active regions are superimposed on
%          a grey (0.5) uniform field.  These values must be provided as indices
%          that PsychToolbox understands, using for example the BlackIndex and
%          WhiteIndex functions.
%
% by kampa@hifo.uzh.ch, 110301
%%
%InitDisplay;

tbaseline               = 1;  % in seconds
tstimulus               = 1;
tinterstimulus          = 1;
fps                     = 30;
%%
% make blank sequence for baseline
sBlank = STIM_Blank(tbaseline);

tFrameDuration          = GetSafeFrameDurations(hWindow, 80e-3); %from InitDisplay.m
nNumRepeats             = 1;
vfUnitSizeDegrees       = 40/fPPD * [1 1];
vnNumUnits              = [12 12];
fPixelsPerDegree        = fPPD;
nSeed                   = 1:20;
nNumFrames              = [];
vfPixelValues           = [nGrey nBlack nWhite];
%%

[sSparse] = STIM_SparseNoise( tFrameDuration, nNumRepeats, ...
                                         vfUnitSizeDegrees, vnNumUnits, fPixelsPerDegree, ...
                                         nSeed, nNumFrames, vfPixelValues);


 
sStimulus = STIM_Sequence([1,2,1], [sBlank sSparse]);

PresentArbitraryStimulus(sStimulus,hWindow,0)

sca;
clear all;