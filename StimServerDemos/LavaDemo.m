% LavaDemo
% plays lavalamp bandpassfiltered noise stimulus
% 
% This is a "lava lamp" stimulus, constructed by spatially and temporally
%   filtering a white noise sequence with bandpass (actually low-pass) filters.
%   
%   Presentation parameters:
%  
%       tFrameDuration - The desired duration of a single frame
%       fPPD - The calibration of the display monitor (pixels per degree)
%       nNumRepeats - The number of times to repeat the presentation of the
%          single band-pass filtered noise stimulus
%  
%   Stimulus parameters:
%  
%       vfSpatFreqCutoffCPD - The cut-off frequencies for the spatial low-pass
%          filter, in cycles per degree.  The arguments are ['fLowCutCPD'
%          'fHighCutCPD'].  If a scalar value is supplied, a low-pass filter will
%          be applied between 0 and the supplied value.
%  
%       vfTempFreqCutoffHz - The cut-off frequencies for the temporal low-pass
%          filter, in Hertz (cycles per second).  The arguments are ['fLowCutHz'
%          'fHighCutHz'].  If a scalar value is supplied, a low-pass filter will
%          be applied between 0 and the supplied value.
%  
%       vfStimSizeDeg - A two-element vector [fXSize fYSize], specifying the
%          desired size of the stimulus, in degrees.  If an empty matrix is
%          provided then the stimulus will fill the presentation window.
%  
%       tStimDuration - The desired stimulus duration, in seconds.
%  
%       fSpatSamplingSPD - The spatial sampling resolution used to generate the
%          stimulus, in samples per degree.  Basically, the white noise movie can
%          end up very large.  By reducing the spatial resolution of the movie we
%          can save memory.  When the movie is presented, it will be scaled up
%          using a bicubic filter by the graphics card.  Since the movie was
%          spatially filtered anyway, you can sub-sample the stimulus without any
%          loss of quality.
%  
%       tWindowTime - An optional parameter specifying that the stimulus movie
%          should gradually increase in contrast from 0% to 100% at the onset of
%          presentation, and decrease gradually from 100% to 0% on stimulus
%          offset, with a duration of 'tWindowTime' seconds for each of the onset
%          and offset.  The default is 0 seconds, meaning that the stimulus begins
%          at 100% contrast.
%  
%       tModulationPeriod - An optional parameter used to modulate the contrast of
%          the stimulus between zero and the maximum, using a sinusoidal profile
%          of modulation.  The period is the time for a full cycle between zero
%          contrast, maximum contrast and back to zero contrast.
%  
%       fModulationAmplitude - An optional parameter used to modify the maximum
%          aparrent contrast of the stimulus.  By default it is set to "1", which
%          implies that the stimulus spans the full range between the darkest and
%          brightest pixel values possible.  Each frame of the stimulus is
%          guaranteed to span this range.  'fModulationAmplitude' is a
%          multiplication factor to this contrast range.  "0" implies zero
%          contrast; greater than 1 implies that the stimulus will be "stretched"
%          to span double the contrast range, but then be clipped to what the
%          screen can display.  Note that this will change the spatial frequency
%          content of the stimulus, but increase the perceived contrast of the
%          stimulus.
%  
%       nSeed - A number used as a random seed, to ensure that each pseudo-random
%          stimulus is repeatable.
%
% 110216 by kampa @hifo.uzh.ch


InitDisplay;


%tFrameDuration = 1/75;
tFrameDuration          = GetSafeFrameDurations(hWindow, 0); 
                            %from InitDisplay.m
display(tFrameDuration);
fPPD                    = fPPD; % from InitDisplay
nNumRepeats             = 1;
vfSpatFreqCutoffCPD     = 1/20;
vfTempFreqCutoffHz      = 4; %from Stryker
vfStimSizeDeg           = [480 480]/fPPD; %same as movies
tStimDuration           = 80; % in sec
fSpatSamplingSPD        = 1;
tWindowTime             = 0; % fade in time and fade out time
tModulationPeriod       = 10;
fModulationAmplitude    = 1.5;
nSeed                   = 7; %to run the same stimulus each time

                                           
                                           
[sStimulus] = STIM_BandLimitedNoise( tFrameDuration, fPPD, nNumRepeats, ...
                                              vfSpatFreqCutoffCPD, vfTempFreqCutoffHz, ...
                                              vfStimSizeDeg, tStimDuration, fSpatSamplingSPD, ...
                                              tWindowTime, tModulationPeriod, fModulationAmplitude, nSeed);
                                         
PresentArbitraryStimulus(sStimulus, hWindow,10);
% blank time set to 5 seconds to allow for calcualtion of stimulus

%sca;
%clear all;
