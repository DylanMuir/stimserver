% tcpDemo.m
% this demo describes how to use a tcp connection to drive the STIMSERVER
% as a slave. The master is usual a labview vi generating the tcp-strings.
%
%
% StartStimulusServer - FUNCTION Start a stimulus server using the matlab instrument control toolbox
%  
%   Usage: StartStimulusServer(sStimuli, nListenPort, hWindow, bTCP, ...)
%          StartStimulusServer(... <, strLogFilename>)
%          StartStimulusServer(... <, {strLogAddress nLogPort <vnTerminator>}>)
%          StartStimulusServer(..., {}, {strTalkbackAddress nTalkbackPort <vnTerminator>})
%  
%   'sStimuli' is a matlab structure array, each element of which defines a
%   stimulus ID starting from 1.  These are the stimuli that will be presented by
%   the stimulus server, on command.  Each stimulus structure must have the form
%   returned by CreateStimulusStructure.
%  
%   'nListenPort' defines the UDP port number that the stimulus server should use
%   to listen for commands.
%  
%   'hWindow' is a handle to an open Psychtoolbox window on which stimuli will be
%   displayed.
%  
%   'bTCP', if true, indicates to use TCP connections.  If false (the default),
%   UDP connections will be used.  TCP can only be used with PNET.
%  
%   'strLogFilename' or {'strLogAddress' 'nLogPort'} define logging options for
%   the sitmulus server.  If 'strLogFilename' is supplied, the server will
%   attempt to write log messages to the specified file.  Log messages will be
%   APPENDED to this file.  If 'strLogAddress' and 'nLogPort' are instead
%   supplied, the server will send log messages over a UDP channel to the
%   specified machine and port 'strLogAddress':'nLogPort'.
%   If the optional parameter 'vnTerminator' is provided, it will override the
%   default system terminator (which is CR/LF).
%  
%   {'strTalkbackAddress' 'nTalkbackPort'} specify that the stimulus server should
%   send status messages over a UDP connection to
%   'strTalkbackAddress':'nTalkbackPort'.
%   If the optional parameter 'vnTerminator' is provided, it will override the
%   default system terminator (which is CR/LF).
%
% 110217 by kampa@hifo.uzh.ch


%% Start server
% needs vsStimuli generated before liek in SquareWaveGratingDemo.m
% InitDisplay;
strExperiment = datestr(now, 'yyyymmdd');
StartStimulusServer(vsStimuli, 5000, hWindow, true, [strExperiment '_stimuli.log.txt']);
sca;
clear all;