%% - Initialise pyschtoolbox, get a window handle in 'hWindow' (custom function)
% - Also set 'fPPD'
InitDisplay;

tFrameDuration = GetSafeFrameDurations(hWindow, 1/30);

%% -- Sparse noise

tPixelDuration = 2;
fUnitWidthDegrees = 15;
fUnitOverlapProportion = 0.4;
fCyclesPerDegree = 1/9;
fBarWidthDegrees = [];
fShiftCyclesPerSec = 1;
fBaseAngle = 0;
fRotateCyclesPerSec = 0.5;
vnNumUnits = [5 5];
vfShiftCenterDeg = [0 0];

vsSparseGratStimuli = GenerateSparseGratingStimuli(tFrameDuration, tPixelDuration, ...
    vnNumUnits, ...
    fUnitWidthDegrees, fUnitOverlapProportion, ...
    fCyclesPerDegree, fPPD, fBarWidthDegrees, ...
    fBaseAngle, fShiftCyclesPerSec, fRotateCyclesPerSec, ...
    vfShiftCenterDeg);

% - Allows you to specify pixel order as a dynamic parameter
sSparseGrating = STIM_Sequence(nan, vsSparseGratStimuli);

% - Configures a fixed pseudorandom sequences
nNumRepeats = 1;
vnSeed = 1;   % - Set a fixed pseudorandom sequence
 
sSparseGratingFixed = STIM_SparseGrating( tFrameDuration, tPixelDuration, nNumRepeats, ...
   fUnitWidthDegrees, vnNumUnits, fUnitOverlapProportion, fPPD, ...
   fCyclesPerDegree, fBarWidthDegrees, ...
   fShiftCyclesPerSec, fBaseAngle, fRotateCyclesPerSec, ...
   vnSeed);

% - Present the sparse grating sequence
% PresentArbitraryStimulus(sSparseGratingFixed, hWindow, 5);
 
 %% - Start the stimulus server
 
vsStimuli = [sSparseGrating sSparseGratingFixed];

strExperiment = datestr(now, 'yyyymmdd');
StartStimulusServer(vsStimuli, 5000, hWindow, true, [strExperiment '_stimuli.log.txt']);
sca;


