% -- Square Wave Grating Demo
%
% uses StimServer and Psychtoolbox
%
% modified from CenterSurroundGratingDemo.m by Patricia
%
%   Usage: [sStimulus] = STIM_SquareGrating( tFrameDuration, tStimulusDuration, nNumRepeats, ...
%                                            vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, ...
%                                            fBarWidthDegrees, fAngleDegrees, fShiftCyclesPerSecond, ...
%                                            fRotateCyclesPerSecond, bDrawMask, bInvertMask, fMaskDiameterDeg, vfMaskPosPix)
% 
%  Presentation parameters:
%  
%       tFrameDuration - The desired duration of a single frame
%       tStimulusDuration - The duration of the entire stimulus
%       nNumRepeats - The number of times to repeat the stimulus presentation
%          (there for convenience) 
%  
%   Stimulus parameters:
%  
%       vfSizeDegrees - A two-element vector [fXSize fYSize], where 'fXSize' and
%          'fYSize' define the size of the stimulus presentation region in degrees.
%          If an empty matrix is provided, the the stimulus will fill the
%          presentation screen.
%       fCyclesPerDegree - The spatial frequency of the grating, in cycles per
%          degree.
%       fPixelsPerDegree - The calibration of the presentation screen, in pixels
%          per degree.
%       fBarWidthDegrees - The width of a single grating bar, in visual degrees.
%          If not provided, then the gratings have a 50% duty cycle.
%       fAngleDegrees - The orientation of the grating in degrees, where 0 degrees
%          is a vertical grating.
%       fShiftCyclesPerSec - The distance to shift the grating during the stimulus
%          presentation, in grating cycles per second.
%       fRotateCyclesPerSec - The amount to rotate the grating during the stimulus
%          presentation, in rotational cycles per second.
%       bDrawMask - 'true' if the stimulus should be masked. Default is
%       'false'.
%       bInvertMask - 'true' if the mask should be inverted. Default is
%       'false'.
%       fMaskDiameterDeg - The diameter size of the circular mask in degrees.
%       vfMaskPosPix - A two-element vector [fXPos fYPos], where 'fXPos' and
%       'fYPos' define the pixel position of the mask relative to the center
%       of the screen. Default is [0 0], the center of the screen.
%
%
% 110215 by kampa@hifo.uzh.ch

% InitDisplay is in VisStim Folder in MatlabScripts
InitDisplay;

tbaseline               = 5;  % in seconds
tstimulus               = 1;
tinterstimulus          = 1;

% make blank sequence for baseline
sBlank = STIM_Blank(tbaseline);

nNumGratings = 8; % equal steps of 8 x 45 degrees = 360 (full turn)

tFrameDuration          = GetSafeFrameDurations(hWindow, 0); %from InitDisplay.m
tStimulusDuration       = tstimulus;        % Seconds
nNumRepeats             = 1;
vfSizeDegrees           = [150 150]; % whole screen; [] causes bug
fCyclesPerDegree        = 2.5 / 600 * fPPD;    % Cycles per degree
fPixelsPerDegree        = fPPD;     % frim InitDisplay.m
fBarWidthDegrees        = [];       % continuous square wave without gaps
fAngleDegrees           = linspace(0, 360, nNumGratings+1); % in steps of 45 degrees
fAngleDegrees           = fAngleDegrees(1:nNumGratings);
fShiftCyclesPerSecond   = 2;        % in Hz temporal frequency
fRotateCyclesPerSecond  = [];       % no rotation
bDrawMask               = false;  % default no mask
bInvertMask             = false;  % default no inverted mask
fMaskDiameterDeg        = 150/fPPD;        % no mask
vfMaskPosPix            = [0 0];    % default is center of screen

% generate 8 sequences for 8 directions of gratings
% fAngleDegrees(nStim) is vector with 8 x 45 degrees steps
for (nStim = 1:nNumGratings)
    % grating sequence
    vsGratings(nStim) = STIM_SquareGrating( tFrameDuration, tStimulusDuration, nNumRepeats, ...
                            vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, ...
                            fBarWidthDegrees, fAngleDegrees(nStim), fShiftCyclesPerSecond, ...
                            fRotateCyclesPerSecond, bDrawMask, bInvertMask, fMaskDiameterDeg, vfMaskPosPix);
end

sStimulus = STIM_Sequence([1:nNumGratings+1 1], [sBlank vsGratings]);

PresentArbitraryStimulus(sStimulus,hWindow,tinterstimulus)

sca;
clear all;
