%% SquareGratingCenterSurrounDemo.m - Demo script for center / surround drifting square wave gratings stimuli

%% Initialise

InitDisplay;

%% Parameters

tFrameDuration = GetSafeFrameDurations(hWindow, 0);      % Use the shortest possible frame duration
tStimulusDuration = 10;                                  % Stimulus duration, in seconds
nNumRepeats = 1;                                         % Number of stimulus repeats
fPixelsPerDegree = fPPD;                                 % Screen calibration (calculated by InitDisplay), in PPD
vfCSCyclesPerDegree = [1/20 1/30];                       % Spatial frequency for [center surround] gratings, in CPD
vfCSBarWidthDegrees = {[] 10};                           % Bar widths for [center surround] gratings, in deg.
vfCSAngleDegrees = [0 185];                              % Grating angles for [center surround] gratings, in deg.
vfCSShiftCyclesPerSecond = [1 0.5];                      % Temporal frequencies for [center surround] gratings, in CPS
vfCSRotateCyclesPerSecond = [0 1/20];                    % Rotation frequencies for [center surround] gratings, in CPS
vfCenterPosOffsetDegrees = [0 0];                        % [x y] Position of stimulus on the screen, offset from center of the screen, in deg.
vfCASRadiusDegrees = {10 5 []};                          % Stimulus radii: [center size, annulus width, surround outer radius] in degrees.  '[]' means 'fill the screen'

%% Generate the stimulus

sSCSG = STIM_SquareCenterSurroundGrating( tFrameDuration, tStimulusDuration, nNumRepeats, ...
                                         fPixelsPerDegree, vfCSCyclesPerDegree, ...
                                         vfCSBarWidthDegrees, vfCSAngleDegrees, ...
                                         vfCSShiftCyclesPerSecond, vfCSRotateCyclesPerSecond, ...
                                         vfCenterPosOffsetDegrees, vfCASRadiusDegrees);
                                      
%% Present the stimulus

PresentArbitraryStimulus(sSCSG, hWindow, 0);

%% Clean up

sca;

% --- END of SquareCenterSurroundDemo.m ---
