%% Initialise the display
InitDisplay;

% -- Grating and plaid stimuli

tFrameDuration = GetSafeFrameDurations(hWindow, 0e-3);
tStimulusDuration = 2;      
nNumRepeats = 1;
vfSizeDegrees = [];
vfCyclesPerDegree = 0.04;
vfShiftCyclesPerSec = 1;
vfRotateCyclesPerSec = 0;
vfContrast = [0.5 0.5];

nNumAngles = 8;
vfBaseAngles = linspace(0, 360, nNumAngles+1);
vfBaseAngles = vfBaseAngles(1:end-1)';
mfAngles = [vfBaseAngles vfBaseAngles; vfBaseAngles-45 vfBaseAngles+45];
clear vsSequence;

for (nStimID = size(mfAngles, 1):-1:1)
    vsSequence(nStimID) = STIM_SquarePlaid(tFrameDuration, tStimulusDuration, nNumRepeats, ...
        vfSizeDegrees, vfCyclesPerDegree, fPPD, ...
        mfAngles(nStimID, :), vfShiftCyclesPerSec, vfRotateCyclesPerSec, ...
        vfContrast);
end

% - Build sequence stimulus
nStartOffset = floor(rand * nNumAngles);
vnOrdering = [circshift(1:nNumAngles, [0 nStartOffset]) nNumAngles + circshift(1:nNumAngles, [0 nStartOffset])];

sSequence = STIM_Sequence(vnOrdering, vsSequence);

% -- Present sequence
PresentArbitraryStimulus(sSequence, hWindow, 4);

disp('Sequence was displayed in this order:');
disp(mfAngles(vnOrdering, :));

sca;

