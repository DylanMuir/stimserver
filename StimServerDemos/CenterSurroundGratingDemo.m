% -- CenterSurround stimulus
InitDisplay;

sBlank = STIM_Blank(5);

tFrame = GetSafeFrameDurations(hWindow, 0);

nNumGratings = 8;
vfAngles = linspace(0, 360, nNumGratings+1);
vfAngles = vfAngles(1:nNumGratings);
tGratingDuration = 2;             % Seconds
fSpatialFreq = 2.5 / 600 * fPPD;    % Cycles per degree
tDriftSpeed = 2;                    % Hz
fCenterDiameterDeg = 150/fPPD;
vfCenterPosDeg = [0 0];
fCenterAngle = 90;

for (nStim = 1:nNumGratings)
   vsGratings(nStim) = STIM_SquareCenterSurroundGrating( tFrame, tGratingDuration, 1, ...
      fPPD, fSpatialFreq, ...
      [], [fCenterAngle vfAngles(nStim)], ...
      tDriftSpeed, [], ...
      vfCenterPosDeg, {fCenterDiameterDeg 2 []});
end

sGratingSeq = STIM_Sequence(1:nNumGratings, vsGratings);

sStimulus = STIM_Sequence(repmat([1 2], 1, 2), [sBlank sGratingSeq]);

PresentArbitraryStimulus(sStimulus,hWindow,0)

sca;
clear all;
