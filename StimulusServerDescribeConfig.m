function StimulusServerDescribeConfig

% StimulusServerDescribeConfig - FUNCTION Send a description of the current configuration to the logging interface
%
% Usage: StimulusServerDescribeConfig

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 22ndSeptember, 2010




% --- END of StimulusServerDescribeConfig.m ---
