function [strDescription] = DescribeOscillatingPlaidStimulus(sStimulus, strIndent)

% OscillatingPlaidStimulus - STIMULUS An square-wave plaid that oscillates in contrast
%
% This stimulus is a grey field, over which aquare bars oscillate between black
% and white.  Two gratings are overlaid to construct a plaid, with an arbitrary
% angle between the bars.
% 
% Presentation parameters:
%
%     tFrameDuration - The desired duration of a single frame
%     tStimulusDuration - The duration of the entire stimulus
%
% Stimulus parameters:
%
%     vfSizeDegrees - A two-element vector [fXSize fYSize], where 'fXSize' and
%        'fYSize define the size of the stimulus presentation region in degrees.
%        If an empty matrix is provided, the the stimulus will fill the
%        presentation screen.
%     fCyclesPerDegree - The spatial frequency of the grating, in cycles per
%        degree.
%     fPixelsPerDegree - The calibration of the presentation screen, in pixels
%        per degree.
%     fAngle1Degrees - The orientation of the first grating in degrees, where 0
%        degrees is a vertical grating.
%     fAngle2Degrees - The orientation of the second grating in degrees, where 0
%        degrees in a vertical grating.
%     fBarWidthDegrees - The width of a single grating bar, in visual degrees.
%        If not provided, then the gratings have a 50% duty cycle.
%     vbOscillateOutOfPhase - A vector of booleans [bOOOPGrating1 bOOOPGrating2],
%        where the two entries determine whether eacg of the two overlaid
%        gratings should oscillate spatiialy out of phase, on every second
%        oscillation.  These parameters have the same sense as for
%        OscillatingGratingStimulus.  If 'true', the corresponding grating bars
%        will be shifted by 180 degrees spatial phase on each subsequent
%        oscillation.  By default, [false false].  Can be supplied as a scalar,
%        in which case the value applies to both overlaid gratings.
%     vfStimulusColours - A vector [fFieldColour fBarColour1 fBarColour2],
%        defining the colours used for the background field and for the two sets
%        of oscillating contrast bars, used for both gratings.

% DescribeOscillatingPlaidStimulus - FUNCTION Describe the parameters for an oscillating plaid stimulus
%
% Usage: [strDescription] = DescribeOscillatingPlaidStimulus(sStimulus <, strIndent>)
%
% Usually called automatically by StimulusServerDescribeStimuli

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 1st October, 2010

% -- Check arguments

if (nargin == 0)
   SSlog('*** DescribeOscillatingPlaidStimulus: Incorrect usage\n');
   help DescribeOscillatingPlaidStimulus;
   return;
end

if (~exist('strIndent', 'var') || isempty(strIndent))
   strIndent = '';
end


% -- Convert parameters to parameter strings

% - Extract parameters from stimulus
[tFrameDuration, tStimulusDuration] = DealParamsOrNaN(sStimulus.cPresentationParameters{:});
[vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, fAngle1Degrees, fAngle2Degrees, ...
   fBarWidthDegrees, vbOscillateOutOfPhase, vfStimulusColours] = ...
   DealParamsOrNaN(sStimulus.cStimulusArguments{:});

strFrameDuration = CheckNanSprintf('%.2f ms', [], tFrameDuration*1e3, tFrameDuration*1e3);
strStimulusDuration = CheckNanSprintf('%.2f s', [], tStimulusDuration, tStimulusDuration);
strSizeDegrees = CheckEmptyNanSprintf('[%.2f x %.2f] deg', '(full screen)', [], vfSizeDegrees, vfSizeDegrees);
strCyclesPerDegree = CheckNanSprintf('%.2f cpd; %.2f dpc', [], fCyclesPerDegree, fCyclesPerDegree, 1/fCyclesPerDegree);
strPixelsPerDegree = CheckNanSprintf('%.2f ppd', [], fPixelsPerDegree, fPixelsPerDegree);
strAngle1Degrees = CheckNanSprintf('%.2f deg', [], fAngle1Degrees, fAngle1Degrees);
strAngle2Degrees = CheckNanSprintf('%.2f deg', [], fAngle2Degrees, fAngle2Degrees);
strBarWidthDegrees = CheckEmptyNanSprintf('%.2f deg', '(0.5 duty cycle)', [], fBarWidthDegrees, fBarWidthDegrees);
strOscillateOutOfPhase = ['[' CheckEmptyNanSprintf(' %d', '(off)', '(dynamic or off)', vbOscillateOutOfPhase, vbOscillateOutOfPhase) ']'];
strStimulusColours = ['[' CheckEmptyNanSprintf(' %d', '(0.5 0 1)', '(dynamic or 0.5 0 1)', vfStimulusColours, vfStimulusColours) ']']';


% -- Produce description
   
strDescription = [strIndent sprintf('Oscillating plaid stimulus\n') ...
                  strIndent sprintf('Parameter list:\n')];
strIndent = [strIndent '   '];

strDescription = [strDescription ...
   strIndent sprintf('Frame duration: %s\n', strFrameDuration) ...
   strIndent sprintf('Stimulus duration: %s\n', strStimulusDuration) ...
   strIndent sprintf('Stimulus size: %s\n', strSizeDegrees) ...
   strIndent sprintf('Grating size: %s\n', strCyclesPerDegree) ...
   strIndent sprintf('Screen calibration: %s\n', strPixelsPerDegree) ...
   strIndent sprintf('Prime grating angle: %s\n', strAngle1Degrees) ...
   strIndent sprintf('Secondary grating angle: %s\n', strAngle2Degrees) ...
   strIndent sprintf('Grating bar width: %s\n', strBarWidthDegrees) ...
   strIndent sprintf('Oscillate out of phase: %s\n', strOscillateOutOfPhase) ...
   strIndent sprintf('Stimulus colours: %s\n', strStimulusColours)];

% --- END of DescribeOscillatingPlaidStimulus.m ---


function strString = CheckNanSprintf(strFormat, strEmptyFormat, oCheckNan, varargin)

if (~isnan(oCheckNan))
   strString = sprintf(strFormat, varargin{:});

elseif isempty(strEmptyFormat)
   strString = '(dynamic)';

else
   strString = sprintf(strEmptyFormat, varargin{:});
end


function strString = CheckEmptyNanSprintf(strFormat, strEmptyFormat, strNanFormat, oCheckEmptyNan, varargin)

if (isnan(oCheckEmptyNan))
   if (isempty(strNanFormat))
      strString = '(dynamic)';
   else
      strString = sprintf(strNanFormat, varargin{:});
   end
   
elseif (isempty(oCheckEmptyNan))
   if isempty(strEmptyFormat)
      strString = '(dynamic)';
   else
      strString = sprintf(strEmptyFormat, varargin{:});
   end
   
else
   strString = sprintf(strFormat, varargin{:});
end

% --- END of DescribeOscillatingPlaidStimulus.m ---
