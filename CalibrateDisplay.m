function [vfPixelsPerDegree, vfDegreesPerMetre, vfPixelsPerMetre, vfSizeDegrees] = ...
            CalibrateDisplay(vfDisplaySizeMetres, vnResolution, fDistanceFromEyeMetres)

% CalibrateDisplay - FUNCTION Calculate pixels per degree for a display
%
% Usage: [vfPixelsPerDegree, vfDegreesPerMetre, vfPixelsPerMetre, vfSizeDegrees] = ...
%           CalibrateDisplay(vfDisplaySizeMetres, vnResolution, fDistanceFromEyeMetres)
%
% 'vfDisplaySizeMetres' is the X and Y size of the display *screen*, in metres.
% 'vnResolution' is the X and Y resolution of the display.
% 'fDistanceFromEyeMetres' is just what's written on the can.
%
% 'vfPixelsPerDegree' is the X and Y calibration, in terms of monitor pixels per
% visual degree.  'vfDegreesPerMetre' is the average X and Y degrees per metre
% of display.

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 30th August, 2010

% -- Check arguments

if (nargin < 3)
   disp('*** CalibrateDisplay: Incorrect usage');
   help CalibrateDisplay;
   sca;
end


% -- Compute calibration

vfSizeDegrees = rad2deg(atan2((vfDisplaySizeMetres / 2), fDistanceFromEyeMetres) * 2);
vfDegreesPerMetre = vfSizeDegrees ./ vfDisplaySizeMetres;
vfPixelsPerDegree = vnResolution ./ vfSizeDegrees;
vfPixelsPerMetre = vnResolution ./ vfDisplaySizeMetres;

% --- END of CalibrateDisplay.m ---

function vfDeg = rad2deg(vfRad)

vfDeg = vfRad .* 180 / pi;