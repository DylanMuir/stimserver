function [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
            PresentOscillatingPlaidStimulus( hWindow, vtBlankTime, ...
                                             tFrameDuration, tStimulusDuration, ...
                                             vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, fAngle1Degrees, fAngle2Degrees, ...
                                             fBarWidthDegrees, vbOscillateOutOfPhase, vfStimulusColours)

% PresentOscillatingPlaidStimulus - FUNCTION Present an oscillating plaid
%
% Usage: [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
%             PresentOscillatingPlaidStimulus( hWindow, vtBlankTime, ...
%                                              tFrameDuration, tStimulusDuration, ...
%                                              vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, fAngle1Degrees, fAngle2Degrees, ...
%                                              < fBarWidthDegrees, vbOscillateOutOfPhase, vfStimulusColours>)
%
% 'vfSizeDegrees' is a vector [X Y] that defines the size of the entire
% stimulus, in degrees.  Leave as an empty vector to fill the presentation
% screen.

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 31st August, 2010 (from PresentOscillatingGratingStimulus.m)


% -- Persistent arguments

persistent POPS_sParams POPS_vnTexIDs;


% -- Defaults

DEF_vbOscillateOutOfPhase = [false false];
DEF_vfStimulusColours = [0.5 0 1];


if (~exist('vtBlankTime', 'var') || isempty(vtBlankTime))
   tStartTime = 0;
   vtBlankTime = 0;
   bPresentStimulus = false;
else
   % -- Blank screen
   tStartTime = Screen('Flip', hWindow);
   
   bPresentStimulus = true;
end

if (numel(vtBlankTime) > 1)
   tFlipTime = vtBlankTime(2) + vtBlankTime(1);
else
   tFlipTime = tStartTime + vtBlankTime(1);
end



% -- Check arguments

if (nargin < 9)
   SSlog('*** PresentOscillatingPlaidStimulus: Incorrect usage');
   help PresentOscillatingPlaidStimulus;
   sca;
   return;
end

if (~exist('fBarWidthDegrees', 'var'))
   fBarWidthDegrees = [];
end

if (~exist('vbOscillateOutOfPhase', 'var') || isempty(vbOscillateOutOfPhase))
   vbOscillateOutOfPhase = DEF_vbOscillateOutOfPhase;
   
elseif (numel(vbOscillateOutOfPhase) == 1)
   vbOscillateOutOfPhase = [vbOscillateOutOfPhase(1) vbOscillateOutOfPhase(1)];
end
   
if (~exist('vfStimulusColours', 'var') || isempty(vfStimulusColours))
   vfStimulusColours = DEF_vfStimulusColours;
end


% - Do we need to re-generate the stimulus?

if (isempty(POPS_sParams))
   bGenerateStim = true;

else
   % - Test each relevant parameter
   bGenerateStim = ~isequal(POPS_sParams.fCyclesPerDegree, fCyclesPerDegree);
   bGenerateStim = bGenerateStim | ~isequal(POPS_sParams.hWindow, hWindow);
   bGenerateStim = bGenerateStim | ~isequal(POPS_sParams.fPixelsPerDegree, fPixelsPerDegree);
   bGenerateStim = bGenerateStim | ~isequal(POPS_sParams.fBarWidthDegrees, fBarWidthDegrees);
   bGenerateStim = bGenerateStim | ~isequal(POPS_sParams.vfSizeDegrees, vfSizeDegrees);
   bGenerateStim = bGenerateStim | ~isequal(POPS_sParams.vbOscillateOutOfPhase, vbOscillateOutOfPhase);
   bGenerateStim = bGenerateStim | ~isequal(POPS_sParams.vfStimulusColours, vfStimulusColours);
end


% -- Generate stimulus as single pixels

if (bGenerateStim)
   SSlog('--- PresentOscillatingPlaidStimulus: Generating stimulus\n');
   
   % - Record parameters
   POPS_sParams = [];
   POPS_sParams.fCyclesPerDegree = fCyclesPerDegree;
   POPS_sParams.hWindow = hWindow;
   POPS_sParams.fPixelsPerDegree = fPixelsPerDegree;
   POPS_sParams.fBarWidthDegrees = fBarWidthDegrees;   
   POPS_sParams.vfSizeDegrees = vfSizeDegrees;
   POPS_sParams.vbOscillateOutOfPhase = vbOscillateOutOfPhase;
   POPS_sParams.vfStimulusColours = vfStimulusColours;
   
   if (isempty(vfSizeDegrees))
      [vnScreenSize(1), vnScreenSize(2)] = Screen('WindowSize', hWindow);
      
      vfSizeDegrees = sqrt(sum((vnScreenSize / fPixelsPerDegree).^2)) .* [1 1];
   end
   
   % - Generate stimulus 1 and add mask
   [mfGrating1f1 mfGrating1f2] = GenerateOscillatingGratingStimulus( vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, ...
                                                                     fBarWidthDegrees, vbOscillateOutOfPhase(1), vfStimulusColours);

   % - Mask grating 1
   mfGratingAlpha1f1 = mfGrating1f1;
   mfGratingAlpha1f1(mfGratingAlpha1f1 ~= vfStimulusColours(1)) = 1;
   mfGratingAlpha1f1(mfGratingAlpha1f1 == vfStimulusColours(1)) = 0;

   mfGratingAlpha1f2 = mfGrating1f2;
   mfGratingAlpha1f2(mfGratingAlpha1f2 ~= vfStimulusColours(1)) = 1;
   mfGratingAlpha1f2(mfGratingAlpha1f2 == vfStimulusColours(1)) = 0;

   mfGrating1f1(:, :, 2) = mfGratingAlpha1f1;
   mfGrating1f2(:, :, 2) = mfGratingAlpha1f2;

   % - Generate stimulus 2 and add mask
   [mfGrating2f1 mfGrating2f2] = GenerateOscillatingGratingStimulus( vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, ...
                                                                     fBarWidthDegrees, vbOscillateOutOfPhase(2), vfStimulusColours);

   % - Mask grating 2
   mfGratingAlpha2f1 = mfGrating2f1;
   mfGratingAlpha2f1(mfGratingAlpha2f1 ~= vfStimulusColours(1)) = 1;
   mfGratingAlpha2f1(mfGratingAlpha2f1 == vfStimulusColours(1)) = 0;

   mfGratingAlpha2f2 = mfGrating2f2;
   mfGratingAlpha2f2(mfGratingAlpha2f2 ~= vfStimulusColours(1)) = 1;
   mfGratingAlpha2f2(mfGratingAlpha2f2 == vfStimulusColours(1)) = 0;

   mfGrating2f1(:, :, 2) = mfGratingAlpha2f1;
   mfGrating2f2(:, :, 2) = mfGratingAlpha2f2;

   % - Convert to screen colour indices
   nBlack = BlackIndex(hWindow);
   nWhite = WhiteIndex(hWindow);
   nGrey = round((nWhite + nBlack)/2);

   mfGrating1f1(mfGrating1f1 == 0) = nBlack;
   mfGrating1f1(mfGrating1f1 == 0.5) = nGrey;
   mfGrating1f1(mfGrating1f1 == 1) = nWhite;
   POPS_sParams.mfGrating1f1 = mfGrating1f1;
   
   mfGrating1f2(mfGrating1f2 == 0) = nBlack;
   mfGrating1f2(mfGrating1f2 == 0.5) = nGrey;
   mfGrating1f2(mfGrating1f2 == 1) = nWhite;
   POPS_sParams.mfGrating1f2 = mfGrating1f2;

   mfGrating2f1(mfGrating2f1 == 0) = nBlack;
   mfGrating2f1(mfGrating2f1 == 0.5) = nGrey;
   mfGrating2f1(mfGrating2f1 == 1) = nWhite;
   POPS_sParams.mfGrating2f1 = mfGrating2f1;

   mfGrating2f2(mfGrating2f2 == 0) = nBlack;
   mfGrating2f2(mfGrating2f2 == 0.5) = nGrey;
   mfGrating2f2(mfGrating2f2 == 1) = nWhite;
   POPS_sParams.mfGrating2f2 = mfGrating2f2;
   
   % - CLear texture cache
   POPS_vnTexIDs = [];
end


% - Create textures, if necessary
if (isempty(POPS_vnTexIDs))
   bGenerateTexture = true;
else
   % - Test to see if our texture exists
   vnTextures = Screen('Windows');
   
   if (~all(ismember(POPS_vnTexIDs, vnTextures)))
      bGenerateTexture = true;
   else
      bGenerateTexture = false;
   end
end

% - Generate a texture, if required
if (bGenerateTexture)
   % - Generate the texture
   POPS_vnTexIDs(1) = Screen('MakeTexture', hWindow, POPS_sParams.mfGrating1f1);
   POPS_vnTexIDs(2) = Screen('MakeTexture', hWindow, POPS_sParams.mfGrating1f2);
   POPS_vnTexIDs(3) = Screen('MakeTexture', hWindow, POPS_sParams.mfGrating2f1);
   POPS_vnTexIDs(4) = Screen('MakeTexture', hWindow, POPS_sParams.mfGrating2f2);
end



% -- Try to pre-load textures

Screen('PreloadTextures', hWindow, POPS_vnTexIDs);


% -- Present stimulus

if (~bPresentStimulus)
   tLastPresentation = [];
   bBlewFrameRate = [];
   bBlewBlankTime = [];
   return;
end   

bFirst = true;
nShowFrame = 1;
bBlewFrameRate = false;
tCurrTime = -inf;
tEndTime = inf;

while (tFlipTime < tEndTime)
   % - Set blending of screen to handle alpha-masked stimuli
   Screen('BlendFunction', hWindow, GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
   
   % - Draw texture and flip window
   Screen('DrawTexture', hWindow, POPS_vnTexIDs(nShowFrame), [], [], fAngle1Degrees);
   Screen('DrawTexture', hWindow, POPS_vnTexIDs(nShowFrame+2), [], [], fAngle2Degrees);
   [tCurrTime, nul, nul, Missed] = FrameMarkerFlip(hWindow, bFirst, tFlipTime);

   % - Did we miss any deadlines?
   if (bFirst)
      bBlewBlankTime = Missed > 0;
      tEndTime = tCurrTime + tStimulusDuration;
   else
      bBlewFrameRate = bBlewFrameRate | (Missed > 0);
   end

   bFirst = false;

   if (nShowFrame == 1)
      nShowFrame = 2;
   else
      nShowFrame = 1;
   end
   
   % - Compute next flip time
   tFlipTime = tCurrTime + tFrameDuration;
end

% - Display a warning
if (bBlewBlankTime || bBlewFrameRate)
   SSlog('--- PresentOscillatingPlaidStimulus: The desired frame rate or blanking period was not met.\n');
end

% - Return the last stimulation time
tLastPresentation = Screen('Flip', hWindow, tEndTime);

% --- END of PresentOscillatingGratingStimulus.m ---

