function [strDescription] = DescribeSinePlaidStimulus(sStimulus, strIndent)

% SinePlaidStimulus - STIMULUS A sine-wave summed plaid composed of two gratings, that can each be dynamically shifted and rotated
%
% This stimulus is a linear summation of two black and white sine-wave gratings,
% presented at some orientation.  The stimulus can be shifted during
% presentation, as well as rotated during presentation.
% 
% Presentation parameters:
%
%     tFrameDuration - The desired duration of a single frame
%     tStimulusDuration - The duration of the entire stimulus
%     nNumRepeats - The number of times to repeat the stimulus presentation
%        (there for convenience)
%
% Stimulus parameters:
%
%     vfSizeDegrees - A two-element vector [fXSize fYSize], where 'fXSize' and
%        'fYSize' define the size of the stimulus presentation region in degrees.
%        If an empty matrix is provided, the the stimulus will fill the
%        presentation screen.
%     vfCyclesPerDegree - The spatial frequency of each grating, in cycles per
%        degree.
%     fPixelsPerDegree - The calibration of the presentation screen, in pixels
%        per degree.
%     vfAngleDegrees - The orientation of each grating in degrees, where 0 degrees
%        is a vertical grating.
%     vfShiftCyclesPerSec - The distance to shift each grating during the stimulus
%        presentation, in grating cycles per second.
%     vfRotateCyclesPerSec - The amount to rotate each grating during the stimulus
%        presentation, in rotational cycles per second.
%     vfContrast - A number between 0 and 1, where 1 is 100% contrast and 0 is 0%
%        contrast.  By default, each grating is presented at 50% contrast.
                                           

% DescribeSinePlaidStimulus - FUNCTION Describe the parameters for a
% sine-wave plaid stimulus
%
% Usage: [strDescription] = DescribeSinePlaidStimulus(sStimulus <, strIndent>)
%
% Usually called automatically by StimulusServerDescribeStimuli

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 10th Janurary, 2012

% -- Check arguments

if (nargin == 0)
   SSlog('*** DescribeSinePlaidStimulus: Incorrect usage\n');
   help DescribeSinePlaidStimulus;
   return;
end

if (~exist('strIndent', 'var') || isempty(strIndent))
   strIndent = '';
end

% -- Convert parameters to parameter strings

% - Extract parameters from stimulus
[tFrameDuration, tStimulusDuration, nNumRepeats] = DealParamsOrNaN(sStimulus.cPresentationParameters{:});
[vfSizeDegrees, vfCyclesPerDegree, fPixelsPerDegree, ...
   vfAngleDegrees, vfShiftCyclesPerSec, vfRotateCyclesPerSec, vfContrast] = ...
      DealParamsOrNaN(sStimulus.cStimulusArguments{:});

if (isscalar(vfCyclesPerDegree))
   vfCyclesPerDegree = [vfCyclesPerDegree vfCyclesPerDegree];
end

if (isscalar(vfAngleDegrees))
   vfAngleDegrees = [vfAngleDegrees vfAngleDegrees];
end

if (isscalar(vfShiftCyclesPerSec))
   vfShiftCyclesPerSec = [vfShiftCyclesPerSec vfShiftCyclesPerSec];
end

if (isscalar(vfRotateCyclesPerSec))
   vfRotateCyclesPerSec = [vfRotateCyclesPerSec vfRotateCyclesPerSec];
end
   
if (isscalar(vfContrast))
   vfContrast = [vfContrast vfContrast];
end

strFrameDuration = CheckNanSprintf('%.2f ms', [], tFrameDuration, tFrameDuration*1e3);
strStimulusDuration = CheckNanSprintf('%.2f s', [], tStimulusDuration, tStimulusDuration);
strNumRepeats = CheckNanSprintf('%d', [], nNumRepeats, nNumRepeats);
strSizeDegrees = CheckEmptyNanSprintf('[%.2f x %.2f] deg', '(full screen)', [], vfSizeDegrees, vfSizeDegrees);
strCyclesPerDegree = CheckNanSprintf('[%.2f, %.2f] cpd; [%.2f, %.2f] dpc', [], vfCyclesPerDegree, vfCyclesPerDegree, 1./vfCyclesPerDegree);
strPixelsPerDegree = CheckNanSprintf('%.2f ppd', [], fPixelsPerDegree, fPixelsPerDegree);
strAngleDegrees = CheckNanSprintf('[%.2f, %.2f] deg', [], vfAngleDegrees, vfAngleDegrees);
strShiftCyclesPerSec = CheckEmptyNanSprintf('[%.2f, %.2f] Hz', '(stationary)', '(dynamic or stationary)', vfShiftCyclesPerSec, vfShiftCyclesPerSec);
strRotateCyclesPerSec = CheckEmptyNanSprintf('[%.2f, %.2f] Hz', '(stationary)', '(dynamic or stationary)', vfRotateCyclesPerSec, vfRotateCyclesPerSec);
strContrast = CheckNanSprintf('[%.2f, %.2f] %%', '(50%%)', vfContrast, vfContrast*100);


% -- Produce description
   
strDescription = [strIndent sprintf('Sinusoidal plaid stimulus\n') ...
                  strIndent sprintf('Parameter list:\n')];
strIndent = [strIndent '   '];

strDescription = [strDescription ...
   strIndent sprintf('Frame duration: %s\n', strFrameDuration) ...
   strIndent sprintf('Stimulus duration: %s\n', strStimulusDuration) ...
   strIndent sprintf('Number of repeats: %s\n', strNumRepeats) ...
   strIndent sprintf('Stimulus size: %s\n', strSizeDegrees) ...
   strIndent sprintf('Grating size: %s\n', strCyclesPerDegree) ...
   strIndent sprintf('Screen calibration: %s\n', strPixelsPerDegree) ...
   strIndent sprintf('Grating base orientations: %s\n', strAngleDegrees) ...
   strIndent sprintf('Grating drift rates: %s\n', strShiftCyclesPerSec) ...
   strIndent sprintf('Grating rotation rates: %s\n', strRotateCyclesPerSec) ...
   strIndent sprintf('Grating contrasts: %s\n', strContrast)];

% --- END of DescribeSinePlaidStimulus.m ---

function strString = CheckNanSprintf(strFormat, strEmptyFormat, oCheckNan, varargin)

if (~isnan(oCheckNan))
   strString = sprintf(strFormat, varargin{:});

elseif isempty(strEmptyFormat)
   strString = '(dynamic)';

else
   strString = sprintf(strEmptyFormat, varargin{:});
end

function strString = CheckEmptyNanSprintf(strFormat, strEmptyFormat, strNanFormat, oCheckEmptyNan, varargin)

if (isnan(oCheckEmptyNan))
   if (isempty(strNanFormat))
      strString = '(dynamic)';
   else
      strString = sprintf(strNanFormat, varargin{:});
   end
   
elseif (isempty(oCheckEmptyNan))
   if isempty(strEmptyFormat)
      strString = '(dynamic)';
   else
      strString = sprintf(strEmptyFormat, varargin{:});
   end
   
else
   strString = sprintf(strFormat, varargin{:});
end

% --- END of DescribeSinePlaidStimulus.m ---
