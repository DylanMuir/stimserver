# README #

This is the source repository for StimServer, a Matlab toolbox for generation and presentation of visual stimuli, based on [Psychtoolbox](http://psychtoolbox.org). See the [StimServer wiki](https://bitbucket.org/DylanMuir/stimserver/wiki) for more information.

### How do I get set up? ###

* Check out the repository to your local machine, using your git client of choice
* Add the base path of the repository to your Matlab path
* StimServer requires [Psychtoolbox](http://psychtoolbox.org) for display of visual stimuli. Install and configure [Psychtoolbox](http://psychtoolbox.org) for your machine

### How to contribute ###

* Firstly, thanks for your willingness to help! Documentation improvements, especially on the [StimServer wiki](https://bitbucket.org/DylanMuir/stimserver/wiki), are especially useful. Bug fixes and feature contributions are an amazing contribution.
* If you would like to contribute a patch or bug fix, write to Dylan Muir (<dylan.muir@unibas.ch>) to request write access to the repository.
* Create a feature branch to encapsulate your code changes.
* After completing, documenting and testing your code, create a pull request for your branch to be integrated into the master branch.
* You may also of course fork the repository, work on your fork (under a feature branch, of course), and then create a pull request as above.

### Who do I talk to? ###

* In general the toolbox is supplied "as-is", with only moderate support
* Please use the [Issues tracker](https://bitbucket.org/DylanMuir/stimserver/issues) to report bugs and feature suggestions
* For questions or feedback, contact Dylan Muir (<dylan.muir@unibas.ch>)