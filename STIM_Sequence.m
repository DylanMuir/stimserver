function [sStimulus] = STIM_Sequence(varargin)

% STIM_Sequence - Stimulus object
%
% Usage: [sStimulus] = STIM_Sequence(vnOrdering, sSequence)

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 24th November, 2010

%- Deal out parameters from argument list
[vnOrdering, sSequence] = DealParamsOrNaN(varargin{:});

% - Group parameters
cPresentationParams = {vnOrdering(:)};
cStimParams = {sSequence};

% - Trim off NANs
cPresentationParams = cPresentationParams(~cellfun(@isnan_empty, cPresentationParams));

% - Build stimulus structure                 
sStimulus = CreateStimulusStructure(@PresentStimulusSequence, ...
               cPresentationParams, cStimParams, @calc_duration, ...
               []);


function vbIsNAN = isnan_empty(voThing)

vbIsEmpty = isempty(voThing);

vbIsNAN = false(size(vbIsEmpty));
vbIsNAN(~vbIsEmpty) = isnan(voThing(~vbIsEmpty));

function tStimDuration = calc_duration(s, b)

vsSequence = s.cStimulusArguments{1};
nNumStimuli = numel(s.cStimulusArguments{1});
vtStimDurations = zeros(1, nNumStimuli);
for (nSeqID = 1:nNumStimuli) %#ok<FORPF>
   vtStimDurations(nSeqID) = vsSequence(nSeqID).fhDurationFunction(vsSequence(nSeqID), b);
end

tStimDuration = sum(vtStimDurations(s.cPresentationParameters{1}));

% --- END of STIM_Sequence.m ---
