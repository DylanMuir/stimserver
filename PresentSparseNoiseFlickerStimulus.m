function [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
            PresentSparseNoiseFlickerStimulus(hWindow, vtBlankTime, ...
                                       tFlickerFrameDuration, tPixelDuration, nNumRepeats, ...
                                       vfUnitSizeDegrees, vnNumUnits, fPixelsPerDegree, ...
                                       vnSeed, nNumPixelPresentations)

% PresentSparseNoiseFlickerStimulus - FUNCTION Present a sparse noise stimulus with flickering pixels
%
% Usage: [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
%             PresentSparseNoiseFlickerStimulus(hWindow, vtBlankTime, ...
%                                        tFlickerFrameDuration, tPixelDuration, nNumRepeats, ...
%                                        vfUnitSizeDegrees, vnNumUnits, fPixelsPerDegree, ...
%                                        < vnSeed, nNumPixelPresentations>)
%
% 'vfUnitSizeDegrees' is a vector [X Y] defining the size of a single stimulus
% unit in degrees.  'vnNumUnits' is a vector [X Y] defining the number of
% stimulus units in the stimulus. 'fPixelsPerDegree' is the display calibration
% number.  'hWindow' is a handle to an open window.  'tBlankTime' is the desired
% time to show a blank screen from the start of the function to the onset of the
% stimulus, in seconds. 'tFlickerFrameDuration' is the desired frame duration of
% the pixel flickers, in seconds.  'nNumRepeats' is the desired number of times
% to display the entire stimulus (with no intervening blank). 'vnSeed' are the
% seeds used to generate the random sparse noise stimulus.  If a vector of seeds
% are provided, a longer sequence built up of the individual seeded sequences
% will be generated. 'nNumPixelPresentations', if provided, can be used to
% present a non-maximal noise sequence. 

% -- Persistent arguments

persistent PSNFS_sParams PSNFS_tfSparseNoise PSNFS_mnTexIDs;


if (~exist('vtBlankTime', 'var') || isempty(vtBlankTime))
   tStartTime = 0;
   vtBlankTime = 0;
   bPresentStimulus = false;
else
   % -- Blank screen
   tStartTime = Screen('Flip', hWindow);
   
   bPresentStimulus = true;
end

if (numel(vtBlankTime) > 1)
   tFlipTime = vtBlankTime(2) + vtBlankTime(1);
else
   tFlipTime = tStartTime + vtBlankTime(1);
end



% -- Check arguments

if (~exist('vnSeed', 'var'))
   vnSeed = [];
end

if (~exist('nNumPixelPresentations', 'var'))
   nNumPixelPresentations = [];
end

% - Get black / white colours
nBlack = BlackIndex(hWindow);
nWhite = WhiteIndex(hWindow);
nGrey = round((nWhite + nBlack)/2);


% - Do we need to re-generate the stimulus?

if (isempty(PSNFS_sParams))
   bGenerateStim = true;

else
   % - Test each relevant parameter
   bGenerateStim = ~isequal(PSNFS_sParams.vnSeed, vnSeed);
   bGenerateStim = bGenerateStim | ~isequal(PSNFS_sParams.hWindow, hWindow);
   bGenerateStim = bGenerateStim | ~isequal(PSNFS_sParams.vnNumUnits, vnNumUnits);
   bGenerateStim = bGenerateStim | ~isequal(PSNFS_sParams.nNumPixelPresentations, nNumPixelPresentations);
end


% -- Generate stimulus as single pixels

if (bGenerateStim)
   disp('--- PresentSparseNoiseFlickerStimulus: Generating stimulus');
   
   % - Record parameters
   PSNFS_sParams = [];
   PSNFS_sParams.vnSeed = vnSeed;
   PSNFS_sParams.hWindow = hWindow;
   PSNFS_sParams.vnNumUnits = vnNumUnits;
   PSNFS_sParams.nNumPixelPresentations = nNumPixelPresentations;
   
   % - Generate stimulus
   PSNFS_tfSparseNoise = GenerateSparseNoiseStimulus([1 1], vnNumUnits, vnSeed, nNumPixelPresentations, [nGrey nWhite]);
   
   % - Pre-calculate params
   PSNFS_sParams.nSequenceLength = size(PSNFS_tfSparseNoise, 3);
   PSNFS_sParams.vnScreenRect = [0 0 0 0];
   [PSNFS_sParams.vnScreenRect(3), PSNFS_sParams.vnScreenRect(4)] = Screen('WindowSize', hWindow);
   PSNFS_sParams.vnDestRec = CenterRect([0 0 round(vfUnitSizeDegrees .* vnNumUnits * fPixelsPerDegree)], PSNFS_sParams.vnScreenRect);
   
   % - Re-generate textures
   PSNFS_mnTexIDs = [];
end


% - Create textures, if necessary
if (isempty(PSNFS_mnTexIDs))
   bGenerateTexture = true;
   
else
   % - Test to see if our texture exists
   vnTextures = Screen('Windows');
   
   if (~all(ismember(PSNFS_mnTexIDs(:), vnTextures)))
      bGenerateTexture = true;
   else
      bGenerateTexture = false;
   end
end

% - Generate a texture, if required
if (bGenerateTexture)   
   % - Check that we can actually use a shader
   AssertGLSL;
   
   % - Replicate the pixel frames
   tfSparseNoise = repmat(PSNFS_tfSparseNoise, [1 1 1 2]);
   
   % - Change white to black on intervening frames
   tfSparseNoise(tfSparseNoise(:, :, :, 2) == nWhite) = nBlack;
   
   vnFrames = size(PSNFS_tfSparseNoise);
   tfSparseNoise = reshape(tfSparseNoise, [vnFrames(1:2) vnFrames(3)*2 1]);
   
   % - Generate the textures
   PSNFS_mnTexIDs = GenerateTextures(permute(tfSparseNoise, [2 1 3 4]), hWindow);

   % - Reshape to give alternate colours in the second dimension
   PSNFS_mnTexIDs = reshape(PSNFS_mnTexIDs, [], 2);
end



% -- Try to pre-load textures

Screen('PreloadTextures', hWindow, PSNFS_mnTexIDs(:));


% -- Present stimulus

if (~bPresentStimulus)
    tLastPresentation = [];
    bBlewFrameRate = [];
    bBlewBlankTime = [];
    return;
end

bFirst = true;
bBlewFrameRate = false;
bBlewBlankTime = false;
tEndTime = tFlipTime + nNumRepeats * PSNFS_sParams.nSequenceLength * tPixelDuration;
tCurrTime = tFlipTime;
nPixelFrame = 1;
nNumColours = 2;

for (nRepeat = 1:nNumRepeats)
   % - Determine start time for this repeat
   tStartTime = tFlipTime;
   
   for (nPixel = 1:PSNFS_sParams.nSequenceLength)
      % - Determine nominal end time for this pixel
      tPixelEndTime = tStartTime + nPixel * tPixelDuration;
      
      while (tCurrTime <= tPixelEndTime)
         % - Draw texture and flip window
         Screen('DrawTexture', hWindow, PSNFS_mnTexIDs(nPixel, nPixelFrame), [], PSNFS_sParams.vnDestRec, [], 0);
         [tCurrTime, nul, nul, Missed] = FrameMarkerFlip(hWindow, bFirst, tFlipTime);
         
         % - Did we miss any deadlines?
         if (bFirst)
            bBlewBlankTime = Missed > 0;
         else
            bBlewFrameRate = bBlewFrameRate | (Missed > 0);
         end
         
         bFirst = false;
         
         % - Move to next pixel frame
         nPixelFrame = mod(nPixelFrame, nNumColours)+1;
         
         % - Compute next flip time
         tFlipTime = tFlipTime + tFlickerFrameDuration;
         if (tFlipTime > tPixelEndTime)
            tFlipTime = tPixelEndTime;
         end
         
         % - Have we blown our stimulation slot?
         if (tFlipTime > tEndTime)
            break;
         end
      end

      % - Have we blown our stimulation slot?
      if (tFlipTime > tEndTime)
         break;
      end
   end
   
   % - Have we blown our stimulation slot?
   if (tFlipTime > tEndTime)
      break;
   end
end

% - Display a warning if necessary
if (bBlewBlankTime || bBlewFrameRate)
   disp('--- PresentSparseNoiseFlickerStimulus: The desired frame rate or blanking period was not met.');
end

% - Return the last stimulation time
tLastPresentation = Screen('Flip', hWindow, tEndTime);

% --- END of PresentSparseNoiseFlickerStimulus.m ---

