function [sStimulus] = STIM_Texture(varargin)

% STIM_Texture - Stimulus object
%
% Usage: [sStimulus] = STIM_Texture( tFrameDuration, tStimulusDuration, nNumRepeats, ...
%                                          vfSizeDegrees, fGridsPerDegree, fPixelsPerDegree, ...
%                                          fDirectionDegrees, fOrientationDegrees, ...
%                                          fShiftGridsPerSecond, fJitter, ...
%                                          fWidthDegrees, fLengthDegrees, bGabor, ...
%                                          fBandwidth, fPhaseoffset, nGridRepetition, ...
%                                          fContrast, bAdapt, tAdaptationDuration, ...
%                                          fAdaptGridsPerSec, fAdaptContrast, nSeed, ...
%                                          vfGreyLevels)
% 
% Author: Andreas Keller <andi@ini.phys.ethz.ch>
% 
% Created: 18th August, 2011

%- Deal out parameters from argument list
[tFrameDuration, tStimulusDuration, nNumRepeats, ...
                        vfSizeDegrees, fGridsPerDegree, fPixelsPerDegree, ...
                        fDirectionDegrees, fOrientationDegrees, ...
                        fShiftGridsPerSecond, fJitter, ...
                        fWidthDegrees, fLengthDegrees, bGabor, ...
                        fBandwidth, fPhaseoffset, nGridRepetition, ...
                        fContrast, bAdapt, tAdaptationDuration, ...
                        fAdaptGridsPerSec, fAdaptContrast, nSeed, ...
                        vfGreyLevels] = DealParamsOrNaN(varargin{:});

% - Group parameters
cPresentationParams = {tFrameDuration, tStimulusDuration, nNumRepeats};
cStimParams = {vfSizeDegrees, fGridsPerDegree, fPixelsPerDegree, ...
                        fDirectionDegrees, fOrientationDegrees, ...
                        fShiftGridsPerSecond, fJitter, ...
                        fWidthDegrees, fLengthDegrees, bGabor, ...
                        fBandwidth, fPhaseoffset, nGridRepetition, ...
                        fContrast, bAdapt, tAdaptationDuration, ...
                        fAdaptGridsPerSec, fAdaptContrast, nSeed, ...
                        vfGreyLevels};

% - Trim off NANs
cPresentationParams = cPresentationParams(~cellfun(@isnan_empty, cPresentationParams));
cStimParams = cStimParams(~cellfun(@isnan_empty, cStimParams));

% - Construct "get duration" function
if bAdapt == true %necessary because bAdapt can be Nan
    fhDurationFunction = @(s, b)(s.cPresentationParameters{2}+s.cStimulusArguments{16}+b);
else
    fhDurationFunction = @(s, b)(s.cPresentationParameters{2}+b);
end


% - Build stimulus structure                 
sStimulus = CreateStimulusStructure(@PresentTextureStimulus, ...
               cPresentationParams, cStimParams, fhDurationFunction, ...
               @DescribeTextureStimulus);

            
function vbIsNAN = isnan_empty(voThing)

vbIsEmpty = isempty(voThing);

vbIsNAN = false(size(vbIsEmpty));
vbIsNAN(~vbIsEmpty) = isnan(voThing(~vbIsEmpty));



% --- END of STIM_Texture.m ---
