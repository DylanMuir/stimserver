function [strDescription] = DescribeMaskedMovieStimulus(sStimulus, strIndent)

% DescribeMaskedMovieStimulus - STIMULUS A movie, with an optional mask
%
% Stimulus parameters:
%
% tFrameDuration - The desired duration of each movie frame, in seconds.
%
% nNumRepeats - The number of times the full stimulus should be displayed.
%
% tStimulusDuration - The desired display time of the stimulus, in seconds.  This can be
%     shorter than the length of the movie file itself.  In this case the
%     stimulus will be trimmed to the value for 'tStimulusDuration'.  This value
%     will be multiplied by 'nNumRepeats' to obtain the full display time of the
%     stimulus.
%
% bDrawMask - A boolean flag indicating whether the movie should be covered with
%     a mask (if 'true') or not ('false'; the default).
% 
% bInvertMask - If 'false', the mask will be a grey screen with the movie
%     displayed within a small circular region.  If 'true', the movie will be
%     displayed full screen, with a grey circle placed over the masked region.
%
% fMaskDiameterDeg - The diameter of the circular mask, in degrees.
%
% vfMaskPosPix - The location of the mask, in pixels offset from the center of
%     the display window.  This argument is a two-element vector ['nXOffset'
%     'nYOffset'].  Positive offsets are to the right and down.
%
% vfMovieSizeDeg - The desired presentation size of the movie, in degrees.  If
%     not specified (an empty matrix '[]'), then the size of the movie will be
%     estimated by the pixel dimensions of the movie.
%
% vfPixelsPerDegree - The spatial calibration of the display window, in pixels
%     per degree.  This argument is a two-element vector ['fXPixelsPerDegree'
%     'fYPixelsPerDegree'].
%
% strMovieName - The full filename of the movie to be presented.
%
% nFitToScreenMode - A scalar integer from {'0', '1', '2'}, specifying how the
%     movie should be fit to the display window.  If '0' is specified, the movie
%     will be displayed with the size specified by 'vfMovieSizeDeg'.  If '1' is
%     specified (the default), then the movie will be fit to the display window
%     without clipping.  If '1' is specified, the movie will be scaled to fill
%     the entire display window, clipping the movie if necessary.
%
% bCenterMovieOnMask - A boolean flag specifying whether the movie should be
%     shifted to match the location of the mask ('true') or centered on the
%     display window ('false'; default).

% DescribeMaskedMovieStimulus - FUNCTION Describe the parameters for an oscillating plaid stimulus
%
% Usage: [strDescription] = DescribeMaskedMovieStimulus(sStimulus <, strIndent>)
%
% Usually called automatically by StimulusServerDescribeStimuli

% Author: Dylan Muir and Patricia Molina-Luna
% Created: 26th October, 2010

% -- Check arguments

if (nargin == 0)
   SSlog('*** DescribeMaskedMovieStimulus: Incorrect usage\n');
   help DescribeMaskedMovieStimulus;
   return;
end

if (~exist('strIndent', 'var') || isempty(strIndent))
   strIndent = '';
end


% -- Convert parameters to parameter strings

% - Extract parameters from stimulus
[tFrameDuration, nNumRepeats, tStimulusDuration, bDrawMask, bInvertMask, fMaskDiameterDeg, vfMaskPosPix, ...
   vfMovieSizeDeg, vfPixelsPerDegree] = DealParamsOrNaN(sStimulus.cPresentationParameters{:});
[strMovieName] = ...
   DealParamsOrNaN(sStimulus.cStimulusArguments{:});

strFrameDuration = CheckNanSprintf('%.2f ms', [], tFrameDuration, tFrameDuration*1e3);
strNumRepeats = CheckEmptyNanSprintf('%d', '1', [], nNumRepeats, nNumRepeats);
strStimulusDuration = CheckNanSprintf('%.2f sec', [], tStimulusDuration, tStimulusDuration);
strDrawMask = CheckEmptyNanSprintf('%d', '(no mask)', [], bDrawMask, bDrawMask);
strInvertMask = CheckEmptyNanSprintf('%d', '(reveal centre)', [], bInvertMask, bInvertMask);
fMaskDiameterDeg = CheckNanSprintf('%.2f deg', [], fMaskDiameterDeg, fMaskDiameterDeg);
strMaskPosPix = CheckNanSprintf('[%d, %d] pix', [], vfMaskPosPix, vfMaskPosPix);
strMovieSizeDeg = CheckEmptyNanSprintf('[%d x %d] deg', '(fit screen)', [], vfMovieSizeDeg, vfMovieSizeDeg);
strPixelsPerDegree = CheckNanSprintf('%.2f ppd', [], vfPixelsPerDegree, vfPixelsPerDegree);
strstrMovieName = CheckNanSprintf('[%s]', [], strMovieName, strMovieName);



% -- Produce description
   
strDescription = [strIndent sprintf('Masked movie stimulus\n') ...
                  strIndent sprintf('Parameter list:\n')];
strIndent = [strIndent '   '];

strDescription = [strDescription ...
   strIndent sprintf('Frame duration: %s\n', strFrameDuration) ...
   strIndent sprintf('Number of repeats: %s\n', strNumRepeats) ...
   strIndent sprintf('Stimulus duration: %s\n', strStimulusDuration) ...
   strIndent sprintf('Mask stimulus: %s\n', strDrawMask) ...
   strIndent sprintf('Inverted mask: %s\n', strInvertMask) ...
   strIndent sprintf('Mask diameter: %s\n', fMaskDiameterDeg) ...
   strIndent sprintf('Mask centre position: %s\n', strMaskPosPix) ...
   strIndent sprintf('Movie size: %s\n', strMovieSizeDeg) ...
   strIndent sprintf('Pixels per degree: %s\n', strPixelsPerDegree) ...
   strIndent sprintf('Movie filename: %s\n', strstrMovieName)];

% --- END of DescribeMaskedMovieStimulus.m ---


function strString = CheckNanSprintf(strFormat, strEmptyFormat, oCheckNan, varargin)

if (~isnan(oCheckNan))
   strString = sprintf(strFormat, varargin{:});

elseif isempty(strEmptyFormat)
   strString = '(dynamic)';

else
   strString = sprintf(strEmptyFormat, varargin{:});
end


function strString = CheckEmptyNanSprintf(strFormat, strEmptyFormat, strNanFormat, oCheckEmptyNan, varargin)

if (isnan(oCheckEmptyNan))
   if (isempty(strNanFormat))
      strString = '(dynamic)';
   else
      strString = sprintf(strNanFormat, varargin{:});
   end
   
elseif (isempty(oCheckEmptyNan))
   if isempty(strEmptyFormat)
      strString = '(dynamic)';
   else
      strString = sprintf(strEmptyFormat, varargin{:});
   end
   
else
   strString = sprintf(strFormat, varargin{:});
end

% --- END of DescribeMaskedMovieStimulus.m ---
