function [vsStimuli] = GenerateSparseGratingStimuli(tFrameDuration, tPixelDuration, ...
                                                    vnNumUnits, ...
                                                    fUnitWidthDegrees, fUnitOverlapProportion, ...
                                                    fCyclesPerDegree, fPixelsPerDegree, fBarWidthDegrees, ...
                                                    fBaseAngle, fShiftCyclesPerSec, fRotateCyclesPerSec, ...
                                                    vfShiftCenterDeg)

% GenerateSparseGratingStimuli - FUNCTION Generate a set of masked grating stimuli to use as a sparse noise stimulus
%
% Usage: [vsStimuli] = GenerateSparseGratingStimuli(tFrameDuration, tPixelDuration, ...
%                                                   vnNumUnits, ...
%                                                   fUnitWidthDegrees, fUnitOverlapProportion, ...
%                                                   fCyclesPerDegree, fPixelsPerDegree, fBarWidthDegrees, ...
%                                                   fBaseAngle, fShiftCyclesPerSec, fRotateCyclesPerSec, ...
%                                                   vfShiftCenterDeg)
%
% See STIM_SparseGrating for information about the arguments.

% -- Check arguments

if (nargin < 11)
   disp('*** GenerateSparseGratingStimuli: Incorrect usage');
   help GenerateSparseGratingStimuli;
   return;
end

if (~exist('vfShiftCenterDeg', 'var') || isempty(vfShiftCenterDeg))
   vfShiftCenterDeg = [0 0];
end


% -- Generate a set of stimuli

% - Generate stimulus locations
vnLoc = 1:prod(vnNumUnits);
[vnY, vnX, vnT] = ind2sub(vnNumUnits([2 1]), vnLoc); %#ok<NASGU>

% - Compute pixel centre offsets
vfStimSizeDeg = fUnitWidthDegrees .* vnNumUnits * (1-fUnitOverlapProportion);
vfXCentres = linspace(-vfStimSizeDeg(1)/2, vfStimSizeDeg(1)/2, vnNumUnits(1) + 1) + (fUnitWidthDegrees * (1-fUnitOverlapProportion))/2;
vfXCentres = vfXCentres(1:end-1) + vfShiftCenterDeg(1);
vfYCentres = linspace(-vfStimSizeDeg(2)/2, vfStimSizeDeg(2)/2, vnNumUnits(2) + 1) + (fUnitWidthDegrees * (1-fUnitOverlapProportion))/2;
vfYCentres = vfYCentres(1:end-1) + vfShiftCenterDeg(2);

% - Generate grating sequence
for (nPixel = prod(vnNumUnits):-1:1)
   vsStimuli(nPixel) = STIM_SquareGrating(tFrameDuration, tPixelDuration, 1, [], ...
      fCyclesPerDegree, fPixelsPerDegree, fBarWidthDegrees, ...
      fBaseAngle, fShiftCyclesPerSec, fRotateCyclesPerSec, ...
      true, false, fUnitWidthDegrees, [vfXCentres(vnX(nPixel)) vfYCentres(vnY(nPixel))] .* fPixelsPerDegree, ...
      [], [], [], [], [], ...
      [vfXCentres(vnX(nPixel)) vfYCentres(vnY(nPixel))] .* fPixelsPerDegree); %#ok<SNASGU,SAGROW,AGROW>
end


% --- END of GenerateSparseGratingStimuli.m ---
