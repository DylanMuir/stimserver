classdef SSonCleanup < handle
%SSonCleanup - Specify cleanup work to be done on function completion.
%   C = SSonCleanup(S), when called in function F, specifies any cleanup tasks
%   that need to be performed when F completes.  S is a handle to a function
%   that performs necessary cleanup work when F exits (e.g., closing files that
%   have been opened by F).  S will be called whether F exits normally or
%   because of an error.
%
%   SSonCleanup is a MATLAB class and C = onCleanup(S) constructs an instance C of
%   that class.  Whenever an object of this class is explicitly or implicitly
%   cleared from the workspace, it runs the cleanup function, S.  Objects that
%   are local variables in a function are implicitly cleared at the termination
%   of that function.
%
%   Example 1: Use SSonCleanup to close a file.
%
%       function fileOpenSafely(fileName)
%           fid = fopen(fileName, 'w');
%           c = SSonCleanup(@()fclose(fid));
%
%           functionThatMayError(fid);
%       end   % c will execute fclose(fid) here
%
%
%   Example 2: Use SSonCleanup to restore the current directory.
%
%       function changeDirectorySafely(fileName)
%           currentDir = pwd;
%           c = SSonCleanup(@()cd(currentDir));
%
%           functionThatMayError;
%       end   % c will execute cd(currentDir) here
%
%   See also: CLEAR, ONCLEANUP

%   Copyright 2007 The MathWorks, Inc.
%   $Revision: 1.1.6.3 $  $Date: 2008/12/08 21:54:22 $

    properties(SetAccess='public', GetAccess='public', Transient)
        task;
    end
    
    methods
        function h = SSonCleanup(functionHandle)
            % SSonCleanup - Create a ONCLEANUP object
            %   C = SSONCLEANUP(FUNC) creates C, an SSONCLEANUP object.  There is no need to 
            %   further interact with the variable, C.  It will execute FUNC at the time it
            %   is cleared.
            %
            %   See also: CLEAR, SSONCLEANUP
            h.task = functionHandle;
        end
        
        function delete(h)
            % DELETE - Delete an SSONCLEANUP object.
            %   DELETE does not need to be called directly, as it is called when the 
            %   SSONCLEANUP object is cleared.  DELETE is implicitly called for all SSONCLEANUP
            %   objects that are local variables in a function that terminates.
            %
            %   See also: CLEAR, SSONCLEANUP, ONCLEANUP/ONCLEANUP
            h.task();
        end
    end
end
