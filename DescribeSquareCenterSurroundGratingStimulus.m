function [strDescription] = DescribeSquareCenterSurroundGratingStimulus(sStimulus, strIndent)

% SquareCenterSurroundGratingStimulus - STIMULUS A square-wave grating, with an annulus 
% that can be dynamically shifted and rotated
% This stimulus consists of two black and white square-wave gratings (center and surround), presented at some
% orientation.  The stimulus can be shifted during presentation, as well as
% rotated during presentation.
% 
% Presentation parameters:
%
%     tFrameDuration - The desired duration of a single frame
%     tStimulusDuration - The duration of the entire stimulus
%     nNumRepeats - The number of times to repeat the stimulus presentation
%        (there for convenience) 
%
% Stimulus parameters:
%
%     vfSizeDegrees - A two-element vector [fXSize fYSize], where 'fXSize' and
%        'fYSize' define the size of the stimulus presentation region in degrees.
%        If an empty matrix is provided, the the stimulus will fill the
%        presentation screen.
%     fCyclesPerDegree - The spatial frequency of the grating, in cycles per
%        degree.
%     fPixelsPerDegree - The calibration of the presentation screen, in pixels
%        per degree.
%     fBarWidthDegrees - The width of a single grating bar, in visual degrees.
%        If not provided, then the gratings have a 50% duty cycle.
%     fAngleDegrees - The orientation of the grating in degrees, where 0 degrees
%        is a vertical grating.
%     fShiftCyclesPerSec - The distance to shift the grating during the stimulus
%        presentation, in grating cycles per second.
%     fRotateCyclesPerSec - The amount to rotate the grating during the stimulus
%        presentation, in rotational cycles per second.
%     fCenterDiameterDeg - The diameter of the center grating in degrees.
%     vfCenterPosPix - A two-element vector [fXPos fYPos], where 'fXPos' and 'fYPos'
%        define the pixel position of the center grating relative to the
%        center of the screen.
%     fCenterAngle - The rotation angle of the center grating relative to the annulus. 

% DescribeSquareCenterSurroundGratingStimulus - FUNCTION Describe the
% parameters for an square-wave grating stimulus with an annulus
%
% Usage: [strDescription] = DescribeSquareCenterSurroundGratingStimulus(sStimulus <, strIndent>)
%
% Usually called automatically by StimulusServerDescribeStimuli

% Author: Patricia Molina-Luna, based on Dylan Muir's DescribeSquareGratingStimulus.m
% Created: 20th January, 2011

% -- Check arguments

if (nargin == 0)
   SSlog('*** DescribeSquareCenterSurroundGratingStimulus: Incorrect usage\n');
   help DescribeSquareCenterSurroundGratingStimulus;
   return;
end

if (~exist('strIndent', 'var') || isempty(strIndent))
   strIndent = '';
end


% -- Convert parameters to parameter strings

% - Extract parameters from stimulus
[tFrameDuration, vtStimulusDuration, nNumRepeats] = DealParamsOrNaN(sStimulus.cPresentationParameters{:});
[fPixelsPerDegree, vfCSCyclesPerDegree, ...
   cvfCSBarWidthDegrees, vfCSAngleDegrees, ...
   vfCSShiftCyclesPerSecond, vfCSRotateCyclesPerSecond, ...
   vfCenterPosOffsetDegrees, cvfCASRadiusDegrees] = ...
      DealParamsOrNaN(sStimulus.cStimulusArguments{:});

strFrameDuration = CheckNanSprintf('%.2f ms', [], tFrameDuration, tFrameDuration*1e3);
strStimulusDuration = ['[' CheckNanSprintf('%.2f ', [], vtStimulusDuration, vtStimulusDuration) '] s'];
strNumRepeats = CheckNanSprintf('%d', [], nNumRepeats, nNumRepeats);
strCyclesPerDegree = CheckNanSprintf('[%.2f , %.2f] cpd; [%.2f , %.2f] dpc', [], vfCSCyclesPerDegree, vfCSCyclesPerDegree(1), vfCSCyclesPerDegree(2), 1/vfCSCyclesPerDegree(1), 1/vfCSCyclesPerDegree(2));
strPixelsPerDegree = CheckNanSprintf('%.2f ppd', [], fPixelsPerDegree, fPixelsPerDegree);
strCenterBarWidthDegrees = CheckEmptyNanSprintf('[%.2f, %.2f] deg', '(0.5 duty cycle)', [], cvfCSBarWidthDegrees{1}, cvfCSBarWidthDegrees{1});
strSurroundBarWidthDegrees = CheckEmptyNanSprintf('[%.2f, %.2f] deg', '(0.5 duty cycle)', [], cvfCSBarWidthDegrees{2}, cvfCSBarWidthDegrees{2});
strAngleDegrees = CheckNanSprintf('[%.2f, %.2f] deg', [], vfCSAngleDegrees, vfCSAngleDegrees(1), vfCSAngleDegrees(2));
strShiftCyclesPerSec = CheckEmptyNanSprintf('[%.2f, %.2f] Hz', '(stationary)', '(dynamic or stationary)', vfCSShiftCyclesPerSecond, vfCSShiftCyclesPerSecond(1), vfCSShiftCyclesPerSecond(2));
strRotateCyclesPerSec = CheckEmptyNanSprintf('[%.2f, %.2f] Hz', '(stationary)', '(dynamic or stationary)', vfCSRotateCyclesPerSecond, vfCSRotateCyclesPerSecond(1), vfCSRotateCyclesPerSecond(2));
strCenterDiameterDeg = CheckEmptyNanSprintf('%.2f deg', '(not displayed)', cvfCASRadiusDegrees{1}, cvfCASRadiusDegrees{1});
strAnnulusWidthDeg = CheckEmptyNanSprintf('%.2f deg', '(none)', cvfCASRadiusDegrees{2}, cvfCASRadiusDegrees{2});
strSurroundDiameterDeg = CheckEmptyNanSprintf('%.2f deg', '(full field)', cvfCASRadiusDegrees{3}, cvfCASRadiusDegrees{3});
strCenterPosDeg = CheckEmptyNanSprintf('[%.2f %.2f] deg','(centered)', vfCenterPosOffsetDegrees, vfCenterPosOffsetDegrees);
   
% -- Produce description
   
strDescription = [strIndent sprintf('Oscillating plaid stimulus\n') ...
                  strIndent sprintf('Parameter list:\n')];
strIndent = [strIndent '   '];

strDescription = [strDescription ...
   strIndent sprintf('Frame duration: %s\n', strFrameDuration) ...
   strIndent sprintf('Stimulus duration: %s\n', strStimulusDuration) ...
   strIndent sprintf('Number of repeats: %s\n', strNumRepeats) ...
   strIndent sprintf('Grating temporal frequencies: %s\n', strCyclesPerDegree) ...
   strIndent sprintf('Screen calibration: %s\n', strPixelsPerDegree) ...
   strIndent sprintf('Center grating bar width: %s\n', strCenterBarWidthDegrees) ...
   strIndent sprintf('Surround grating bar width: %s\n', strSurroundBarWidthDegrees) ...
   strIndent sprintf('Grating orientations: %s\n', strAngleDegrees) ...
   strIndent sprintf('Grating drift temporal frequencies: %s\n', strShiftCyclesPerSec) ...
   strIndent sprintf('Grating rotation rates: %s\n', strRotateCyclesPerSec) ...
   strIndent sprintf('Center grating diameter: %s\n', strCenterDiameterDeg) ...
   strIndent sprintf('Annulus width: %s\n', strAnnulusWidthDeg) ...
   strIndent sprintf('Surround grating diameter: %s\n', strSurroundDiameterDeg) ...
   strIndent sprintf('Center position offset: %s\n', strCenterPosDeg)];
   
% --- END of DescribeSquareCenterSurroundGratingStimulus.m ---


function strString = CheckNanSprintf(strFormat, strEmptyFormat, oCheckNan, varargin)

if (~isnan(oCheckNan))
   strString = sprintf(strFormat, varargin{:});

elseif isempty(strEmptyFormat)
   strString = '(dynamic)';

else
   strString = sprintf(strEmptyFormat, varargin{:});
end


function strString = CheckEmptyNanSprintf(strFormat, strEmptyFormat, strNanFormat, oCheckEmptyNan, varargin)

if (isnan(oCheckEmptyNan))
   if (isempty(strNanFormat))
      strString = '(dynamic)';
   else
      strString = sprintf(strNanFormat, varargin{:});
   end
   
elseif (isempty(oCheckEmptyNan))
   if isempty(strEmptyFormat)
      strString = '(dynamic)';
   else
      strString = sprintf(strEmptyFormat, varargin{:});
   end
   
else
   strString = sprintf(strFormat, varargin{:});
end

% --- END of DescribeSquareGratingStimulus.m ---
