
function [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
            PresentSineGratingStimulus(   hWindow, vtBlankTime, ...
                                            tFrameDuration, tStimulusDuration, nNumRepeats, ...
                                            vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, ...
                                            fAngleDegrees, fShiftCyclesPerSec, fRotateCyclesPerSec, ...
                                            bDrawMask, bInvertMask, fMaskDiameterDeg, ...
                                            vfMaskPosPix, fContrast, bAdapt, ...
                                            tAdaptationDuration, fAdaptShiftCycPerSec, fAdaptContrast, ...
                                            bCyclicTF, fTFBandWidth, tTFRampDuration, ...
                                            tTFTransitionDuration)
                                        
                             
% PresentSineGratingStimulus - FUNCTION Present a sine grating
% stimulus. The stimulus can be presented with a circular mask.
%
% Usage: [tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
%             PresentSineGratingStimulus(  hWindow, vtBlankTime, ...
%                                                 tFrameDuration, tStimulusDuration, nNumRepeats, ...
%                                                 vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree, ...
%                                                 fAngleDegrees, <fShiftCyclesPerSec, fRotateCyclesPerSec>, ...
%                                                 <bDrawMask, bInvertMask>, ...
%                                                 <fMaskDiameterDeg, vfMaskPosPix, fContrast, bAdapt>, ...
%                                                 <tAdaptationDuration, fAdaptShiftCycPerSec, fAdaptContrast>, ...
%                                                 <bCyclicTF, fTFBandWidth, tTFRampDuration>, ...
%                                                 <tTFTransitionDuration>)
%
% 'vfSizeDegrees' is a vector [X Y] that defines the size of the entire
% stimulus, in degrees.  Leave as an empty vector to fill the presentation
% screen.
% 'vfMaskPosPix' is a vector [X Y] that defines the mask position relative
% to the center of the screen.
% 'bAdapt'determines if an adaptation stimulus is presented.
% 'tAdaptationDuration' is the duration of the presentation of an adaption
% grating before the main grating is presented.
% 'fAdaptShiftCycPerSec' is the temporal frequency of the presentation of
% an adaption grating before the main grating is presented.
% 'fAdaptContrast' is the contrast of the presentation of
% an adaption grating before the main grating is presented.
% 'bCyclicTF' - 'true' if a cyclic temporal frequency ramp should be
% presented, i.e. an accelerating sine wave grating.
% 'fTFBandWidth' defines how the range of cycles per second covered by the
% temporal frequency cycling.
% 'tTFRampDuration' determines the time it takes to transform the lowest
% temporal frequency to the highest.
% 'tTFTransitionDuration' determines the time of fading out from the high
% temporal frequency and fading in to the lower temporal frequency.

% Author: Andreas Keller, based on Dylan Muir's PresentSquareGratingStimulus
% Created: 7th Oktober, 2011


% -- Persistent arguments

persistent PSGS_sParams PSGS_nTexID;


% -- Check arguments

if (nargin < 8)
   disp('*** PresentSineGratingStimulus: Incorrect usage');
   help PresentSineGratingStimulus;
   return;
end

if (~exist('vtBlankTime', 'var') || isempty(vtBlankTime))
   tStartTime = 0;
   vtBlankTime = 0;
   bPresentStimulus = false;
else
   % -- Blank screen
   tStartTime = Screen('Flip', hWindow);
   
   bPresentStimulus = true;
end

if (numel(vtBlankTime) == 1)
   vtBlankTime(2) = vtBlankTime(1);
   vtBlankTime(1) = tStartTime;
end


% -- Check arguments
if (~exist('fShiftCyclesPerSec', 'var') || isempty(fShiftCyclesPerSec))
    fShiftCyclesPerSec = 0;
end

if (~exist('fRotateCyclesPerSec', 'var') || isempty(fRotateCyclesPerSec))
    fRotateCyclesPerSec = 0;
end

if (~exist('fContrast', 'var') || isempty(fContrast))
   fContrast = 1;
end

if (~exist('bDrawMask', 'var'))
    bDrawMask = [];
end

if (~exist('bInvertMask', 'var'))
    bInvertMask = [];
end

if (~exist('fMaskDiameterDeg','var'))
    fMaskDiameterDeg = [];
end

if (~exist('vfMaskPosPix','var'))
    vfMaskPosPix = [];
end

if (~exist('bAdapt','var') || isempty(bAdapt))
    bAdapt = false;
end

if (~exist('tAdaptationDuration','var') || isempty(tAdaptationDuration))
    tAdaptationDuration = 0;
end

if (~exist('fAdaptShiftCycPerSec','var') || isempty(fAdaptShiftCycPerSec))
    fAdaptShiftCycPerSec = 0;
end

if (~exist('fAdaptContrast','var') || isempty(fAdaptContrast))
    fAdaptContrast = 1;
end

if (~exist('bCyclicTF','var') || isempty(bCyclicTF))
    bCyclicTF = false;
end

if (~exist('fTFBandWidth','var') || isempty(fTFBandWidth))
    fTFBandWidth = 0;
end

if (~exist('tTFRampDuration','var') || isempty(tTFRampDuration))
    tTFRampDuration = 0;
end

if (~exist('tTFTransitionDuration','var') || isempty(tTFTransitionDuration))
    tTFTransitionDuration = 0;
end

% - Do we need to re-generate the stimulus?

if (isempty(PSGS_sParams))
   bGenerateStim = true;

else
   % - Test each relevant parameter
   bGenerateStim = ~isequal(PSGS_sParams.fCyclesPerDegree, fCyclesPerDegree);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.hWindow, hWindow);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.fPixelsPerDegree, fPixelsPerDegree);
   bGenerateStim = bGenerateStim | ~isequal(PSGS_sParams.vfSizeDegrees, vfSizeDegrees);
end


% -- Generate stimulus

if (bGenerateStim)
   disp('--- PresentSineGratingStimulus: Generating stimulus');
   
   % - Record parameters
   PSGS_sParams = [];
   PSGS_sParams.fCyclesPerDegree = fCyclesPerDegree;
   PSGS_sParams.hWindow = hWindow;
   PSGS_sParams.fPixelsPerDegree = fPixelsPerDegree;   
   PSGS_sParams.vfSizeDegrees = vfSizeDegrees;
   PSGS_sParams.fPixelsPerCycle = fPixelsPerDegree / fCyclesPerDegree;
   PSGS_sParams.fContrast = fContrast;
   PSGS_sParams.tAdaptationDuration = tAdaptationDuration;
   PSGS_sParams.fAdaptShiftCycPerSec = fAdaptShiftCycPerSec;
   PSGS_sParams.fAdaptContrast = fAdaptContrast;
   
   % - Determine how big the stimulus should be, so it can be rotated and
   %   shifted
   if (isempty(vfSizeDegrees))
      [vnScreenSize(1), vnScreenSize(2)] = Screen('WindowSize', hWindow);
      
      vfScreenSizeDegrees = sqrt(sum((vnScreenSize / fPixelsPerDegree).^2)) .* [1 1];
      vfSizeDegrees = (sqrt(sum(vfScreenSizeDegrees.^2)) + 1/fCyclesPerDegree) * [1 1];
   end
   
   % - Generate stimulus
   mfGrating = GenerateSineGratingStimulus(vfSizeDegrees, fCyclesPerDegree, fPixelsPerDegree);
   
   % - Convert to screen colour indices
   nBlack = BlackIndex(hWindow);
   nWhite = WhiteIndex(hWindow);
   
   mfGrating=round(mfGrating*nWhite+(1-mfGrating)*nBlack);
  
   PSGS_sParams.mfGrating = mfGrating;
   
   % - Clear texture cache
   PSGS_nTexID = [];
end

% - Create textures, if necessary
if (isempty(PSGS_nTexID))
   bGenerateTexture = true;
else
   % - Test to see if our texture exists
   vnTextures = Screen('Windows');
   
   if (~any(vnTextures == PSGS_nTexID))
      bGenerateTexture = true;
   else
      bGenerateTexture = false;
   end
end

% - Generate a texture, if required
if (bGenerateTexture)   
   % - Check that we can actually use a shader
   AssertGLSL;
   
   % - Generate a shader to use
   hGlsl = MakeTextureDrawShader(hWindow, 'SeparateAlphaChannel');
   
   % - Generate the texture
   PSGS_nTexID = GenerateTextures(PSGS_sParams.mfGrating, hWindow, hGlsl);
end


% -- Present the stimulus using PresentSimpleStimulus

if (~bPresentStimulus)
    tLastPresentation = [];
    bBlewBlankTime = [];
    bBlewFrameRate = [];
    return;
end

vfShiftPixelsPerFrame = fShiftCyclesPerSec / fCyclesPerDegree * fPixelsPerDegree * tFrameDuration;
fRotateDegreesPerFrame = fRotateCyclesPerSec * tFrameDuration * 360;


fAdaptShiftPixPerFr = fAdaptShiftCycPerSec / fCyclesPerDegree * fPixelsPerDegree * tFrameDuration;


if bCyclicTF
    vfShiftPixelsPerFrame(2) = fTFBandWidth / fCyclesPerDegree * fPixelsPerDegree * tFrameDuration;
    vfShiftPixelsPerFrame(1) = vfShiftPixelsPerFrame(1) - vfShiftPixelsPerFrame(2) / 2;
end

[tLastPresentation, bBlewBlankTime, bBlewFrameRate] = ...
    PresentSimpleStimulus(hWindow, vtBlankTime, ...
        tFrameDuration, tStimulusDuration, nNumRepeats, ...
        PSGS_nTexID, vfShiftPixelsPerFrame, fAngleDegrees, fRotateDegreesPerFrame, ...
        PSGS_sParams.fPixelsPerCycle, fPixelsPerDegree, bDrawMask, bInvertMask, ...
        fMaskDiameterDeg, vfMaskPosPix, fContrast, bAdapt, tAdaptationDuration, ...
        fAdaptShiftPixPerFr, fAdaptContrast, bCyclicTF, ...
        tTFRampDuration, tTFTransitionDuration);

% --- END of PresentSineGratingStimulus.m ---


