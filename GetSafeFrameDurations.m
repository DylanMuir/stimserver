function [vtSafeFrameDurations, tIFI] = GetSafeFrameDurations(hWindow, vtTargetDurations)

% GetSaferameDurations - FUNCTION Determine safe frame rates for the current setup
%
% Usage: [vtSafeFrameDurations, tIFI] = GetSafeFrameDurations(hWindow <, vtTargetDurations>)
%
%

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 13th September, 2010

% -- Check arguments

if (nargin < 1)
   SSlog('*** GetSafeFrameDurations: Incorrect usage');
   help GetSafeFrameDurations;
   return;
end


% -- Determine safe frame durations

% - Get native inter-refresh interval
tIFI = Screen('GetFlipInterval', hWindow);

% - Should we return a reasonable list of safe frame durations?
if (~exist('vtTargetDurations', 'var') || isempty(vtTargetDurations))
   vtSafeFrameDurations = (1:20) * tIFI;
else
   
   % - Find the closest possible frame durations to the targets
   vtSafeFrameDurations = round(vtTargetDurations ./ tIFI) .* tIFI;
   
   % Filter out zero durations
   vtSafeFrameDurations(vtSafeFrameDurations == 0) = tIFI;
end

% --- END of GetSafeFrameDuration.m ---
