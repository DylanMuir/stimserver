function StartStimulusServer(sStimuli, nListenPort, hWindow, bTCP, oLogging, oTalkback)

% StartStimulusServer - FUNCTION Start a stimulus server using the matlab instrument control toolbox
%
% Usage: StartStimulusServer(sStimuli, nListenPort, hWindow, bTCP, ...)
%        StartStimulusServer(... <, strLogFilename>)
%        StartStimulusServer(... <, {strLogAddress nLogPort <vnTerminator>}>)
%        StartStimulusServer(..., {}, {strTalkbackAddress nTalkbackPort <vnTerminator>})
%
% 'sStimuli' is a matlab structure array, each element of which defines a
% stimulus ID starting from 1.  These are the stimuli that will be presented by
% the stimulus server, on command.  Each stimulus structure must have the form
% returned by CreateStimulusStructure.
%
% 'nListenPort' defines the UDP port number that the stimulus server should use
% to listen for commands.
%
% 'hWindow' is a handle to an open Psychtoolbox window on which stimuli will be
% displayed.
%
% 'bTCP', if true, indicates to use TCP connections.  If false (the default),
% UDP connections will be used.  TCP can only be used with PNET.
%
% 'strLogFilename' or {'strLogAddress' 'nLogPort'} define logging options for
% the sitmulus server.  If 'strLogFilename' is supplied, the server will
% attempt to write log messages to the specified file.  Log messages will be
% APPENDED to this file.  If 'strLogAddress' and 'nLogPort' are instead
% supplied, the server will send log messages over a UDP channel to the
% specified machine and port 'strLogAddress':'nLogPort'.
% If the optional parameter 'vnTerminator' is provided, it will override the
% default system terminator (which is CR/LF).
%
% {'strTalkbackAddress' 'nTalkbackPort'} specify that the stimulus server should
% send status messages over a UDP connection to
% 'strTalkbackAddress':'nTalkbackPort'.
% If the optional parameter 'vnTerminator' is provided, it will override the
% default system terminator (which is CR/LF).

% Author: Dylan Muir <dylan@ini.phys.ethz.ch>
% Created: 31st August, 2010

% -- Global state

global STIMSERV_sStimulusList STIMSERV_sState;


% -- Defaults

DEF_bTCP = false;
DEF_oLogging = [];
DEF_oTalkback = [];
DEF_vnTerminator = char([13 10]);      % CR / LF


% -- Check arguments

if (nargin < 3)
   SSlog('*** StartStimulusServer: Incorrect usage.\n');
   help StartStimulusServer;
   sca;
   return;
end

if (~exist('bTCP', 'var'))
   bTCP = DEF_bTCP;
end

if (~exist('oLogging', 'var'))
   oLogging = DEF_oLogging;
end

if (~exist('oTalkback', 'var'))
   oTalkback = DEF_oTalkback;
end


% -- Check for an existing server

if (~isempty(STIMSERV_sState))
   SSlog('--- StartStimulusServer: Warning: A server was already running or was not cleanly stopped.\n');
   SSlog('       The existing server will be destroyed.\n');
   
   % - Close the existing server
   StopStimulusServer('cleanup');
end


% -- Save the supplied stimulus list and other parameters

STIMSERV_sStimulusList = sStimuli;

STIMSERV_sState.hWindow = hWindow;
STIMSERV_sState.tLastStimTime = -inf;
STIMSERV_sState.bMissedBlank = false;
STIMSERV_sState.bBlewFrameRate = false;
STIMSERV_sState.bUseTCP = bTCP;
STIMSERV_sState.oService = nListenPort;
STIMSERV_sState.hServSocket = [];
STIMSERV_sState.oLogging = [];
STIMSERV_sState.hLogSocket = [];
STIMSERV_sState.vnLogTerminator = DEF_vnTerminator;
STIMSERV_sState.oTalkback = [];
STIMSERV_sState.hTalkbackSocket = [];
STIMSERV_sState.vnTalkbackTerminator = DEF_vnTerminator;


% - Register an onCleanup instance to shut down the server if this function
% terminates
hOC = SSonCleanup(@() StopStimulusServer('cleanup'));


% -- Pre-initialise stimuli

StimServerVisualFeedback(hWindow, 'Pre-initialising stimuli...');

try
   for (nStimID = 1:numel(sStimuli)) %#ok<FORPF>
      SSlog('--- StartStimulusServer: Pre-initialising stimulus [%d]...\n', nStimID);
      PresentArbitraryStimulus(sStimuli(nStimID), hWindow, []);
   end

catch err
   % - Error initialising stimuli, so shut down server
   try
      StopStimulusServer('abort');
   catch err2
   end
   
   rethrow(err);
end
   
SSlog('--- StartStimulusServer: Finished Pre-initialising stimuli.\n');


% -- Test if we should use the instrument control toolbox or PNET

if (~bTCP && license('test', 'instrument') && license('checkout', 'instrument'))
   STIMSERV_sState.bUsePNET = false;

elseif (exist('pnet') ~= 3) %#ok<EXIST>
   % - PNET was not found
   SSlog('*** StartStimulusServer: Could not find the instrument control toolbox or pnet.');
   SSlog('       Cannot start the server');
   return;
   
else
   STIMSERV_sState.bUsePNET = true;
end


% -- Start the server with the requested interface

% - Disable OnCleanup
hOC.task = @()true;

if (STIMSERV_sState.bUsePNET)
   StartStimulusServerPNET;
else
   StartStimulusServerICT;
end



% --- END of StartStimulusServer FUNCTION ---


function StartStimulusServerPNET

   % -- Should we use TCP or UDP sockets?
   if (STIMSERV_sState.bUseTCP)
      STIMSERV_sState.hServSocket{1} = pnet('tcpsocket', STIMSERV_sState.oService);

      % - We currently have no connection handle
      STIMSERV_sState.hServSocket{2} = [];
      
   else
      % - USe a UDP socket
      STIMSERV_sState.hServSocket{1} = pnet('udpsocket', STIMSERV_sState.oService);
      
      % - The connection handle is the UDP socket
      STIMSERV_sState.hServSocket{2} = STIMSERV_sState.hServSocket{1};
   end
      
   % - Could we open the socket?
   if (STIMSERV_sState.hServSocket{1} == -1)
      SSlog('*** StartStimulusServer: Could not open socket to listen for commands.\n');
      SSlog('       Could not start server.\n');
      return;
   end

   % - Set read timeout
   pnet(STIMSERV_sState.hServSocket{1}, 'setreadtimeout', 1);
   
   % -- Configure logging and talkback
   % - Extract logging details
   StimServerLogging(oLogging);
   
   % - Extract talkback details
   StimServerTalkback(oTalkback);
   
   
   % -- Display a message
   
   SSlog('--- StartStimulusServer: A stimulus server (ver %s) is now running on port %d.\n', StimServerVersion, nListenPort);
   SSlog('       %d stimuli cued.\n', numel(sStimuli));
   
   SStalkback('SST STARTUP %s', StimServerVersion);
   
   if (STIMSERV_sState.bUseTCP)
      SStalkback('SST LISTEN TCP %d', nListenPort);
   else
      SStalkback('SST LISTEN UDP %d', nListenPort);
   end
   
   % - Describe loaded stimuli
   StimulusServerDescribeStimuli;
   
   SStalkback('SST IDLE');   
   
   % - Enter blocking core server function
   StimulusServer_PNET;
end



function StartStimulusServerICT

   % -- Create an UDP listener object
   
   STIMSERV_sState.hServSocket = udp;
   set(STIMSERV_sState.hServSocket, 'LocalPort', STIMSERV_sState.oService);
   
   
   % -- Open the UDP port and assign the server callback function
   
   set(STIMSERV_sState.hServSocket, 'DatagramReceivedFcn', @StimulusServer_CALLBACK);
   
   try
      fopen(STIMSERV_sState.hServSocket);
      
   catch errMsg
      SSlog('*** StartStimulusServer: Could not start server.\n');
      SSlog('       Error: [%s]\n', errMsg.message);
      
      STIMSERV_sState = [];
      STIMSERV_sStimulusList = [];
      
      return;
   end
   
   
   % -- Configure logging and talkback
   % - Extract logging details
   StimServerLogging(oLogging);
   
   % - Extract talkback details
   StimServerTalkback(oTalkback);
   
   
   % -- Display a message
   
   SSlog('--- StartStimulusServer: A stimulus server (ver %s) is now running on port %d.\n', StimServerVersion, nListenPort);
   SSlog('       %d stimuli cued.\n', numel(sStimuli));
   
   SStalkback('SST STARTUP %s', StimServerVersion);
   SStalkback('SST LISTEN UDP %d', nListenPort);
   
   % - Describe loaded stimuli
   StimulusServerDescribeStimuli;
   
   SStalkback('SST IDLE');
end

% --- END of StartStimulusServerICT FUNCTION ---

end

% --- END of StartStimulusServer.m ---

